var map;
var place_id = "";
var marker;
var categoria;
var latlngAtual;
var markers = [];
var tipo_busca;
var categorias = [{"descr_categoria_ing":"train_station","id_categoria":96,"descr_categoria":"Estação de Trem"},
{"descr_categoria_ing":"gym","id_categoria":48,"descr_categoria":"Academia"},
{"descr_categoria_ing":"airport","id_categoria":49,"descr_categoria":"aeroporto"},
{"descr_categoria_ing":"aquarium","id_categoria":50,"descr_categoria":"aquário"},
{"descr_categoria_ing":"balada","id_categoria":51,"descr_categoria":"balada"},
{"descr_categoria_ing":"bank","id_categoria":52,"descr_categoria":"banco"},
{"descr_categoria_ing":"bar","id_categoria":53,"descr_categoria":"bar"},
{"descr_categoria_ing":"library","id_categoria":54,"descr_categoria":"biblioteca"},
{"descr_categoria_ing":"bicycle_store","id_categoria":55,"descr_categoria":"bicicletaria"},
{"descr_categoria_ing":"bowling_alley","id_categoria":56,"descr_categoria":"boliche"},
{"descr_categoria_ing":"cafe","id_categoria":57,"descr_categoria":"café"},
{"descr_categoria_ing":"campground","id_categoria":58,"descr_categoria":"camping e alojamento"},
{"descr_categoria_ing":"finance","id_categoria":59,"descr_categoria":"cartório"},
{"descr_categoria_ing":"locksmith","id_categoria":60,"descr_categoria":"chaveiro"},
{"descr_categoria_ing":"movie_theater","id_categoria":61,"descr_categoria":"cinema"},
{"descr_categoria_ing":"post_office","id_categoria":62,"descr_categoria":"correio"},
{"descr_categoria_ing":"dentist","id_categoria":63,"descr_categoria":"dentista"},
{"descr_categoria_ing":"danceteria","id_categoria":64,"descr_categoria":"danceteria"},
{"descr_categoria_ing":"school","id_categoria":65,"descr_categoria":"escola"},
{"descr_categoria_ing":"subway_station","id_categoria":66,"descr_categoria":"estação de metrô"},
{"descr_categoria_ing":"parking","id_categoria":67,"descr_categoria":"estacionamento"},
{"descr_categoria_ing":"stadium","id_categoria":68,"descr_categoria":"estádio"},
{"descr_categoria_ing":"evento_temporario","id_categoria":69,"descr_categoria":"evento temporário"},
{"descr_categoria_ing":"pharmacy","id_categoria":70,"descr_categoria":"farmácia"},
{"descr_categoria_ing":"hospital","id_categoria":71,"descr_categoria":"hospital"},
{"descr_categoria_ing":"lodging","id_categoria":72,"descr_categoria":"hotel"},
{"descr_categoria_ing":"church","id_categoria":73,"descr_categoria":"igreja"},
{"descr_categoria_ing":"car_wash","id_categoria":74,"descr_categoria":"lava rápido"},
{"descr_categoria_ing":"book_store","id_categoria":75,"descr_categoria":"livraria"},
{"descr_categoria_ing":"store","id_categoria":76,"descr_categoria":"lojas"},
{"descr_categoria_ing":"loterica","id_categoria":77,"descr_categoria":"lotéricas"},
{"descr_categoria_ing":"museum","id_categoria":78,"descr_categoria":"museu"},
{"descr_categoria_ing":"car_repair","id_categoria":79,"descr_categoria":"oficina mecânica"},
{"descr_categoria_ing":"bakery","id_categoria":80,"descr_categoria":"padaria"},
{"descr_categoria_ing":"amusement_park","id_categoria":81,"descr_categoria":"parque de diversões"},
{"descr_categoria_ing":"police","id_categoria":82,"descr_categoria":"polícia"},
{"descr_categoria_ing":"gas_station","id_categoria":83,"descr_categoria":"posto de gasolina"},
{"descr_categoria_ing":"city_hall","id_categoria":84,"descr_categoria":"prefeitura"},
{"descr_categoria_ing":"restaurant","id_categoria":85,"descr_categoria":"restaurante"},
{"descr_categoria_ing":"0","id_categoria":86,"descr_categoria":"rodoviária"},
{"descr_categoria_ing":"bus_station","id_categoria":87,"descr_categoria":"ponto de ônibus"},
{"descr_categoria_ing":"beauty_salon","id_categoria":88,"descr_categoria":"salão de beleza"},
{"descr_categoria_ing":"shopping_mall","id_categoria":89,"descr_categoria":"shopping"},
{"descr_categoria_ing":"grocery_or_supermarket","id_categoria":90,"descr_categoria":"supermercado"},
{"descr_categoria_ing":"theater","id_categoria":91,"descr_categoria":"teatro"},
{"descr_categoria_ing":"university","id_categoria":92,"descr_categoria":"universidade"},
{"descr_categoria_ing":"veterinary_care","id_categoria":93,"descr_categoria":"veterinário"},
{"descr_categoria_ing":"zoo","id_categoria":94,"descr_categoria":"zoológico"}];



function descr_categoria(id_categoria){
   $(categorias).each(function (index, value) {
      if(value.id_categoria == id_categoria){
         categoria = value.descr_categoria_ing;
      }
   })
}

function addCategoryMarkers(latlng){
   var id_categoria = document.getElementById('ID_CATEGORIA').value;
   descr_categoria(id_categoria);
   setMapOnAll(null);
   if(categoria != ''){
      infowindow = new google.maps.InfoWindow();
      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch({
         location: latlng,
         radius: 300,
         type: [categoria]
      }, callback);
   }
}

function callback(results, status) {
   if (status === google.maps.places.PlacesServiceStatus.OK) {
      // alert(setPlaceId(latlngAtual));
      var place_atual = "place_id";
      // alert(place_atual);
      for (var i = 0; i < results.length; i++) {
         var SameThreshold = 0.1;
         if (google.maps.geometry.spherical.computeDistanceBetween(latlngAtual, results[i].geometry.location) > SameThreshold){
            createMarker2(results[i]);
         }

      }
   }
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}


function initialize() {
   var latlng = new google.maps.LatLng(document.getElementById('LATITUDE_USER').value, document.getElementById('LONGITUDE_USER').value);
   latlngAtual = latlng;
	var mapOptions = {
		center: latlng,
		zoom: 15
   };


	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
   var input = document.getElementById('pac-input');

   var autocomplete = new google.maps.places.Autocomplete(input);
   autocomplete.bindTo('bounds', map);

   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

   var infowindow = new google.maps.InfoWindow();
   var marker = new google.maps.Marker({
     map: map
   });
   marker.addListener('click', function() {
     infowindow.open(map, marker);
   });

   markers.push(marker);

   autocomplete.addListener('place_changed', function() {
         $('#spaninfo').html('<div id="infowindow-content"><span id="place-name"  class="title"></span><br> <span id="place-address"></span></div>');
         infowindow.close();
         var place = autocomplete.getPlace();
         if (!place.geometry) {
           return;
         }

         if (place.geometry.viewport) {
           map.fitBounds(place.geometry.viewport);
         } else {
           map.setCenter(place.geometry.location);
           map.setZoom(17);
         }
         clearMarkers();
         marker = new google.maps.Marker({
             position: place.geometry.location,
             map: map,
             draggable: true //make it draggable
         });

         // Set the position of the marker using the place ID and location.
         marker.setPlace({
           placeId: place.place_id,
           location: place.geometry.location
         });
         marker.setVisible(true);
         marker.setPosition(place.geometry.location);

         document.getElementById('place-name').textContent = place.name;
         document.getElementById('place-address').textContent = place.formatted_address;
         place_id = place.place_id;

         // número rua = street_number
         // cidade = administrative_area_level_2
         // nome_rua = route
         $.each(place.address_components, function(index, item){
            if($.inArray('street_number', item.types) > -1){
               $("#numero_endereco").val(item.long_name);
            };
            if($.inArray('administrative_area_level_2', item.types) > -1){
               $("#cidade_endereco").val(item.long_name);
            };
            if($.inArray('route', item.types) > -1){
               $("#rua_endereco").val(item.long_name);
            };
         })
         markers.push(marker);
         latlngAtual = place.geometry.location;
         map.setZoom(20);
         markerLocation();

         infowindow.setContent(document.getElementById('infowindow-content'));
         infowindow.open(map, marker);
      });




   // var latlng2 = new google.maps.LatLng(document.getElementById('LATITUDE_USER').value, document.getElementById('LONGITUDE_USER').value);
   createMarker(latlng);
   //addCategoryMarkers(latlng);
   // tipo_busca = document.getElementById('tipo_busca').value;

	google.maps.event.addListener(map, 'click', function(event) {
      //   if(tipo_busca == 2){
      //      return;
      //   }
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        latlngAtual = clickedLocation;
        //addCategoryMarkers(clickedLocation);

        //If the marker hasn't been added.
      //   if(marker === false){
      clearMarkers();
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
      //   } else{

            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
            markers.push(marker)
      //   }
        //Get the marker's location.
        latlngAtual = clickedLocation;


        markerLocation();
    });
}

google.maps.event.addDomListener(window, 'load', initialize);


function searchAddress() {

  var addressInput = document.getElementById('rua_endereco').value + ',' + document.getElementById('numero_endereco').value + ' ' + document.getElementById('cidade_endereco').value;

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({address: addressInput}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {

      var myResult = results[0].geometry.location;

      createMarker(myResult);
      //addCategoryMarkers(myResult);

      map.setCenter(myResult);

      latlngAtual = myResult;
      map.setZoom(17);
			markerLocation();
		}
	});

}

function createMarker(latlng) {

  if(marker != undefined && marker != ''){
    marker.setMap(null);
    marker = '';
  }

  marker = new google.maps.Marker({
    map: map,
    position: latlng
  });

  markers.push(marker);
}

function createMarker2(place) {
      icon = "http://maps.google.com/mapfiles/ms/icons/red.png";
      var placeLoc = place.geometry.location;
      // alert(placeLoc);

      var marker2 = new google.maps.Marker({
         map: map,
         position: place.geometry.location,
         icon: icon
      });
      markers.push(marker2);

      google.maps.event.addListener(marker2, 'click', function() {
         var position = marker2.position;
         latlngAtual = position;
         clearMarkers();
         //addCategoryMarkers(position);
         createMarker(position);
         markerLocation();

      });
     }


function markerLocation(){
   //Add lat and lng values to a field that we can save.
   document.getElementById('place_id').value = place_id; //latitude
   document.getElementById('LATITUDE_USER').value = latlngAtual.lat(); //latitude
   document.getElementById('LONGITUDE_USER').value = latlngAtual.lng(); //longitude
}
