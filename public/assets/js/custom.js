$(document).ready(function() {

    $("#ajax-contato-form").submit(function() {
        var str = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "sendformcontato",
            data: str,
            success: function(msg) {
                if (msg == 'OK') {
                    result =
                        '<div class="notification_ok">Obrigado, sua mensagem foi recebida e em breve entraremos em contato.</div>';
                    $("#fields").hide()
                } else {
                    result = msg
                }
                $('#note').html(result)
            },
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(thrownError);
			}
        });
        return false
    });
    $( "#selectplangold" )
    .change(function () {
    	if($(this).val() == 1){
			$( "#vlrplangold" ).text( $( "#vlrplangold1" ).val() );
	    } else if($(this).val() == 2){
	    	$( "#vlrplangold" ).text( $( "#vlrplangold2" ).val() );
	    } else if($(this).val() == 3){
	    	$( "#vlrplangold" ).text( $( "#vlrplangold3" ).val() );
	    }
    });
    $( "#selectplansilver" )
    .change(function () {
    	if($(this).val() == 1){
			$( "#vlrplansilver" ).text( $( "#vlrplansilver1" ).val() );
	    } else if($(this).val() == 2){
	    	$( "#vlrplansilver" ).text( $( "#vlrplansilver2" ).val() );
	    } else if($(this).val() == 3){
	    	$( "#vlrplansilver" ).text( $( "#vlrplansilver3" ).val() );
	    }
    });
    $( "#selectplanbronze" )
    .change(function () {
    	if($(this).val() == 1){
			$( "#vlrplanbronze" ).text( $( "#vlrplanbronze1" ).val() );
	    } else if($(this).val() == 2){
	    	$( "#vlrplanbronze" ).text( $( "#vlrplanbronze2" ).val() );
	    } else if($(this).val() == 3){
	    	$( "#vlrplanbronze" ).text( $( "#vlrplanbronze3" ).val() );
	    }
    });
    $( "#selectplanpag" )
    .change(function () {
    	update_planos();
    });
    $( "#selectplanpagper" )
    .change(function () {
    	update_planos();
    });
});

function update_planos(){
	var idperiodo = $( "#selectplanpagper" ).val();
	switch($( "#selectplanpag" ).val()) {
	    case '1':
	    	$( "#vlrplanpag" ).text( $( "#vlrplangold"+idperiodo ).val() );
	        break;
	    case '2':
	    	$( "#vlrplanpag" ).text( $( "#vlrplansilver"+idperiodo ).val() );
	        break;
	    case '3':
	    	$( "#vlrplanpag" ).text( $( "#vlrplanbronze"+idperiodo ).val() );
	        break;
	    default:
	        //...
	}

}
