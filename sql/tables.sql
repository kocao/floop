DROP TABLE public.ci_sessions;
DROP TABLE public.financeiro;
DROP TABLE public.periodicidade;

DROP TABLE public.periodo_plano;
DROP TABLE public.planos;
DROP TABLE public.userdetail;
DROP TABLE public.userstype;
DROP TABLE public.users;
DROP TABLE public.categorias;
DROP TABLE public.log_sistema;

-- Table: public.ci_sessions


CREATE TABLE public.ci_sessions
(
    id character varying(128) COLLATE pg_catalog."default" NOT NULL,
    ip_address character varying(45) COLLATE pg_catalog."default" NOT NULL,
    "timestamp" bigint NOT NULL DEFAULT 0,
    data text COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    CONSTRAINT ci_sessions_pkey PRIMARY KEY (id, ip_address)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.ci_sessions
    OWNER to floopapp;

-- Index: ci_sessions_timestamp

-- DROP INDEX public.ci_sessions_timestamp;

CREATE INDEX ci_sessions_timestamp
    ON public.ci_sessions USING btree
    (timestamp)
    TABLESPACE pg_default;


-- Table: public.categorias

--

CREATE TABLE public.categorias
(
    id_categoria integer NOT NULL DEFAULT nextval('seq_id_categoria_categorias'::regclass),
    descr_categoria character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT categorias_pkey PRIMARY KEY (id_categoria)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


-- Table: public.userstype

CREATE TABLE public.userstype
(
    id_usertype integer NOT NULL DEFAULT nextval('seq_id_usertype_userstype'::regclass),
    descr_usertype character varying(50) COLLATE pg_catalog."default" NOT NULL,
    is_admin boolean NOT NULL DEFAULT false,
    CONSTRAINT userstype_pkey PRIMARY KEY (id_usertype)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.userstype
    OWNER to floopapp;


-- Table: public.users
CREATE TABLE public.users
(
    id_user integer NOT NULL DEFAULT nextval('seq_id_user_users'::regclass),
    email_user character varying(250) COLLATE pg_catalog."default" NOT NULL,
    pws_user character varying(256) COLLATE pg_catalog."default" NOT NULL,
    data_cadastro timestamp without time zone NOT NULL DEFAULT now(),
    nome_user character varying(50) COLLATE pg_catalog."default" NOT NULL DEFAULT 'não informado'::character varying,
    id_usertype integer NOT NULL DEFAULT 2,
    key_user character varying(32) COLLATE pg_catalog."default" NOT NULL,
    ativo_inativo integer NOT NULL DEFAULT 1,
    CONSTRAINT users_pkey PRIMARY KEY (id_user),
    CONSTRAINT fk_id_usertype_users FOREIGN KEY (id_usertype)
        REFERENCES public.userstype (id_usertype) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to floopapp;



-- Table: public.financeiro



CREATE TABLE public.financeiro
(
    id_finan integer NOT NULL DEFAULT nextval('seq_id_finan_financeiro'::regclass),
    descr_finan character varying(50) COLLATE pg_catalog."default",
    dtvecto_finan date NOT NULL DEFAULT ('now'::text)::date,
    valor_finan numeric(10, 2) NOT NULL,
    debcred_finan character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'C'::character varying,
    status_finan character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'P'::character varying,
    id_user integer NOT NULL,
    ativoinativo_finan character varying(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'A'::character varying,
    CONSTRAINT financeiro_pkey PRIMARY KEY (id_finan),
    CONSTRAINT fk_iduser_users FOREIGN KEY (id_user)
        REFERENCES public.users (id_user) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.financeiro
    OWNER to floopapp;


-- Table: public.periodicidade



CREATE TABLE public.periodicidade
(
    id_periodo integer NOT NULL DEFAULT nextval('seq_id_periodo_periodicidade'::regclass),
    descr_periodo character varying(50) COLLATE pg_catalog."default" NOT NULL,
    qtdias_periodo integer NOT NULL,
    CONSTRAINT periodicidade_pkey PRIMARY KEY (id_periodo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.periodicidade
    OWNER to floopapp;




-- Table: public.planos

-- DROP TABLE public.planos;

CREATE TABLE public.planos
(
    id_plano integer NOT NULL DEFAULT nextval('seq_id_plano_planos'::regclass),
    descr_plano character varying(50) COLLATE pg_catalog."default" NOT NULL,
    maxdescr_plano integer NOT NULL DEFAULT 0,
    maxfotos_plano integer NOT NULL DEFAULT 0,
    maxeventos_plano integer DEFAULT 0,
    abrev_plano character varying(1) COLLATE pg_catalog."default",
    maxfotoseventos_plano integer NOT NULL DEFAULT 0,
    maxfotospromo_plano integer NOT NULL DEFAULT 0,
    showfacebook_plano boolean,
    showyoutube_plano boolean,
    showtwitter_plano boolean,
    showinstagram_plano boolean,
    showgmais_plano boolean,
    showsite_plano boolean,
    showreserva_plano boolean,
    maxfotoscardapio_plano integer NOT NULL DEFAULT 0,
    CONSTRAINT planos_pkey PRIMARY KEY (id_plano)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.planos
    OWNER to floopapp;

-- Table: public.periodo_plano

-- DROP TABLE public.periodo_plano;

CREATE TABLE public.periodo_plano
(
    id_plano integer NOT NULL,
    id_periodo integer NOT NULL,
    valor numeric(10, 2) NOT NULL,
    url_pagto character varying(256) COLLATE pg_catalog."default",
    CONSTRAINT pk_id_plano_id_periodo_planos_periodo PRIMARY KEY (id_plano, id_periodo),
    CONSTRAINT fk_idperiodo_periodicidade FOREIGN KEY (id_periodo)
        REFERENCES public.periodicidade (id_periodo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idplano_planos FOREIGN KEY (id_plano)
        REFERENCES public.planos (id_plano) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.periodo_plano
    OWNER to floopapp;

-- Table: public.userdetail

-- DROP TABLE public.userdetail;

CREATE TABLE public.userdetail
(
    id_user integer NOT NULL,
    cnpj_user character varying(18) COLLATE pg_catalog."default",
    nomeempresa_user character varying(50) COLLATE pg_catalog."default",
    enderemp_user character varying(50) COLLATE pg_catalog."default",
    nroendemp_user character varying(15) COLLATE pg_catalog."default",
    cependemp_user character varying(9) COLLATE pg_catalog."default",
    compendemp_user character varying(30) COLLATE pg_catalog."default",
    bairroendemp_user character varying(30) COLLATE pg_catalog."default",
    cidendemp_user character varying(30) COLLATE pg_catalog."default",
    ufendemp_user character varying(2) COLLATE pg_catalog."default",
    pathlogoemp_user character varying(255) COLLATE pg_catalog."default",
    id_plano integer,
    id_periodo integer,
    id_categoria integer NOT NULL DEFAULT 1,
    fonecom_user character varying(20) COLLATE pg_catalog."default",
    fonecel_user character varying(20) COLLATE pg_catalog."default",
    siteemp_user character varying(255) COLLATE pg_catalog."default",
    lnkreservamesa_user character varying(255) COLLATE pg_catalog."default",
    lnkfacebook_user character varying(255) COLLATE pg_catalog."default",
    lnkyoutube_user character varying(255) COLLATE pg_catalog."default",
    lnktwitter_user character varying(255) COLLATE pg_catalog."default",
    lnkinstagram_user character varying(255) COLLATE pg_catalog."default",
    lnkgmais_user character varying(255) COLLATE pg_catalog."default",
    latitude_user double precision NOT NULL DEFAULT '0'::double precision,
    longitude_user double precision NOT NULL DEFAULT '0'::double precision,
    CONSTRAINT fk_idcategoria_categorias FOREIGN KEY (id_categoria)
        REFERENCES public.categorias (id_categoria) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idperiodo_periodicidade FOREIGN KEY (id_periodo)
        REFERENCES public.periodicidade (id_periodo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_idplano_planos FOREIGN KEY (id_plano)
        REFERENCES public.planos (id_plano) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.userdetail
    OWNER to floopapp;

-- Table: public.anuncios_status

-- DROP TABLE public.anuncios_status;

CREATE TABLE public.anuncios_status
(
    id_statusanuncio integer NOT NULL DEFAULT nextval('seq_id_statusanuncio_anuncios_status'::regclass),
    descr_statusanuncio character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT anuncios_status_pkey PRIMARY KEY (id_statusanuncio)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.anuncios_status
    OWNER to floopapp;


-- Table: public.anuncios

-- DROP TABLE public.anuncios;

CREATE TABLE public.anuncios
(
    id_anuncio integer NOT NULL DEFAULT nextval('seq_id_anuncio_anuncios'::regclass),
    id_user integer NOT NULL,
    descr_anuncio text COLLATE pg_catalog."default" NOT NULL,
    id_statusanuncio integer NOT NULL DEFAULT 1,
    CONSTRAINT anuncios_pkey PRIMARY KEY (id_anuncio),
    CONSTRAINT anuncios_id_statusanuncio_fkey FOREIGN KEY (id_statusanuncio)
        REFERENCES public.anuncios_status (id_statusanuncio) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.anuncios
    OWNER to floopapp;


-- Table: public.images_type

-- DROP TABLE public.images_type;

CREATE TABLE public.images_type
(
    id_imgtype integer NOT NULL DEFAULT nextval('seq_id_tipoimg_images_type'::regclass),
    descr_imgtype character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT images_type_pkey PRIMARY KEY (id_imgtype)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.images_type
    OWNER to floopapp;


-- Table: public.images_users

-- DROP TABLE public.images_users;

CREATE TABLE public.images_users
(
    id_img integer NOT NULL DEFAULT nextval('seq_id_img_images_users'::regclass),
    id_tipoimg integer NOT NULL,
    id_user integer NOT NULL,
    legenda_img character varying(100) COLLATE pg_catalog."default",
    file_img character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT images_users_pkey PRIMARY KEY (id_img),
    CONSTRAINT images_users_id_tipoimg_fkey FOREIGN KEY (id_tipoimg)
        REFERENCES public.images_type (id_imgtype) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT images_users_id_user_fkey FOREIGN KEY (id_user)
        REFERENCES public.users (id_user) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.images_users
    OWNER to floopapp;

-- Table: public.log_sistema

-- DROP TABLE public.log_sistema;

CREATE TABLE public.log_sistema
(
    datalog timestamp without time zone DEFAULT now(),
    logtxt text COLLATE pg_catalog."default",
    id_user integer NOT NULL,
    CONSTRAINT log_sistema_id_user_fkey FOREIGN KEY (id_user)
        REFERENCES public.users (id_user) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.log_sistema
    OWNER to floopapp;



INSERT INTO public.userstype(id_usertype,descr_usertype, is_admin) VALUES (1,'Administrador', TRUE);
INSERT INTO public.userstype(id_usertype,descr_usertype, is_admin) VALUES (2,'Parceiro', FALSE);


INSERT INTO public.planos(descr_plano)VALUES ('Ouro');
INSERT INTO public.planos(descr_plano)VALUES ('Prata');
INSERT INTO public.planos(descr_plano)VALUES ('Bronze');

INSERT INTO public.periodicidade(descr_periodo, qtdias_periodo)	VALUES ('mensal', 30);
INSERT INTO public.periodicidade(descr_periodo, qtdias_periodo)	VALUES ('trimestral', 90);
INSERT INTO public.periodicidade(descr_periodo, qtdias_periodo)	VALUES ('semestral', 180);



insert into categorias (descr_categoria) values ('Academia');
insert into categorias (descr_categoria) values ('aeroporto');
insert into categorias (descr_categoria) values ('aquário');
insert into categorias (descr_categoria) values ('balada');
insert into categorias (descr_categoria) values ('banco');
insert into categorias (descr_categoria) values ('bar');
insert into categorias (descr_categoria) values ('biblioteca');
insert into categorias (descr_categoria) values ('bicicletaria');
insert into categorias (descr_categoria) values ('boliche');
insert into categorias (descr_categoria) values ('café');
insert into categorias (descr_categoria) values ('camping e alojamento');
insert into categorias (descr_categoria) values ('cartório');
insert into categorias (descr_categoria) values ('chaveiro');
insert into categorias (descr_categoria) values ('cinema');
insert into categorias (descr_categoria) values ('correio');
insert into categorias (descr_categoria) values ('dentista');
insert into categorias (descr_categoria) values ('danceteria');
insert into categorias (descr_categoria) values ('escola');
insert into categorias (descr_categoria) values ('estação de metrô');
insert into categorias (descr_categoria) values ('estacionamento');
insert into categorias (descr_categoria) values ('estádio');
insert into categorias (descr_categoria) values ('evento temporário');
insert into categorias (descr_categoria) values ('farmácia');
insert into categorias (descr_categoria) values ('hospital');
insert into categorias (descr_categoria) values ('hotel');
insert into categorias (descr_categoria) values ('igreja');
insert into categorias (descr_categoria) values ('lava rápido');
insert into categorias (descr_categoria) values ('livraria');
insert into categorias (descr_categoria) values ('lojas');
insert into categorias (descr_categoria) values ('lotéricas');
insert into categorias (descr_categoria) values ('museu');
insert into categorias (descr_categoria) values ('oficina mecânica');
insert into categorias (descr_categoria) values ('padaria');
insert into categorias (descr_categoria) values ('parque de diversões');
insert into categorias (descr_categoria) values ('polícia');
insert into categorias (descr_categoria) values ('posto de gasolina');
insert into categorias (descr_categoria) values ('prefeitura');
insert into categorias (descr_categoria) values ('restaurante');
insert into categorias (descr_categoria) values ('rodoviária');
insert into categorias (descr_categoria) values ('ponto de ônibus');
insert into categorias (descr_categoria) values ('salão de beleza');
insert into categorias (descr_categoria) values ('shopping');
insert into categorias (descr_categoria) values ('supermercado');
insert into categorias (descr_categoria) values ('teatro');
insert into categorias (descr_categoria) values ('universidade');
insert into categorias (descr_categoria) values ('veterinário');
insert into categorias (descr_categoria) values ('zoológico');



INSERT INTO public.images_type(descr_imgtype) VALUES ('Imagem do local');
INSERT INTO public.images_type(descr_imgtype) VALUES ('Imagem do cardápio');
INSERT INTO public.images_type(descr_imgtype) VALUES ('Imagem da promoção');
INSERT INTO public.images_type(descr_imgtype) VALUES ('Imagem do evento');

INSERT INTO public.anuncios_status(descr_statusanuncio) VALUES ('Pendente');
INSERT INTO public.anuncios_status(descr_statusanuncio) VALUES ('Publicado');

ALTER TABLE users_files ALTER COLUMN id_file SET DEFAULT nextval('id_file_seq');



CREATE TABLE public.eventos
(
    id_evento integer NOT NULL,
    complemento character varying(250) COLLATE pg_catalog."default",
    data character varying(15) COLLATE pg_catalog."default",
    hora character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT eventos_pkey PRIMARY KEY (id_evento)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.eventos
    OWNER to flopeteste;
