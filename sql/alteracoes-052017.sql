-- deletando os períodos trimestral e semestral do plano bronze
delete from periodo_plano where id_periodo in (5,6) and id_plano = 3;

   CREATE TABLE public.users_files
   (
       id_user bigint NOT NULL,
       type_file bigint NOT NULL,
       path_file character varying(250) COLLATE pg_catalog."default" NOT NULL,
       sequence bigint NOT NULL,
       url_file character varying(250) COLLATE pg_catalog."default",
       CONSTRAINT fk_id_user_users FOREIGN KEY (id_user)
           REFERENCES public.users (id_user) MATCH SIMPLE
           ON UPDATE NO ACTION
           ON DELETE NO ACTION
   )
   WITH (
       OIDS = FALSE
   )
   TABLESPACE pg_default;

   ALTER TABLE public.users_files
       OWNER to floopapp;
   ALTER TABLE public.anuncios
      ADD COLUMN siteemp_user character varying(100) COLLATE pg_catalog."default";
   -- Atualizar no servidor de teste
   ALTER TABLE public.users_files
         ADD COLUMN url_firebase character varying(250) COLLATE pg_catalog."default";
   ALTER TABLE public.userdetail
             ADD COLUMN key_firebase character varying(100) COLLATE pg_catalog."default";
 alter table userdetail
	add column lnkcomprar_user varchar(100);

alter table userdetail
	add column promocao_user varchar(250);

   CREATE TABLE public.agenda_eventos
(
    id_user integer NOT NULL,
    data date NOT NULL,
    hora character varying(10) COLLATE pg_catalog."default",
    descricao character varying(300) COLLATE pg_catalog."default",
    complemento character varying(300) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.agenda_eventos OWNER to floopapp;
ALTER TABLE public.users_files ADD COLUMN legenda character varying(250) COLLATE pg_catalog."default";

alter table categorias add column descr_categoria_ing varchar(100);
insert into categorias (descr_categoria, descr_categoria_ing) values ('estabelecimento','establishment');

alter table anuncios add column lnkreservamesa_user varchar(250);
alter table anuncios add column lnkfacebook_user varchar(250);
alter table anuncios add column lnkyoutube_user varchar(250);
alter table anuncios add column lnktwitter_user varchar(250);
alter table anuncios add column lnkinstagram_user varchar(250);
alter table anuncios add column lnkgmais_user varchar(250);
alter table anuncios add column lnkcomprar_user varchar(250);


CREATE TABLE public.horario_funcionamento
(
    dia character varying(20) COLLATE pg_catalog."default",
    fechado boolean,
    horarioinicio character varying(10) COLLATE pg_catalog."default",
    horariofim character varying(10) COLLATE pg_catalog."default",
    sequence integer,
    id_anuncio integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.horario_funcionamento
   OWNER to flopeteste;


alter table users_files
   add column path_firebase varchar(100);

alter table users
  add column hash_recuperar varchar(100);
