<?php
if (!defined('BASEPATH'))
	exit('Acesso negado.');

class Libfloop {
	private $CI;
	public function __construct() {
		$this -> CI = &get_instance();
	}

	function user_logged_in() {
		$retorno = FALSE;
		if ($this -> CI -> session -> userdata('isLoggedIn')) {
			$retorno = TRUE;
		}
		return $retorno;
	}

	function user_is_admin() {
		return $this -> CI -> users_model -> user_isadmin();
	}

	function get_path_img_user() {
		return FCPATH . DEFAULT_USER_IMAGES_PATH . $this -> CI -> session -> keyuser . DIRECTORY_SEPARATOR;
	}

	function get_url_img_user() {
		return base_url(DEFAULT_USER_IMAGES_URL . $this -> CI -> session -> keyuser . '/');
	}

}
