<?php
use Google\Cloud\Storage\StorageClient;
use League\Flysystem\Filesystem;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class Firebasestorage {
    public $storageClient;
    public $bucket;
    public $bucketName;
    public $adapter;
    public $filesystem;
/**
 * The credentials will be auto-loaded by the Google Cloud Client.
 *
 * 1. The client will first look at the GOOGLE_APPLICATION_CREDENTIALS env var.
 *    You can use ```putenv('GOOGLE_APPLICATION_CREDENTIALS=/path/to/service-account.json');``` to set the location of your credentials file.
 *
 * 2. The client will look for the credentials file at the following paths:
 * - windows: %APPDATA%/gcloud/application_default_credentials.json
 * - others: $HOME/.config/gcloud/application_default_credentials.json
 *
 * If running in Google App Engine, the built-in service account associated with the application will be used.
 * If running in Google Compute Engine, the built-in service account associated with the virtual machine instance will be used.
 */

// $storageClient = new StorageClient([
//     'projectId' => 'teste-7ec8c',
// ]);
// $bucket = $storageClient->bucket('new_bucket');
//
// $adapter = new GoogleStorageAdapter($storageClient, $bucket);
//
// $filesystem = new Filesystem($adapter);

/**
*/
 // * The credentials are manually specified by passing in a keyFilePath.
function init(){
   $this->storageClient = new StorageClient([
    'projectId' => 'floopapp-672ae',
    'keyFilePath' => FCPATH.'FloopApp-80f3d413e469.json',
   //  'projectId' => 'teste-7ec8c',
   //  'keyFilePath' => FCPATH.'teste-d560bebe344f.json',
  ]);
  $this->bucketName = 'floopapp-672ae.appspot.com';
  $this->bucket = $this->storageClient->bucket($this->bucketName);
  // $this->bucket = $this->storageClient->bucket('teste-7ec8c.appspot.com');
  $this->adapter = new GoogleStorageAdapter($this->storageClient, $this->bucket);
  $this->filesystem = new Filesystem($this->adapter);
}


// // write a file
// $conteudo = fread ($file, filesize ('daenerys.jpg'));

//
// // update a file
// $filesystem->update('path/to/file.txt', 'new contents');

// read a file
public function upload($path_storage, $path_file){
  // echo $path_storage;
  // echo $path_file;
  $file_name = end(explode('/',$path_file));
  if(!$this->filesystem->has($path_storage.'/'.$file_name)){
     $contents = file_get_contents($path_file, FILE_BINARY);
     $this->filesystem->write($path_storage.'/'.$file_name, $contents);
   }

  return $path_storage.'/'.$file_name;
// // check if a file exists
// $exists = $filesystem->has('firebase.png');
//
// // delete a file
// $filesystem->delete('path/to/file.txt');
//
// // rename a file
// $filesystem->rename('filename.txt', 'newname.txt');
//
// // copy a file
// $filesystem->copy('filename.txt', 'duplicate.txt');
//
// // delete a directory
//
// $filesystem->deleteDir('path/to/directory');
  }

   public function publicUrl($path){
      $path = str_replace("/","%2F",$path);
      $public_url = "https://firebasestorage.googleapis.com/v0/b/".$this->bucketName."/o/".$path."?alt=media";
      return $public_url;
   }

  public function createDir($dir){
      $this->filesystem->createDir($dir);
  }

  public function deleteDir($dir){
   //   exit(0);
      $this -> adapter -> deleteDir($dir.'/images_cardapio');
  }

  public function deleteFile($path){
      if($this-> filesystem ->has($path)){
         $this -> filesystem -> delete($path);
      }
  }

}
