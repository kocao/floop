<?php
class Pagseguro{
  public $ch;
  public $email;
  public $token;

  function init(){

    // Inicia o cURL
    $this->ch = curl_init();
    $this->token = '21F821DDB57F4105A330DE864B8EE7B5';
    $this->email = '2fixcount@gmail.com';
   //  $this->token = 'F951EA9F548C4EC28772EFE785D98426';
   //  $this->email = 'nielbn@yahoo.com.br';

    curl_setopt($this->ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded; charset=utf8"));
    // Imita o comportamento patrão dos navegadores: manipular cookies
    curl_setopt ($this->ch, CURLOPT_COOKIEJAR, 'cookie.txt');
    // Define o tipo de transferência (Padrão: 1)
    curl_setopt ($this->ch, CURLOPT_RETURNTRANSFER, 1);
   //  curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
   //  curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  function request($requisicao){
    // Habilita o protocolo POST
    curl_setopt ($this->ch, CURLOPT_POST, 0);



    $url_request = "https://ws.pagseguro.uol.com.br/v2/checkout";
    curl_setopt($this->ch, CURLOPT_URL, $url_request);
    $email = $this->email;
    $curl_fields = "email=$email".
    "&token=$this->token".
    "&currency=BRL".
    "&itemId1=".$requisicao[0]['id'].
    "&itemDescription1=".$requisicao[0]['produto'].
    "&itemAmount1=".$requisicao[0]['valor'].
    "&itemQuantity1=1".
    "&reference=".$requisicao[0]['referencia'].
    "&senderEmail=".$requisicao[0]['email'].
    "&shippingType=1".
    "&shippingAddressStreet=".$requisicao[0]['endereco'].
    "&shippingAddressNumber=".$requisicao[0]['numero'].
    "&shippingAddressCity=".$requisicao[0]['cidade'];

    // Define os parâmetros que serão enviados (usuário e senha por exemplo)
    curl_setopt ($this->ch, CURLOPT_POSTFIELDS, $curl_fields);

    $content = curl_exec($this->ch);
    curl_close($this->ch);

    $sXML = $content;
    $oXML = new SimpleXMLElement($sXML);

    return $oXML;

  }


  function getStatusByRef($ref,$initialDate){
    $data = new DateTime($initialDate);
    $data = (string)$data->format('Y-m-d/H:i');
    $data = str_replace('/', 'T', $data);


    $url_request = "https://ws.pagseguro.uol.com.br/v2/transactions?";

    $curl_fields = "email=$this->email".
                   "&token=$this->token".
                   "&reference=$ref";
                  //  "&initialDate=$data";
    $url_request = $url_request.$curl_fields;


    curl_setopt($this->ch, CURLOPT_URL, $url_request);
    // Define os parâmetros que serão enviados (usuário e senha por exemplo)
    // curl_setopt ($this->ch, CURLOPT_POSTFIELDS, $curl_fields);
    $content = curl_exec($this->ch);

    curl_close($this->ch);

    $doc = simplexml_load_string($content);

    if ($doc) {
       $sXML = $content;
       $oXML = new SimpleXMLElement($sXML);
       return $oXML;
    } else {
       return false;
    }
  }

}
