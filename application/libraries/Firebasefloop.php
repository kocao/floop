<?php

class Firebasefloop {
  public $firebase;
  public $database;
  public $id;
  public $estrutura;

  function init($estrutura = "parceiros"){
      $fb = new Kreait\Firebase\Factory();
      // $this->firebase = $fb->withCredentials(FCPATH.'/teste-d560bebe344f.json')
      //     ->withDatabaseUri('https://teste-7ec8c.firebaseio.com')
         $this->firebase = $fb->withCredentials(FCPATH.'/FloopApp-80f3d413e469.json')
         ->withDatabaseUri('https://floopapp-672ae.firebaseio.com/')
          ->create();
      $this->database = $this->firebase->getDatabase();
      $this -> estrutura = $estrutura;
  }

  function insert($data){
      $newPost = $this->database
      ->getReference($this -> estrutura)
      ->push($data);
      return $newPost->getKey();
  }

  function deleteKey($key_firebase){
      $this -> estrutura;
      if(!empty($key_firebase)){
         $this->database->getReference($this -> estrutura ."/". $key_firebase)->remove();
      }
  }

  function update($id, $data){
      $this->database->getReference()->update([$this->estrutura.'/'.$id => $data]);
  }

  function addBlackList($place_id){
     $this->database->getReference()->update([$this->estrutura.'/'.$place_id => $place_id]);
  }

  function removeBlackList($place_id){
     if(!empty($place_id)){
        $this->database->getReference($this -> estrutura ."/". $place_id) -> remove();
     }
  }

  function getAll(){
    $blacklist = $this -> database -> getReference($this -> estrutura);
    if($blacklist -> getSnapshot() -> hasChildren()){
      return $blacklist -> getChildKeys();
    } else {
      return [];
    }
  }
}
