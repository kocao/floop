<?php
if (!defined('BASEPATH'))
	exit('Acesso negado.');

$config['site_name'] = 'Floop';
$config['site_description'] = 'Aplicativo para encontrar locais.';
$config['site_author'] = 'Hostpar Informática Ltda.';
$config['site_facebook'] = 'https://www.facebook.com/floopapp';
$config['site_twitter'] = 'https://twitter.com/floop_app';
$config['site_instagram'] = 'https://www.instagram.com/floop.app/';
$config['site_google'] = 'https://plus.google.com/103190274096977518432" rel="publisher"';
$config['theme'] = 'floop';
$config['dados'] = [];
$config['dados']['title_main'] = '';
$config['dados']['keywords'] = '';
$config['dados']['description'] = '';
$config['dados']['descricao'] = '';
$config['dados']['url_facebook'] = 'https://www.facebook.com/floop';
$config['dados']['url_twitter'] = 'https://twitter.com/floop';
$config['dados']['url_linkedin'] = 'http://www.linkedin.com/company/floop';
$config['dados']['code_googlean'] = '';

/*
 * usage:
 $this->config->item('encryption_key');
 $this->config->item('dados')['url_facebook'];
 $this->session->tipousr;
 */
