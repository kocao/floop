<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this -> grid_finan();
	}

	public function gravar() {
		if ($this -> libfloop -> user_is_admin() && $this -> input -> post('NOME_USER')) {
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_finan_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');;
			$dados['titulo_dash_view'] = 'Financeiro';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		}
	}

	public function editar() {
		if ($this -> libfloop -> user_is_admin() && $this -> uri -> segment(3)) {
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_finan_edit_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');;
			$dados['titulo_dash_view'] = 'Editar Financeiro';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		}
	}

	function grid_finan() {
		if ($this -> libfloop -> user_is_admin()) {
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_finan_view';
			$dados['rows'] = $this -> users_model -> get_rows_finan();
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');;
			$dados['titulo_dash_view'] = 'Financeiro';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

   function exportar_firebase() {
		  $key = $_GET['key'];
		  if($key == 'exportCron') {
				$autenticado = true;
			}
      if (!$this -> libfloop -> user_is_admin()) {

      if (!isset($_SERVER['PHP_AUTH_USER'])) {
          header('WWW-Authenticate: Basic realm="My Realm"');
          header('HTTP/1.0 401 Unauthorized');
          exit;
      } else if ( $_SERVER['PHP_AUTH_USER'] == 'floopapp' &&  $_SERVER['PHP_AUTH_PW'] == 'ppapoolf'){
         $autenticado = true;
      } else {
         header('WWW-Authenticate: Basic realm="My Realm"');
         header('HTTP/1.0 401 Unauthorized');
         exit;
      }
   } else {
      $autenticado = true;
   }

      if($autenticado){
         // Desativando anuncios vencidos
         $this -> anuncios_model -> desativar_vencidos();
         // Atualizando situação de pagamento no pagseguro
         $this -> financeiro_model -> atualiza_pendentes();

         // Atualizando anuncios de acordo com o financeiro
         $this -> anuncios_model -> atualiza_publicacao();
         $anuncios_publicar = $this -> anuncios_model -> anuncios_publicar();

         $firebasefloop = new Firebasefloop();
         $firebasefloop->init();

         $storage = new Firebasestorage();
         $storage->init();

         foreach ($anuncios_publicar as $key => $anuncio) {
            $id_user = $anuncio['id_user'];

            $user_data = $this -> users_model -> get_user_data($id_user);
            $key_firebase = $user_data -> key_firebase;

            // Se não existir uma referência no firebase cria uma nova
            if(empty($key_firebase)){
               $id = $firebasefloop->insert('null');
               $this->db->update('userdetail',['key_firebase' => $id], ['id_user' => $id_user]);
            } else {
               $id = $key_firebase;
            }

            // Deleta todos os arquivos do firebase
            $all_files = $this -> users_model -> get_all_files($id_user);
            foreach ($all_files as $key => $file) {
               if(!empty($file['path_firebase'])){
                  $storage->deleteFile($file['path_firebase']);
               }
            }

            // Pegando todos os arquivos por categoria
            $logo = $this->users_model->get_logo($id_user)[0];
            $cardapios = $this->users_model->get_cardapios($id_user);
            $locais = $this->users_model->get_locais($id_user);
            $promocoes = $this->users_model->get_promocoes($id_user);

            //Inserindo imagem do logo no firebase
            if ($logo) {
               $file = end(explode('/',$logo['path_file']));
               $path_firebase = 'parceiros/'.$id.'/images_marker/'.$file;
               $path_storage = $storage->upload('parceiros/'.$id.'/images_marker', $logo['path_file']);
               $url_firebase = $storage->publicUrl($path_storage);
               $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $logo['id_user'], 'type_file' => $logo['type_file'], 'id_file' => $logo['id_file']]);
            }

            // Inserindo as imagens de cardápio no firebase
            if ($cardapios) {
               foreach ($cardapios as $key => $cardapio) {
                  $file = end(explode('/',$cardapio['path_file']));
                  $path_firebase = 'parceiros/'.$id.'/images_cardapio/'.$file;
                  $path_storage = $storage->upload('parceiros/'.$id.'/images_cardapio', $cardapio['path_file']);
                  $url_firebase = $storage->publicUrl($path_storage);
                  $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $cardapio['id_user'], 'type_file' => $cardapio['type_file'], 'id_file' => $cardapio['id_file']]);

               }
            }

            // Inserindo as imagens de estabelecimentos no firebase
            if ($locais) {
               foreach ($locais as $key => $local) {
                  $file = end(explode('/',$local['path_file']));
                  $path_firebase = 'parceiros/'.$id.'/images_header/'.$file;
                  $path_storage = $storage->upload('parceiros/'.$id.'/images_header', $local['path_file']);
                  $url_firebase = $storage->publicUrl($path_storage);
                  $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $local['id_user'], 'type_file' => $local['type_file'], 'id_file' => $local['id_file']]);
               }
            }

            // Inserindo as Imagens de promoção no firebase
            if ($promocoes) {
               foreach ($promocoes as $key => $promocao) {
                  $file = end(explode('/',$promocao['path_file']));
                  $path_firebase = 'parceiros/'.$id.'/images_promocoes/'.$file;
                  $path_storage = $storage->upload('parceiros/'.$id.'/images_promocoes', $promocao['path_file']);
                  $url_firebase = $storage->publicUrl($path_storage);
                  $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $promocao['id_user'], 'type_file' => $promocao['type_file'], 'id_file' => $promocao['id_file']]);
               }
            }

            $dados = $this->users_model->montar_dados_firebase($id_user);
            $place_id = $dados['place_id'];
            unset($dados['place_id']);

            $key_firebase = $this->users_model->get_user_data($id_user)->key_firebase;
            $firebasefloop -> update($key_firebase, $dados);


            $firebasefloopblack = new Firebasefloop();
            $firebasefloopblack -> init("blacklist");

            $firebasefloopblack -> addBlackList($place_id);

            // Atualiza o anúncio para publicado
            $this -> db -> update('anuncios', ['id_statusanuncio' => 3], ['id_anuncio' => $anuncio['id_anuncio']]);

            // Deletando da blacklist os ids de anúncios que não estão mais ativos
            $this -> anuncios_model -> removeBlackList();

         }
      }
      if ($this -> libfloop -> user_is_admin()) {
         redirect(base_url('gerenciaranuncios'));
      }

   }

}
