<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('users_model');
	}

	public function index() {
		if ($this -> libfloop -> user_logged_in()) {
			redirect(base_url());
		} else {
			$this -> show_login();
		}
	}

	public function show_login() {
		$dados = [];
		$dados['file_view'] = 'login_view';
		$dados['prefix_menu'] = base_url();
		$this -> load -> view('default_view', $dados);
	}

	function login_user() {
		$this -> form_validation -> set_rules('email', 'E-mail', 'trim|required|max_length[250]|strtolower|valid_email');
		$this -> form_validation -> set_rules('password', 'Senha', 'required|max_length[20]');
		if ($this -> form_validation -> run() == TRUE) {
			$email = $this -> input -> post('email');
			$pass = $this -> input -> post('password');
         if($this -> users_model -> confirmacao_pendente($email)){
            $this -> session -> set_flashdata('msgerror', "Confirmação de e-mail pendente! Verifique a Caixa de Entrada do e-mail cadastrado no Floop. Se você não recebeu o e-mail de confirmação, verifique sua caixa de spam ou clique <a href='".base_url()."login/reenviar_confirmacao?email=$email"."'>aqui</a> para reenviarmos o e-mail.");
            $this -> show_login();
         }
			if ($this -> users_model -> validate_user($email, $pass)) {
				//if
				redirect(base_url('dashboard'));
			} else {
				$this -> session -> set_flashdata('msgerror', 'Usuário/Senha Inválido(s)!');
				$this -> show_login();
			}
		} else {
			$this -> show_login();
		}
	}

	function logoff() {
		if ($this -> libfloop -> user_logged_in()) {
			$this -> users_model -> logout_user();
			redirect(base_url());
		}
	}
	
	function reenviar_confirmacao(){
		$email = $_GET['email'];
		$user = $this -> db -> query ("select id_user, hash_confirmacao from users where email_user = '$email'")->result_array()[0];
		$id_user = $user['id_user'];
		$hash = $user['hash_confirmacao'];
		$this -> load -> library('email');
		$this -> email -> initialize();
		$this -> email -> subject('Confirmação de email');
		$this -> email -> from('suporte@floop.com.br', 'Floop', $this -> input -> post('email'));
		$this -> email -> to($email);
		$this -> email -> message('Clique no Link abaixo para confirmar o seu email.');
		$this -> email -> message("Clique no Link  para confirmar o seu email ".base_url()."login/confirmacao?hash=$hash");
		$this -> email -> send();
		$this -> session -> set_flashdata('msgdone',"A confirmação foi reenviada com sucesso! Para começar a utilizar o Floop, valide a conta através do e-mail de confirmação enviado para $email. \n Se você não recebeu o e-mail de confirmação, verifique sua caixa de spam ou clique <a href='".base_url()."login/reenviar_confirmacao?email=$email"."'>aqui</a> para reenviarmos o e-mail.");
		redirect(base_url('login'));
	}

	function create_user() {
		$this -> form_validation -> set_message('is_unique', '%s já cadastrado!');
		$this -> form_validation -> set_rules('name', 'Nome', 'trim|required|max_length[50]|ucfirst');
		$this -> form_validation -> set_rules('email', 'E-mail', 'trim|required|max_length[250]|strtolower|valid_email|is_unique[users.email_user]');
		$this -> form_validation -> set_rules('password', 'Senha', 'required|max_length[20]|min_length[6]');
		$this -> form_validation -> set_rules('password1', 'Repita a senha', 'required|max_length[20]|matches[password]');
		if ($this -> form_validation -> run() == TRUE) {
			$user_data = [];
			$user_data['nome_user'] = $this -> input -> post('name');
			$user_data['email_user'] = $this -> input -> post('email');
			$user_data['pws_user'] = $this -> encrypt -> encode($this -> input -> post('password'));
			$user_data['key_user'] = md5($this -> config -> item('encryption_key') . $this -> input -> post('email'));
			if ($this -> users_model -> create_user($user_data)) {
            $email = $user_data['email_user'];
            $hash = gerar_hash();
            $id_user = $this -> db -> query ("select id_user from users where email_user = '$email'")->result_array()[0]['id_user'];
            $this -> db -> update ('users', ['hash_confirmacao' => $hash], ['id_user' => $id_user]);
            $this -> load -> library('email');
            $this -> email -> initialize();
            $this -> email -> subject('Confirmação de email');
            $this -> email -> from('suporte@floop.com.br', 'Floop', $this -> input -> post('email'));
            $this -> email -> to($email);
            $this -> email -> message('Clique no Link abaixo para confirmar o seu email.');
            $this -> email -> message("Clique no Link  para confirmar o seu email ".base_url()."login/confirmacao?hash=$hash");
            $this -> email -> send();

						$this -> users_model -> validate_user($user_data['email_user'], $this -> input -> post('password'));
            $this -> session -> set_flashdata('msgdone',"Usuário criado com sucesso! Para começar a utilizar o Floop, valide a conta através do e-mail de confirmação enviado para $email. \n Se você não recebeu o e-mail de confirmação, verifique sua caixa de spam ou clique <a href='".base_url()."login/reenviar_confirmacao?email=$email"."'>aqui</a> para reenviarmos o e-mail.");
				    redirect(base_url('login'));
			}
		} else {
			$this -> show_login();
		}
	}

	function password_recovery() {
      $this->load->helper(['site_helper']);
      $hash = gerar_hash();
		if ($this -> libfloop -> user_logged_in()) {
			redirect(base_url('dashboard'));
		} else if ($this -> input -> post('email')) {
			$this -> form_validation -> set_rules('email', 'E-mail', 'trim|required|max_length[250]|strtolower|valid_email');
			if ($this -> form_validation -> run() == TRUE) {
				$email = $this -> input -> post('email');
				if ($this -> users_model -> exist_user($email)) {
               $id_user = $this -> db -> query ("select id_user from users where email_user = '$email'")->result_array()[0]['id_user'];
               $this -> db -> update ('users', ['hash_recuperar' => $hash], ['id_user' => $id_user]);
					$this -> load -> library('email');
					$this -> email -> initialize();
					$this -> email -> subject('Recuperação de senha');
					$this -> email -> from('suporte@floop.com.br', 'Floop', $this -> input -> post('email'));
					$this -> email -> to($email);
					$this -> email -> message('Sua senha é: ');
					$this -> email -> message('Recupere sua senha através do link:'.base_url()."Dadospessoais/recuperar_senha?hash=$hash");
					$this -> email -> send();
					$this -> session -> set_flashdata('msginfo', 'O link para alteração de senha foi enviado para o email informado!');
					$this -> show_login();
				} else {
					$this -> session -> set_flashdata('msgerror', 'E-mail Inválido!');
					$this -> forgot_password();
				}
			} else {
            $this -> session -> set_flashdata('msgerror', 'E-mail Inválido!');

				$this -> forgot_password();
			}
		} else {
         $this -> session -> set_flashdata('msgerror', 'E-mail Inválido!');

			$this -> forgot_password();
		}
	}

	function forgot_password() {
		$dados = [];
		$dados['file_view'] = 'forgot_password_view';
		$dados['prefix_menu'] = base_url();
		$this -> load -> view('default_view', $dados);
	}

   public function confirmacao(){
      $hash = $_GET['hash'];
      $query = $this -> db -> query ("select * from users where hash_confirmacao = '$hash' and confirmado = false");
      if($query){
         $id_user = $query -> result_array()[0]['id_user'];
         $this -> db -> update('users',['confirmado' => true], ['id_user' => $id_user]);
         $this -> session -> set_flashdata('msgdone','E-mail confirmado com sucesso! Você já pode começar a usar a sua conta Floop!');
         $this -> show_login();
      }
   }

}
