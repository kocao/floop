<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gerenciaranuncios extends CI_Controller {

	public function __construct() {
		parent::__construct();
      $this->load->helper(['site_helper']);
	}

	public function index() {
		$this -> grid_geren();
	}

	public function atualizar_plano() {
      if ($this -> libfloop -> user_is_admin()) {
         $id_user = $_POST['id_user'];
         $id_plano = $_POST['id_plano'];
         $dados = [];
         $dados['id_plano'] = $id_plano;
         if($id_plano == 3) {
            $dados['id_periodo'] =  4;
         }
         $dt_vcto = date_db($_POST['dt_vcto']);
         // $id_periodo = $_POST['id_periodo'];
         $this->db->update('userdetail',$dados, ['id_user' => $id_user] );
         $this->db->update('anuncios',['dt_vcto' => $dt_vcto], ['id_user' => $id_user] );
         redirect('gerenciaranuncios/grid_geren');
      } else {
         redirect(base_url());
      }
	}

	public function editar() {
      if ($this -> libfloop -> user_is_admin()) {
         $id_user = $_GET['id_user'];
         $row = $this -> users_model -> get_userdetail($id_user);
			$dados = [];
			$dados['file_view'] = 'dashboard/geren_anuncio/dash_edit_anuncio_view';
			$dados['userdetail'] = $row[0];
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');;
			$dados['titulo_dash_view'] = 'Alterar Anúncios';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

	function grid_geren() {
		if ($this -> libfloop -> user_is_admin()) {
			$dados = [];
			$dados['file_view'] = 'dashboard/geren_anuncio/dash_geren_anuncio_view';
			$dados['rows'] = $this -> users_model -> get_rows_anuncios_grid();
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');;
			$dados['titulo_dash_view'] = 'Gerenciar Anúncios';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

   public function desativar_anuncio(){
      if ($this -> libfloop -> user_is_admin()) {
         $id_anuncio = $_GET['id_anuncio'];
         $this -> anuncios_model -> desativar_anuncio($id_anuncio, null, true);
         redirect('gerenciaranuncios/grid_geren');
		} else {
			redirect(base_url());
		}
   }

}
