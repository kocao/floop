<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planosdeanuncios extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this -> grid_cad_planos();
	}

	public function gravar() {
		if ($this -> libfloop -> user_is_admin()) {
			if ($this -> input -> post('IDPLANO')) {
				$idplano = $this -> input -> post('IDPLANO');
			} else {
				$idplano = 0;
			}
			$this -> form_validation -> set_rules('DESCRICAO', 'Descrição', 'trim|required|max_length[50]|ucwords|xss_clean');
			$this -> form_validation -> set_rules('MAXDESCR_PLANO', 'Nro. máximo de caracteres', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('MAXFOTOS_PLANO', 'Nro. máximo de fotos', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('VALORES[' . $idplano . ']', 'Valor', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('URLSPAG[' . $idplano . ']', 'Url Pagto', 'trim|max_length[255]|valid_url|xss_clean');
			$this -> form_validation -> set_rules('MAXFOTOSCARDAPIO_PLANO', 'Nro. máximo de fotos de cardápio', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('MAXFOTOSEVENTOS_PLANO', 'Nro. máximo de fotos de eventos', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('MAXFOTOSPROMO_PLANO', 'Nro. máximo de fotos de promoções', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('ABREV_PLANO', 'Abreviatura do plano', 'trim|required|max_length[1]|strtoupper|xss_clean');
			$this -> form_validation -> set_rules('SHOWFACEBOOK_PLANO','Exibir link facebook', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWYOUTUBE_PLANO','Exibir link youtube', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWTWITTER_PLANO','Exibir link twitter', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWINSTAGRAM_PLANO','Exibir link instagram', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWGMAIS_PLANO','Exibir link G+', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWSITE_PLANO','Exibir link do site', 'xss_clean');
			$this -> form_validation -> set_rules('SHOWRESERVA_PLANO','Exibir link para reservas', 'xss_clean');

			//'callback_value_plan_check');
			if ($this -> form_validation -> run() == TRUE) {
				$valores = $this -> input -> post('VALORES');
				$urlspag = $this -> input -> post('URLSPAG');
				$dataplano = [];
				$dataplano['descr_plano'] = $this -> input -> post('DESCRICAO');
				$dataplano['maxdescr_plano'] = $this -> input -> post('MAXDESCR_PLANO');
				$dataplano['maxfotos_plano'] = $this -> input -> post('MAXFOTOS_PLANO');
				$dataplano['maxfotoscardapio_plano'] = $this -> input -> post('MAXFOTOSCARDAPIO_PLANO');
				$dataplano['maxeventos_plano'] = $this -> input -> post('MAXEVENTOS_PLANO');
				$dataplano['maxfotoseventos_plano'] = $this -> input -> post('MAXFOTOSEVENTOS_PLANO');
				$dataplano['maxfotospromo_plano'] = $this -> input -> post('MAXFOTOSPROMO_PLANO');
				$dataplano['abrev_plano'] = $this -> input -> post('ABREV_PLANO');
				$dataplano['showfacebook_plano'] = $this -> input -> post('SHOWFACEBOOK_PLANO');
				$dataplano['showyoutube_plano'] = $this -> input -> post('SHOWYOUTUBE_PLANO');
				$dataplano['showtwitter_plano'] = $this -> input -> post('SHOWTWITTER_PLANO');
				$dataplano['showinstagram_plano'] = $this -> input -> post('SHOWINSTAGRAM_PLANO');
				$dataplano['showgmais_plano'] = $this -> input -> post('SHOWGMAIS_PLANO');
				$dataplano['showsite_plano'] = $this -> input -> post('SHOWSITE_PLANO');
				$dataplano['showreserva_plano'] = $this -> input -> post('SHOWRESERVA_PLANO');
				$dataplano['showcomprar_plano'] = $this -> input -> post('SHOWCOMPRAR_PLANO');
				if ($this -> users_model -> create_upd_plano($dataplano, $valores, $urlspag, $idplano)) {
				}
			}
         $this -> session -> set_flashdata('msgdone', "Dados do plano gravados com sucesso!");
			$this -> show_cadastro($idplano);
		} else {
			$this -> grid_cad_planos();
		}
	}

	function value_plan_check($planval) {
		$retorno = TRUE;
		if (isset($planval)) {
			if (is_string($planval)) {
				$planval = str_replace($planval, ',', '.');
				if (is_numeric($planval)) {
					$planval = floatval($planval);
				}

			}
			/*
			 if (!is_numeric($planval)) {
			 $this -> form_validation -> set_message('value_plan_check', 'Os valores devem conter apenas dados numéricos!');
			 $retorno = FALSE;
			 }
			 *
			 */
		}
		return $retorno;

	}

	public function editar() {
		if ($this -> libfloop -> user_is_admin() && is_numeric($this -> uri -> segment(3))) {
			$this -> show_cadastro($this -> uri -> segment(3));
		} else {
			$this -> grid_cad_planos();
		}
	}

	function grid_cad_planos() {
		if ($this -> libfloop -> user_is_admin()) {
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_cad_planos_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
			$dados['rows'] = $this -> users_model -> get_rows_cad_planos();
			$dados['titulo_dash_view'] = 'Cadastro de Planos';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

	function show_cadastro($idplano = 0) {
		$datarow = $this -> users_model -> get_planos_data($idplano);
		if ($this -> libfloop -> user_is_admin() && $datarow) {
			$dados = [];
			$dados['idplano'] = $idplano;
			$dados['data_row'] = $datarow;
			$dados['data_vlr'] = $this -> users_model -> get_planos_vlr($idplano);
			$dados['file_view'] = 'dashboard/dash_cad_plan_edit_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
			$dados['titulo_dash_view'] = 'Editar Plano';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			$this -> grid_cad_planos();
		}
	}

}
