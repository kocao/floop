<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planos extends CI_Controller {
	 
	public function __construct(){
		parent::__construct();
	} 
	 	 
	public function index()
	{
		if( $this->session->userdata('isLoggedIn') ){
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_planos_view';
			$dados['prefix_menu'] = '';
			$dados['header_action'] = FALSE;
			$dados['titulo_dash_view'] = 'Detalhes do Plano';			
			$this->load->view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}
}
