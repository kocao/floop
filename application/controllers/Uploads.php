<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Uploads extends CI_Controller {
	function __construct() {
		parent::__construct();
		if (!$this -> libfloop -> user_is_admin()) {
			redirect(base_url('dashboard'));
		}
	}

	function index() {
		$data = array();
		if (!empty($_FILES)) {
			$uploadData = array();
			$tempFile = $_FILES['file']['tmp_name'];
			$fileName = $_FILES['file']['name'];
			$tamanho = $this -> input -> post('tamanho');
			$targetPath = DEFAULT_USER_IMAGES_PATH;
			$fileNameNew = $fileName;
			$filecont = 1;
			$path_parts = pathinfo($fileNameNew);
			while (file_exists(FCPATH . $targetPath . $fileNameNew)) {
				$fileNameNew = $path_parts['filename'] . '_' . $filecont . '.' . $path_parts['extension'];
				$filecont += 1;
			}
			$targetFile = $targetPath . $fileNameNew;
			//$this -> users_model -> create_log(['logtxt' => $targetFile]);
			move_uploaded_file($tempFile, $targetFile);
			//$this -> users_model -> create_log(['logtxt' => $tempFile]);
			
			//echo $targetFile;
			$uploadData[0]['nome_arquivo'] = $fileNameNew;
			$uploadData[0]['tamanho'] = $tamanho;
			if (!empty($uploadData)) {
				//$insert = $this -> Capa_model -> insert($uploadData);
			}
		}
		
		$selectrows = $this -> Capa_model -> getRows();
		$menu_final = $this -> Usuario_model -> menu();
		$data['pagetitle'] = "Upload de Capas";
		$data['breadcrumb'] = array('Capa', 'Seleção de Capas');
		$data['nome_usuario'] = $this -> session -> userdata('nome_usuario');
		$data['descricao_perfil'] = $this -> session -> userdata('descricao_perfil_usuario');
		$data['menu'] = $menu_final;
		$data['numeroRegistros'] = count($selectrows);
		$data['path_upload'] = DEFAULT_CAPA_PATH;
		$data['files'] = $selectrows;
		$this -> load -> view(NOME_VIEW_UPLOAD, $data);
		 
	}

}
