<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this -> grid_users();
	}

	public function gravar() {
		if ($this -> libfloop -> user_is_admin()) {
			if (is_numeric($this -> input -> post('iduser'))) {
				$iduser = $this -> input -> post('iduser');
			} else {
				$iduser = 0;
			}
			$this -> form_validation -> set_rules('selecttypeuser', 'Tipo do usuário', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('selectstatususer', 'Status do usuário', 'required|is_numeric|xss_clean');
			if ($this -> form_validation -> run() == TRUE) {
				$datauser = [];
				$datauser['id_usertype'] = $this -> input -> post('selecttypeuser');
				$datauser['ativo_inativo'] = $this -> input -> post('selectstatususer');
				if ($this -> users_model -> update_user($datauser, $iduser)) {
					$this -> session -> set_flashdata('msgdone', "Dados gravados com sucesso!");
				}
			}
			$this -> show_cadastro($iduser);
		}
	}

	public function editar() {
		if ($this -> libfloop -> user_is_admin() && is_numeric($this -> uri -> segment(3))) {
			$this -> show_cadastro($this -> uri -> segment(3));
		} else {
			$this -> grid_cad_planos();
		}
	}

	function grid_users() {
		if ($this -> libfloop -> user_is_admin()) {
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_users_view';
			$dados['rows'] = $this -> users_model -> get_rows_users();
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
			$dados['titulo_dash_view'] = 'Cadastro de Usuários';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

	function show_cadastro($iduser = 0) {
		$datarow = $this -> users_model -> get_other_user_data($iduser);
		if ($this -> libfloop -> user_is_admin() && $datarow) {
			$dados = [];
			$dados['iduser'] = $iduser;
			$dados['data_row'] = $datarow;
			$dados['file_view'] = 'dashboard/dash_user_edit_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
			$dados['titulo_dash_view'] = 'Editar Usuário';
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			$this -> grid_cad_planos();
		}
	}

   function delete_user(){
      $id_user = $_GET['id_user'];
      if ($this -> libfloop -> user_is_admin()) {
         $this -> users_model -> delete_user($id_user);
         $this -> index();
      } else {
         redirect(base_url());
      }
   }

}
