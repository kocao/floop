<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$dados = [];
		$dados['file_view'] = 'home_view';
		$dados['prefix_menu'] = '';
		$this->load->view('default_view', $dados);
		// $this->load->view('teste', $dados);
	}

	public function politicadeprivacidade(){
		$dados = [];
		$dados['file_view'] = 'privacidade';
		$dados['prefix_menu'] = '';
		$this->load->view('default_view', $dados);
	}

	public function termosdeuso(){
		$dados = [];
		$dados['file_view'] = 'termo';
		$dados['prefix_menu'] = '';
		$this->load->view('default_view', $dados);
	}

	public function ofloop(){
		$dados = [];
		$dados['file_view'] = 'floop';
		$dados['prefix_menu'] = '';
		$this->load->view('default_view', $dados);
	}
}
