<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamento extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
      if (!$this -> libfloop -> user_logged_in()) {
         redirect(base_url());
      }

      if( $this -> anuncios_model -> anuncio_publicado() ) {
         redirect('anuncios/meu_anuncio');
      }

		$dados = [];
		$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
		$dados['file_view'] = 'dashboard/dash_pagamento_view';
		$dados['titulo_dash_view'] = '4. Pagamento';
		$this->load->view('dashboard/default_dash_view', $dados);
	}

	public function plano_contratado()
	{
		$dados = [];
		$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
		$dados['file_view'] = 'dashboard/dash_plan_contratado_view.php';
		$dados['titulo_dash_view'] = '4. Pagamento';
		$this->load->view('dashboard/default_dash_view', $dados);
	}

   public function pagar(){
      $origem = $_POST['origem'];
      $valor = 0;
      $id_periodo = 0;
      $id_plano = 0;

      $id_user = $this -> users_model -> get_id_user();
      // $this -> users_model -> exportar_firebase($id_user);
      $pagseguro = new Pagseguro();
      $pagseguro->init();
      $dados_pagseguro = $this -> users_model -> get_dados_pagseguro();

      // atualizando valor do upgrade
      if($origem == 'upgrade'){
         $id_periodo = $_POST['selectplanpagper'];
         $id_plano = $_POST['selectplanpag'];
         $valor = $this -> anuncios_model -> valor_upgrade($id_plano, $id_periodo);
         $dados_pagseguro[0]['valor'] = $valor;
         $id_anuncio = $this -> anuncios_model -> get_anuncio()['id_anuncio'];
         $query = "select  'Floop Upgrade Plano ' || planos.descr_plano || ' (' || periodicidade.descr_periodo || ')' as produto from planos
         inner join periodo_plano
           on planos.id_plano = periodo_plano.id_plano
         inner join periodicidade
           on periodo_plano.id_periodo = periodicidade.id_periodo
         where periodo_plano.id_periodo = $id_periodo
           and planos.id_plano = $id_plano";
         $produto = $this -> db -> query($query) -> result_array()[0]['produto'];
         $dados_pagseguro[0]['produto'] = $produto;
         $this -> db -> update('anuncios',['id_statusanuncio' => 5], ['id_anuncio' => $id_anuncio]);
      }


      $financeiros = $this-> db -> query("select * from financeiro where id_user = $id_user");
      if($financeiros) {
         $financeiros = $financeiros -> result_array();
      }
      foreach ($financeiros as $key => $financeiro) {
         $this -> db -> update('financeiro', ['ativoinativo_finan' => 'I'], ['id_finan' => $financeiro['id_finan']]);
      }


      $dados = ['id_user' => $id_user, 'valor_finan' => $dados_pagseguro[0]['valor'], 'status_finan' => 0 , 'ativoinativo_finan' => 'A'];
      $this -> db -> insert ('financeiro', $dados);
      $id_finan = $this -> db -> insert_id();
      $dados_pagseguro[0]['referencia'] = "ref$id_finan";

      $xml = $pagseguro->request($dados_pagseguro);

      // Pegando data e hora atual
      $tz = 'America/Sao_Paulo';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $data_atual = $dt->format('Y-m-d H:i');


      $dados = ['descr_finan' => $dados_pagseguro[0]['produto'],
          'dtvecto_finan' => null,
          'valor_finan' => $dados_pagseguro[0]['valor'],
          'status_finan' => 0,
          'id_user' => $id_user,
          'ativoinativo_finan' => 'A',
          'dtrequisicao' => $data_atual,
          'id_plano' => $id_plano,
          'id_periodo' => $id_periodo,
          'code_url' => $xml->code,
          'transaction_code' => ''];

      // if ($this -> db -> insert ('financeiro', $dados)) {
      if ($this -> db -> update ('financeiro', $dados, ['id_finan' => $id_finan])) {
         redirect("https://pagseguro.uol.com.br/v2/checkout/payment.html?code=".$xml->code);
      } else {
         // var_dump($this -> db -> _error_message());
      }
   }

}
