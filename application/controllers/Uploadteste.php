<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Uploadteste extends CI_Controller
{
	function  __construct() {
		parent::__construct();
		
	}
	function index(){
		$data = array();
		if (!empty($_FILES)) {
			$uploadData = array();
			$tempFile = $_FILES['file']['tmp_name'];
			$fileName = $_FILES['file']['name'];
			$targetPath = DEFAULT_USER_IMAGES_PATH;
			$fileNameNew = $fileName;
			$filecont = 1;
			$path_parts = pathinfo($fileNameNew);
			while (file_exists(FCPATH.$targetPath . $fileNameNew)){
				$fileNameNew = $path_parts['filename'] . '_' . $filecont . '.' . $path_parts['extension'];
				$filecont += 1;
			} 
			$targetFile = $targetPath . $fileNameNew ;
			move_uploaded_file($tempFile, $targetFile);
			$uploadData[0]['nome_arquivo'] = $fileNameNew;
			if(!empty($uploadData)){
				//$insert = $this->Produto_model->insert($uploadData);
			}
		}
		/*
		$selectrows = $this->Produto_model->getRows();	
		$menu_final = $this->Usuario_model->menu();
		 * 
		 */
		$data['pagetitle'] = "Upload de Produtos";
		$data['breadcrumb'] = array('Produto', 'Seleção de Produtos');
		$data['nome_usuario'] = $this->session->userdata('nome_usuario');
		$data['descricao_perfil'] = $this->session->userdata('descricao_perfil_usuario');
		//$data['menu'] = $menu_final;
		$data['numeroRegistros'] = 8989;
		$data['path_upload'] = DEFAULT_USER_IMAGES_PATH;
        //$data['files'] = $selectrows;
		$this->load->view('upload_teste_view', $data);
	}
}