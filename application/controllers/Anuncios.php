<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anuncios extends CI_Controller {
	private $dataplano, $idimgtype;
   public $tipos_arquivo;

	public function __construct() {
		parent::__construct();
      $this->tipos_arquivo = ['logo' => 1, 'local' => 2, 'cardapio' => 3, 'promocao' => 4 ];
		$this -> dataplano = $this -> users_model -> get_plano_usuario();
      $this->load->model("Users_files_model");
      $this->load->model("anuncios_model");
      $this->load->helper(['site_helper']);
	}

	public function index() {
		$this -> show_cadastro($this -> users_model -> get_id_anuncio_usuario());
	}

	function show_cadastro($idanuncio = 0) {
		$plan_contratado_row = $this -> users_model -> get_row_user_plan();
      if ($this -> anuncios_model -> anuncio_publicado() && $plan_contratado_row -> abrev_plano == 'B') {
         redirect('anuncios/meu_anuncio');
      }
      if($this -> financeiro_model -> pagamento_pendente() && !$this -> anuncios_model -> anuncio_publicado()){
         redirect('pagamento');
      }
		if (!$plan_contratado_row) {
			redirect(base_url('dashboard'));
		} else {
			$iduser = $this -> users_model -> get_id_user();

			$dados = [];
			$dados['plan_contratado_row'] = $plan_contratado_row;
			$dados['file_view'] = 'dashboard/dash_anuncios_view';
			$dados['prefix_menu'] = '';
			$dados['header_action'] = TRUE;
         if( !$this -> anuncios_model -> anuncio_publicado() ) {
            $dados['titulo_dash_view'] = "3. Crie o seu anúncio";
         } else {
            $dados['titulo_dash_view'] = "Edite seu anúncio";
         }
			$dados['path_upload'] = $targetPath;
			$dados['files'] = $imagerows;
			$dados['dataplano'] = $this -> dataplano;
			$dados['idanuncio'] = $idanuncio;
			$dados['anunciorow'] = $this -> db -> where('id_anuncio', $idanuncio)->get('anuncios')->result_array();
			$dados['idtipoimg'] = $this -> idimgtype;
			$this -> load -> view('dashboard/default_dash_view', $dados);
		}
	}

	public function gravar() {
      $plan_contratado_row = $this -> users_model -> get_row_user_plan();

		if ($this -> input -> post('ID_ANUNCIO')) {
			$idanuncio = $this -> input -> post('ID_ANUNCIO');
		} else {
			$idanuncio = 0;
		}
      if ($this -> input -> post('ID_TIPOIMG')) {
          $this -> idimgtype = $this -> input -> post('ID_TIPOIMG');
      }

      // $this -> form_validation -> set_rules('siteemp_user', 'Site da empresa', 'trim|valid_url|required|max_length[255]|xss_clean');
		  $this -> form_validation -> set_rules("DESCR_ANUNCIO", "Descricao do anuncio", 'trim|required|max_length[' . $this -> dataplano -> maxdescr_plano . ']|ucwords|xss_clean');
      $this -> form_validation -> set_rules('lnkreservamesa_user', 'Link para reservas', 'trim|valid_url|max_length[255]|xss_clean');
      $this -> form_validation -> set_rules('lnkfacebook_user', 'Perfil do Facebook', 'trim|valid_url|max_length[255]|xss_clean');
      $this -> form_validation -> set_rules('lnkyoutube_user', 'Perfil do Youtube', 'trim|valid_url|max_length[255]|xss_clean');
      $this -> form_validation -> set_rules('lnktwitter_user', 'Perfil do Twitter', 'trim|valid_url|max_length[255]|xss_clean');
      $this -> form_validation -> set_rules('lnkinstagram_user', 'Perfil do Instagram', 'trim|valid_url|max_length[255]|xss_clean');
      $this -> form_validation -> set_rules('lnkgmais_user', 'Perfil do G+', 'trim|valid_url|max_length[255]|xss_clean');

      $dataanuncio = [];
      $dataanuncio['descr_anuncio'] = $this -> input -> post('DESCR_ANUNCIO');
      $dataanuncio['id_user'] = $this -> users_model -> get_id_user();
      $dataanuncio['id_statusanuncio'] = 2;
      $dataanuncio['siteemp_user'] = $this -> input -> post('siteemp_user');
      $dataanuncio['lnkreservamesa_user'] = $this -> input -> post('lnkreservamesa_user');
      $dataanuncio['lnkcomprar_user'] = $this -> input -> post('lnkcomprar_user');
      $dataanuncio['lnkfacebook_user'] = $this -> input -> post('lnkfacebook_user');
      $dataanuncio['lnkyoutube_user'] = $this -> input -> post('lnkyoutube_user');
      $dataanuncio['lnktwitter_user'] = $this -> input -> post('lnktwitter_user');
      $dataanuncio['lnkinstagram_user'] = $this -> input -> post('lnkinstagram_user');
      $dataanuncio['lnkgmais_user'] = $this -> input -> post('lnkgmais_user');

		if ($this -> form_validation -> run() == TRUE) {


         if ($this -> form_validation -> run() == TRUE) {
            if ($this->anuncios_model->insert_or_update($dataanuncio)) {
               $this->anuncios_model->insert_horarios($_POST);
               $this->anuncios_model->insert_eventos($_POST);
               if($this -> anuncios_model -> anuncio_publicado()){
                  $this -> users_model -> exportar_firebase($this -> users_model -> get_id_user());
                  $this -> session -> set_flashdata('msginfo', 'As informações foram atualizadas com sucesso!');
                  redirect('anuncios/meu_anuncio');
               }
               redirect(base_url('pagamento'));
            } else {
               $this -> session -> set_flashdata('msgerror', "Erro ao gravar anúncio!");
            }
         } else {
               $this -> session -> set_flashdata('msgerror', 'Erro ao gravar registro!');
         }

		}

      $anunciorow[0] = $dataanuncio;

      $dados = [];
      $dados['plan_contratado_row'] = $plan_contratado_row;
      $dados['file_view'] = 'dashboard/dash_anuncios_view';
      $dados['prefix_menu'] = '';
      $dados['header_action'] = TRUE;
      $dados['titulo_dash_view'] = "3. Crie o seu anúncio";
      $dados['path_upload'] = $targetPath;
      $dados['files'] = $imagerows;
      $dados['dataplano'] = $this -> dataplano;
      $dados['idanuncio'] = $idanuncio;
      $dados['anunciorow'] = $anunciorow;
      $dados['idtipoimg'] = $this -> idimgtype;
      $this -> load -> view('dashboard/default_dash_view', $dados);
	}

	public function gravar_logo() {
      $plan_contratado_row = $this -> users_model -> get_row_user_plan();

      $dataanuncio = [];
      $dataanuncio['descr_anuncio'] = $this -> input -> post('DESCR_ANUNCIO');
      $dataanuncio['id_user'] = $this -> users_model -> get_id_user();
      $dataanuncio['id_statusanuncio'] = 2;
      $dataanuncio['siteemp_user'] = $this -> input -> post('siteemp_user');
      $dataanuncio['lnkreservamesa_user'] = $this -> input -> post('lnkreservamesa_user');
      $dataanuncio['lnkcomprar_user'] = $this -> input -> post('lnkcomprar_user');
      $dataanuncio['lnkfacebook_user'] = $this -> input -> post('lnkfacebook_user');
      $dataanuncio['lnkyoutube_user'] = $this -> input -> post('lnkyoutube_user');
      $dataanuncio['lnktwitter_user'] = $this -> input -> post('lnktwitter_user');
      $dataanuncio['lnkinstagram_user'] = $this -> input -> post('lnkinstagram_user');
      $dataanuncio['lnkgmais_user'] = $this -> input -> post('lnkgmais_user');
			if ($this->anuncios_model->insert_or_update($dataanuncio)) {
				 $this->anuncios_model->insert_horarios($_POST);
				 $this->anuncios_model->insert_eventos($_POST);
				 redirect(base_url('anuncios'));
			}
  }

	function alterar_legenda() {
		if ($this -> input -> post('ID_IMG')) {
			$idimg = $this -> input -> post('ID_IMG');
			$imgdata['legenda_img'] = $this -> input -> post('LEGENDA_IMG');
			if ($this -> users_model -> upd_legenda_img($idimg, $imgdata)) {
				echo '1';
			} else {
				echo '2';
			}
		}
	}

	function deletar_imagem() {
      $id_user = $this -> users_model -> get_id_user();
      $id_file = $_POST['id_file'];

      $this -> db -> select('*');
		  $this -> db -> from('users_files');
		  $this -> db -> where(['id_file' => $id_file, 'id_user' => $id_user]);
      $file = $this -> db -> get() -> result_array()[0];
      $storage = new Firebasestorage();
      $storage->init();
      if(!empty($file['path_firebase'])){
        $storage->deleteFile($file['path_firebase']);
      }


		// $file = $this -> db -> query ("select * from users_files where id_file = $id_file and id_user = $id_user")-> get() -> result_array()[0];
      # Retirado por causa do cache de imagens da locaweb
      // $path = $file['path_file'];
		// if (file_exists($path)) {
		// 	unlink($path);
		// }
      $this -> db -> delete ('users_files', ['id_file' => $id_file, 'id_user' => $id_user]);
	}

   function add_evento(){
      $this -> load -> view ('dashboard/_evento');
   }

   function upload_file(){
      $targetPath = $this -> libfloop -> get_path_img_user();
      $type_file = "";
      $file = "";
      $datafile = [];
      $excedeu_limite = false;

      if (!file_exists($targetPath)) {
         mkdir($targetPath, 0777, true);
      }
   	$targetSalvos = $targetPath."salvos/";

      if (!file_exists($targetSalvos)) {
         mkdir($targetSalvos, 0777, true);
      }

		while ($row = current($_FILES)) {
				$type_file = key($_FILES);
            switch ($type_file) {
               case 'local' :
                  $total_permitido = $this -> dataplano -> maxfotos_plano;
                  $total_gravado = count($this -> users_model -> get_locais());
                  break;
               case 'cardapio' :
                  $total_permitido = $this -> dataplano -> maxfotoscardapio_plano;
                  $total_gravado = count($this -> users_model -> get_cardapios());
                  break;
               case 'promocao' :
                  $total_permitido = $this -> dataplano -> maxfotospromo_plano;
                  $total_gravado = count($this -> users_model -> get_promocoes());
                  break;
               case 'logo' :
                  $total_permitido = 1;
                  $total_gravado = count($this -> users_model -> get_logo());
                  break;
               default :
                  $total_permitido = 0;
                  $total_gravado = 0;
                  break;
            }
            if($total_gravado >= $total_permitido){
               $excedeu_limite = true;
               break;
            }

				if (isset($row['name']) && $row['name'] != '') {
						$config['allowed_types']        = 'gif|jpg|png|jpeg';
                  $config['file_name']             = $type_file;
						// $config['max_size']             = 5000;
						// $config['max_width']            = 1024;
						// $config['max_height']           = 768;
						$config['upload_path'] = $targetSalvos;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload($type_file))
						{
                     $errors = $this->upload->display_errors();
							$this -> session -> set_flashdata('msgerror', $errors);
						}
						else
						{
							$data = $this->upload->data();
                     $id_user = $this -> users_model -> get_id_user();

                     // function redimensiona_foto($tmp, $arquivo, $max_x, $max_y, $pasta){
                     $tmp = $data['file_path'].$data['file_name'];
                     if($type_file == 'logo'){
                        $this -> anuncios_model -> redimensiona_foto($tmp, $data['file_name'], 600, 430, $data['file_path']);
                     }

                     $datafile['id_user'] = $this -> users_model -> get_id_user();
                     $datafile['type_file'] = $this->tipos_arquivo[$type_file];
                     $datafile['path_file'] = $data['file_path'].$data['file_name'];
                     $datafile['url_file'] = $this -> libfloop ->  get_url_img_user() . 'salvos/'.$data['file_name'];
                     $datafile['legenda'] = $_POST["legenda"];
                     $this->db->insert('users_files', $datafile);
                     $file_id = $this->db->insert_id();
						}
				}

				next($_FILES);
		}
      if($excedeu_limite){
         echo "false";
      } else {
         $this -> load -> view('dashboard/_item_galeria', ['url_file' => $datafile['url_file'], 'id_file' => $file_id, 'tipo' => $type_file, 'legenda' => $_POST['legenda']]);
      }
   }
// 	 function redimensiona_index(){
// 			$url_logo = $this -> users_model -> get_logo()[0]["url_file"];

// 		  $dados = [];
// 			$dados['file_view'] = 'dashboard/crop_logo';
// 			$dados['prefix_menu'] = '';
// 			$dados['header_action'] = TRUE;
//       $dados['titulo_dash_view'] = "Redimensionar Logo";
// 			$dados['url_logo'] = $url_logo;
// 			$this -> load -> view('dashboard/default_dash_view', $dados);


// 	 }

	 function redimensiona_logo(){
		 $logo = $this -> users_model -> get_logo();
		 $path_file = $logo['path_file'];
		 $coords = $_POST['coords'];
		 $this -> anuncios_model -> redimensiona_logo($coords);

		 redirect('anuncios');
	 }

   function meu_anuncio(){
      $dados = [];
      $dados['file_view'] = 'dashboard/dash_meu_anuncio';
      $dados['prefix_menu'] = '';
      $dados['titulo_dash_view'] = "Meu Anúncio";
      $dados['header_action'] = TRUE;
      $dados['idanuncio'] = $idanuncio;
      // $dados['anunciorow'] = $this -> db -> where('id_anuncio', $idanuncio)->get('anuncios')->result_array();
      $this -> load -> view('dashboard/default_dash_view', $dados);
   }
}
