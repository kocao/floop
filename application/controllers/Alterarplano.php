<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alterarplano extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if ($this -> libfloop -> user_logged_in()) {
         if($this -> financeiro_model -> pagamento_pendente()){
            redirect('pagamento');
         }

         if( $this -> anuncios_model -> anuncio_publicado() ) {
            redirect('anuncios');
         }

			$dados = [];
			$dados['file_view'] = 'dashboard/dash_alterar_plano_view';
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
			$dados['titulo_dash_view'] = '2. Escolha o plano';

			//$plan_contratado_row = $this -> users_model -> get_row_user_plan();
			//if (!$plan_contratado_row) {
			//	redirect(base_url('dashboard'));
			//} else {
			$this -> load -> view('dashboard/default_dash_view', $dados);
			//}
		} else {
			redirect(base_url());
		}
	}

	public function get_periodos(){
		if (empty($_POST['id_plano'])) {
			$id_plano = null;
		} else {
			$id_plano = $_POST['id_plano'];
		}

		$periodos = $this -> users_model -> get_planos_vlr($id_plano);
		$dados['periodos'] = $periodos;
		$dados['id_periodo_user'] = 0;


		$this -> load -> view('dashboard/_periodos_select', $dados);
	}

	public function get_preco(){
		$idplano = $_POST['id_plano'];
		$idperiodo = $_POST['id_periodo'];
		$valor =	$this -> users_model -> get_vlrplan_by_idper($idplano, $idperiodo);
		echo "R$ ".$valor;
	}

	public function gravarplano() {
		if ($this -> libfloop -> user_logged_in()) {
			$this -> form_validation -> set_rules('selectplanpag', 'Plano', 'trim|required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('selectplanpagper', 'Período', 'trim|required|is_numeric|xss_clean');
			if ($this -> form_validation -> run() == TRUE) {
				$userdata = [];
				$userdata['id_plano'] = $this -> input -> post('selectplanpag');
				$userdata['id_periodo'] = $this -> input -> post('selectplanpagper');
				if ($this -> users_model -> update_user_plan($userdata)) {
					redirect(base_url('anuncios'));
				} else {
					$this -> session -> set_flashdata('msgerror', 'Informe os dados pessoais!');
				}
			}
		}

		$dados = [];
		$dados['file_view'] = 'dashboard/dash_alterar_plano_view';
		$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
		$dados['titulo_dash_view'] = '2. Escolha o plano';

		$this -> load -> view('dashboard/default_dash_view', $dados);

	}

   function upgrade_plano(){
      $plano_usuario = $this -> users_model -> get_row_user_plan();
      $id_periodo = $plano_usuario -> id_periodo;
      $periodos = $this -> db -> query("SELECT * FROM periodicidade where id_periodo >= $id_periodo")-> result_array();
      if($plano_usuario -> id_plano == 2) {
         $planos = $this -> db -> query("SELECT * FROM planos where id_plano not in(3,2)")-> result_array();
      } else {
         $planos = $this -> db -> query("SELECT * FROM planos where id_plano not in(3)")-> result_array();
      }

      $dados = [];
      $dados['planos'] = $planos;
      $dados['periodos'] = $periodos;
      $dados['file_view'] = 'dashboard/dash_upgrade_plano_view';
      $dados['header_action'] = $this -> input -> post('HEADER_ACTION');
      $dados['titulo_dash_view'] = 'Upgrade de plano';

      $this -> load -> view('dashboard/default_dash_view', $dados);
   }

   function valor_upgrade(){
      $id_plano = $_POST['id_plano'];
      $id_periodo = $_POST['id_periodo'];
      $valor_upgrade = $this -> anuncios_model -> valor_upgrade($id_plano, $id_periodo);
      echo $valor_upgrade;
   }


}
