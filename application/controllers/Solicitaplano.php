<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitaplano extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if ($this -> libfloop -> user_logged_in()) {
			if (is_numeric( $this -> uri -> segment(3)) && is_numeric($this -> uri -> segment(4)) ){
				$lsession = [];
				$lsession['idplano'] = $this -> uri -> segment(3);
				$lsession['idperiodo'] = $this -> uri -> segment(4);
				$this -> session -> set_userdata($lsession);
			}
			redirect(base_url('Dashboard'));
		} else {
			redirect(base_url('Login'));
		}
	}

}
