<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if ($this -> libfloop -> user_logged_in()) {
			/*
			$dados = [];
			$dados['file_view'] = 'dashboard/dash_home_view';
			$dados['prefix_menu'] = '';
			$dados['header_action'] = TRUE;
			$dados['titulo_dash_view'] = 'Área do Anunciante';
			$this -> load -> view('dashboard/default_dash_view', $dados);
			 * 
			 */
			redirect('dadospessoais');
		} else {
			redirect(base_url());
		}
	}

}
