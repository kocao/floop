<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dadospessoais extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		if ($this -> libfloop -> user_logged_in()) {
         if($this -> financeiro_model -> pagamento_pendente() && !$this -> anuncios_model -> anuncio_publicado()){
            redirect('pagamento');
         }
			$this -> show_cadastro();
		} else {
			redirect(base_url());
		}
	}

   public function recuperar_senha() {
      $this->load->helper(['site_helper']);
      $hash = $_GET['hash'];

      $user = $this->db->query("select id_user, hash_recuperar from users where hash_recuperar = '$hash'")->result_array()[0];
      if ( !empty($hash) && $hash == $user['hash_recuperar'] ) {
         $dados = [];
         $dados['file_view'] = 'dashboard/dash_alterar_senha_view';
         $dados['user'] = $user;
         $dados['prefix_menu'] = '';
         $this -> load -> view('default_view', $dados);
      } else {
         redirect(base_url());
      }
   }

   public function alterar_senha(){
   	$this -> form_validation -> set_rules('PWS_USER1', 'Senha', 'required|max_length[20]|min_length[6]|xss_clean');
   	$this -> form_validation -> set_rules('PWS_USER2', 'Repita a senha', 'required|max_length[20]|matches[PWS_USER1]|xss_clean');

      $user = $this->db->query("select id_user, hash_recuperar from users where hash_recuperar = '".$_POST['hash_recuperar']."'")->result_array()[0];
      if ( !empty($_POST['hash_recuperar']) && $_POST['hash_recuperar'] == $user['hash_recuperar'] ) {
         if ($this -> form_validation -> run() == TRUE ) {
            $userdata['pws_user'] = $this -> encrypt -> encode($this -> input -> post('PWS_USER1'));
            $userdata['hash_recuperar'] = null;
            $this -> db -> update('users', $userdata, ['id_user' => $_POST['id_user']]);
            $this -> session -> set_flashdata('msginfo', 'A senha foi alterada com sucesso!');
            redirect('Login/show_login');
         } else {
            $dados = [];
            $dados['file_view'] = 'dashboard/dash_alterar_senha_view';
            $dados['user'] = $user;
            $dados['prefix_menu'] = '';
   			$this -> load -> view('default_view', $dados);
         }
      }else {
         redirect(base_url());
      }
   }


   public function recuperar_senha_logado() {

      if ($this -> libfloop -> user_logged_in()) {
         $user_model = $this -> users_model -> get_user_data();
         $user['id_user'] = $user_model -> id_user;
         $dados = [];
         $dados['file_view'] = 'dashboard/dash_alterar_senha_view_logado';
         $dados['user'] = $user;
         $dados['titulo_dash_view'] = 'Alterar Senha';
         $dados['prefix_menu'] = '';
         $this -> load -> view('dashboard/default_dash_view', $dados);
      } else {
         redirect(base_url());
      }
   }

   public function alterar_senha_logado(){
      $this -> form_validation -> set_rules('PWS_USER1', 'Senha', 'required|max_length[20]|min_length[6]|xss_clean');
      $this -> form_validation -> set_rules('PWS_USER2', 'Repita a senha', 'required|max_length[20]|matches[PWS_USER1]|xss_clean');

      if ($this -> libfloop -> user_logged_in()) {
         if ($this -> form_validation -> run() == TRUE ) {
            $userdata['pws_user'] = $this -> encrypt -> encode($this -> input -> post('PWS_USER1'));
            $userdata['hash_recuperar'] = null;
            $this -> db -> update('users', $userdata, ['id_user' => $_POST['id_user']]);
            $this -> session -> set_flashdata('msginfo', 'A senha foi alterada com sucesso!');
            redirect('Dadospessoais/index');
         } else {
            $user_model = $this -> users_model -> get_user_data()->result_array();
            $user['id_user'] = $user_model -> id_user;
            $dados = [];
            $dados['file_view'] = 'dashboard/dash_alterar_senha_view_logado';
            $dados['user'] = $user;
            $dados['prefix_menu'] = '';
            $this -> load -> view('dashboard/default_dash_view', $dados);
         }
      }else {
         redirect(base_url());
      }
   }


	function show_cadastro() {
		$data_row = $this -> users_model -> get_user_data();
      $plan_contratado_row = $this -> users_model -> get_row_user_plan();


		if ($this -> libfloop -> user_logged_in()) {
         if($this -> financeiro_model -> pagamento_pendente()){
            redirect('pagamento');
         }
			$dados = [];
			if ($this -> input -> post('HEADER_ACTION')) {
				$dados['file_view'] = 'dashboard/dash_home_view';
			} else {
				$dados['file_view'] = 'dashboard/dash_dados_view';
			}
			$dados['prefix_menu'] = '';
			$dados['data_row'] = $data_row;
         $dados['plan_contratado_row'] = $plan_contratado_row;
			$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
         if( !$this -> anuncios_model -> anuncio_publicado() ) {
			  $dados['titulo_dash_view'] = '1. Preencha seus dados';
         } else {
            $dados['titulo_dash_view'] = 'Dados Pessoais';
         }
         if($this -> anuncios_model -> anuncios_desativado()){
            $this -> session -> set_flashdata('msgerror', 'Ops! Lamentamos informar que o seu anúncio foi desativado. Preencha os passos a seguir para anunciar novamente no Floop. Qualquer dúvida, entre em contato com a nossa equipe.');
         }
         if($this -> anuncios_model -> anuncios_vencido()){
            $this -> session -> set_flashdata('msgerror', 'Ops! Infelizmente o seu plano venceu e o seu anúncio foi desativado. Deseja anunciar novamente no Floop? Preencha os passos a seguir!');
         }
         $anuncio = $this -> anuncios_model -> get_anuncio();
         if($anuncio ){
            $data_user = $this -> users_model -> get_user_data();

            if( $data_user -> upgrade == "t"){
               $plano = $this -> users_model -> get_row_user_plan();
               $descr_plano = $plano -> descr_plano;
               $id_user =  $anuncio['id_user'];
               $retorno = $this -> db -> update('userdetail',['upgrade' => false]);
               $this -> session -> set_flashdata('msgdone', "Parabéns! Seu upgrade foi realizado com sucesso! Agora você é cota $descr_plano e possui novas opções disponíveis para seu anúncio. Clique <a href='anuncios'>aqui</a> para atualizar seu anúncio ou dirija-se à aba Editar Anúncio.");
            }
         }
			$this -> load -> view('dashboard/default_dash_view', $dados);
		} else {
			redirect(base_url());
		}
	}

	public function gravar() {
		$userdatadetail = [];



		if ($this -> libfloop -> user_logged_in() && $this -> input -> post('NOME_USER')) {
			$this -> form_validation -> set_message('is_unique', '%s já cadastrado!');
			$this -> form_validation -> set_rules('NOME_USER', 'Nome', 'trim|required|max_length[50]|ucwords|xss_clean');
			$this -> form_validation -> set_rules('CNPJ_USER', 'CNPJ', 'trim|required|max_length[18]|xss_clean');
			$this -> form_validation -> set_rules('NOMEEMPRESA_USER', 'Nome da empresa', 'trim|required|max_length[50]|ucwords|xss_clean');
			$this -> form_validation -> set_rules('ENDEREMP_USER', 'Endereço', 'trim|required|max_length[300]|ucfirst|xss_clean');
			$this -> form_validation -> set_rules('ID_CATEGORIA', 'Categoria', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('FONECOM_USER', 'Telefone comercial', 'trim|required|max_length[20]|xss_clean');
			$this -> form_validation -> set_rules('FONECEL_USER', 'Telefone celular', 'trim|required|max_length[20]|xss_clean');
			$this -> form_validation -> set_rules('LATITUDE_USER', 'Latitude', 'required|is_numeric|xss_clean');
			$this -> form_validation -> set_rules('LONGITUDE_USER', 'Longitude', 'required|is_numeric|xss_clean');

			$userdatadetail = [];
			$userdatadetail['cnpj_user'] = $this -> input -> post('CNPJ_USER');
			$userdatadetail['nomeempresa_user'] = $this -> input -> post('NOMEEMPRESA_USER');
			$userdatadetail['enderemp_user'] = $this -> input -> post('ENDEREMP_USER');
			$userdatadetail['id_categoria'] = $this -> input -> post('ID_CATEGORIA');
			$userdatadetail['fonecom_user'] = $this -> input -> post('FONECOM_USER');
			$userdatadetail['fonecel_user'] = $this -> input -> post('FONECEL_USER');
			$userdatadetail['latitude_user'] = floatval($this -> input -> post('LATITUDE_USER'));
			$userdatadetail['longitude_user'] = floatval($this -> input -> post('LONGITUDE_USER'));
			$userdatadetail['place_id'] = $this -> input -> post('place_id');
			$userdatadetail['termo'] = $this -> input -> post('termo');
			$userdatadetail['privacidade'] = $this -> input -> post('privacidade');

         if ($this -> form_validation -> run() == TRUE) {
				$userdata = [];
				$userdata['nome_user'] = $this -> input -> post('NOME_USER');

				if ($this -> users_model -> create_update_user_details($userdatadetail, $userdata)) {
               $this -> session -> set_userdata(['nomeusr' => $this -> input -> post('NOME_USER')]);
               if ($this -> anuncios_model -> anuncio_publicado()) {
                  $this -> users_model -> exportar_firebase($this -> users_model -> get_id_user());
                  $this -> session -> set_flashdata('msginfo', 'As informações foram atualizadas com sucesso!');
                  redirect('anuncios/meu_anuncio');
               }
					redirect(base_url('alterarplano'));
				} else {
					$this -> session -> set_flashdata('msgerror', 'Erro ao gravar registro!');
				}
			}
		}
		$userdatadetail['nome_user'] = $this -> input -> post('NOME_USER');
		$userdatadetail['email_user'] = $this -> input -> post('EMAIL_USER');

		$model = $this -> users_model -> get_user_data();
		$data_row = $this -> users_model -> new($model, $userdatadetail);

		$dados = [];
		$dados['file_view'] = 'dashboard/dash_dados_view';
		$dados['prefix_menu'] = '';
		$dados['data_row'] = $data_row;
		$dados['header_action'] = $this -> input -> post('HEADER_ACTION');
		$dados['titulo_dash_view'] = '1. Preencha seus dados';
		$this -> load -> view('dashboard/default_dash_view', $dados);

	}

	// public function gravarplano() {
	// 	if ($this -> libfloop -> user_logged_in() && $this -> input -> post('selectplanpag') && $this -> input -> post('selectplanpagper')) {
	// 		$this -> form_validation -> set_rules('selectplanpag', 'Plano', 'trim|required|is_numeric|xss_clean');
	// 		$this -> form_validation -> set_rules('selectplanpagper', 'Período', 'trim|required|is_numeric|xss_clean');
	// 		if ($this -> form_validation -> run() == TRUE) {
	// 			$userdata = [];
	// 			$userdata['id_plano'] = $this -> input -> post('selectplanpag');
	// 			$userdata['id_periodo'] = $this -> input -> post('selectplanpagper');
	// 			if ($this -> users_model -> update_user_plan($userdata)) {
	// 				redirect(base_url('pagamento'));
	// 			} else {
	// 				$this -> session -> set_flashdata('msgerror', 'Informe os dados pessoais!');
	// 			}
	// 		}
	// 	}
	// 	$this -> show_cadastro();
	// }

	function do_upload_file() {
		if (!empty($_FILES)) {
			$uploadData = array();
			$tempFile = $_FILES['PATHLOGOEMP_USER']['tmp_name'];
			$fileName = $_FILES['PATHLOGOEMP_USER']['name'];

			$targetPath = $this -> libfloop -> get_path_img_user();

			if (!file_exists($targetPath)) {
				mkdir($targetPath, 0777, true);
			}
			unset($_FILES['PATHLOGOEMP_USER']);
			$fileNameNew = $fileName;
			$filecont = 1;
			$filenameprefix = 'LOGO';

			$fileNameNew = $fileName;
			$path_parts = pathinfo($fileNameNew);
			$fileNameNew = $filenameprefix . '_' . $filecont . '.' . $path_parts['extension'];
			/*
			 while (file_exists($targetPath . $fileNameNew)) {
			 $fileNameNew = $path_parts['filename'] . '_' . $filecont . '.' . $path_parts['extension'];
			 $filecont += 1;
			 }
			 *
			 */
			$targetFile = $targetPath . $fileNameNew;
			move_uploaded_file($tempFile, $targetFile);
			$uploadData[0]['nome_arquivo'] = $fileNameNew;
			//$uploadData[0]['tamanho'] = $tamanho;
			if (!empty($uploadData)) {
				//$insert = $this->Capa_model->insert($uploadData);
				//return $urlPath . $fileNameNew;
				return $fileNameNew;
			} else {
				$this -> session -> set_flashdata('msgerror', $this -> upload -> display_errors());
				return FALSE;
			}
		}
	}

}
