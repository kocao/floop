<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendformcontato extends CI_Controller {

	public function index() {
		$this -> form_validation -> set_rules('name', 'Nome', 'trim|required|max_length[50]|ucwords');
		$this -> form_validation -> set_rules('email', 'E-mail', 'trim|required|max_length[200]|strtolower|valid_email');
		$this -> form_validation -> set_rules('subject', 'Assunto', 'trim|required|max_length[100]');
		$this -> form_validation -> set_rules('message', 'Mensagem', 'trim|required|max_length[5000]');
		if ($this -> form_validation -> run() == TRUE) {
			$this -> load -> library('email');
			$this -> email -> initialize();
			$this -> email -> subject($this -> input -> post('subject'));
			$this -> email -> from('suporte@floop.com.br', $this -> input -> post('name'), $this -> input -> post('email'));
			$this -> email -> reply_to($this -> input -> post('email'), $this -> input -> post('name'));
			$this -> email -> to('suporte@floop.com.br');
			$this -> email -> message($this -> input -> post('message'));
			$this -> email -> send();
			echo 'OK';
		} else {
			echo 'Erro ao enviar a mensagem, verifique os campos informados!';
		}
	}

}
