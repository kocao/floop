<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
	<div class="col-md-12 center">
		<h2 class="heading-dark mt-xl mb-none"><strong>Planos</strong> de anúncios</h2>
		<p class="mb-xl">
			Escolha o plano que melhor se adapta com o seu negócio.
		</p>
	</div>
</div>
<div class="row">
	<div class="pricing-table princig-table-flat">
		<div class="col-lg-4 col-sm-6">
			<!-- <div class="plan most-popular"> -->
			<div class="plan m-popular">
				<div class="plan-ribbon-wrapper">
					<div class="plan-ribbon">
						+Popular
					</div>
				</div>
				<h3>Ouro</h3>
				<h3>
				<div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-6">
						<input type="hidden" id="vlrplangold1" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(1,4),2,',','.');?>" />
						<input type="hidden" id="vlrplangold2" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(1,5),2,',','.');?>" />
						<input type="hidden" id="vlrplangold3" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(1,6),2,',','.');?>" />
						<select data-plugin-selecttwo="" class="form-control populate" id="selectplangold">
							<optgroup label="Selecione">
								<option value="1" selected>Mensal</option>
								<option value="2">Trimestral</option>
								<option value="3">Semestral</option>
							</optgroup>

						</select>
					</div>
				</div><span id="vlrplangold"> <?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(1,4),2,',','.');?> </span> por mês </h3>
				<a class="btn btn-lg btn-primary" href="<?php echo base_url('solicitaplano');?>">Assinar</a>
				<ul>
					<li>
                  Descrição do estabelecimento (até 1000 caracteres)
					</li>
					 <li>
							Fotos do local com legenda (até 9 fotos)
					 </li>
					 <li>
							Link do site da empresa
					 </li>
					 <li>
							Área para inserção de cardápio (bares/restaurantes)
					 </li>
					<li>
						Logo da empresa no mapa do aplicativo
					</li>
					<li>
						Gerenciamento online - atualize seu anúncio quando e quantas vezes achar necessário
					</li>
					<li>
						Apareça sempre nas buscas por categoria do seu estabelecimento
					</li>
					<li>
						Espaço para divulgar promoções
					</li>
					<li>
						Link para reservas de mesa (redirecionamento para o site do parceiro)
					</li>
					<li>
						Área para divulgar agenda de eventos próximos (até 7 eventos)
					</li>
					<li>
						Links para redes sociais (Facebook, Youtube, Instagram, Twitter e Google+)
					</li>
					<li>
						Link para venda de ingressos (redirecionamento para o site do parceiro)
					</li>
					<li>
						Colocação no ranking de estabelecimentos mais populares mesmo que não haja avisos de lotação
					</li>
					<li>
						1 publicação semanal nas redes sociais do Floop
					</li>
				</ul>
			</div>
		</div>
		<div class="col-lg-4 col-sm-6">
			<div class="plan">
				<h3>Prata</h3>
				<h3>
				<div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-6">
						<input type="hidden" id="vlrplansilver1" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(2,4),2,',','.');?>" />
						<input type="hidden" id="vlrplansilver2" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(2,5),2,',','.');?>" />
						<input type="hidden" id="vlrplansilver3" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(2,6),2,',','.');?>" />
						<select data-plugin-selecttwo="" class="form-control populate" id="selectplansilver">
							<optgroup label="Selecione">
								<option value="1" selected="">Mensal</option>
								<option value="2">Trimestral</option>
								<option value="3">Semestral</option>
							</optgroup>

						</select>
					</div>
				</div><span id="vlrplansilver"> <?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(2,4),2,',','.');?> </span> por mês </h3>
				<a class="btn btn-lg btn-primary" href="<?php echo base_url('solicitaplano');?>">Assinar</a>
				<ul>
					<li>
						Descrição do estabelecimento (até 600 caracteres)
					</li>
               <li>
                  Fotos do local com legenda (até 6 fotos)
               </li>
               <li>
                  Link do site da empresa
               </li>
               <li>
                  Área para inserção de cardápio (bares/restaurantes)
               </li>
					<li>
                  Logo da empresa no mapa do aplicativo
					</li>
					<li>
                  Gerenciamento online - atualize seu anúncio quando e quantas vezes achar necessário
					</li>
					<li>
                  Apareça sempre nas buscas por categoria do seu estabelecimento
					</li>
					<li>
                  Espaço para divulgar promoções
					</li>
					<li>
                  Link para reservas de mesa (redirecionamento para o site do parceiro)
					</li>
					<li>
                  Área para divulgar agenda de eventos próximos (até 3 eventos)
					</li>
				</ul>
			</div>
		</div>
		<div class="col-lg-4 col-sm-6">
			<div class="plan">
				<h3>Bronze</h3>
				<h3>
				<div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-6">
						<input type="hidden" id="vlrplanbronze1" value="<?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(3,4),2,',','.');?>" />
						<select data-plugin-selecttwo="" class="form-control populate" id="selectplanbronze">
							<optgroup label="Selecione">
								<option value="1" selected>Mensal</option>
							</optgroup>
						</select>
					</div>
				</div><span id="vlrplanbronze"> <?php echo 'R$'.number_format($this->users_model->get_vlrplan_by_idper(3,4),2,',','.');?> </span> por mês </h3>
				<a class="btn btn-lg btn-primary" href="<?php echo base_url('solicitaplano');?>">Assinar</a>
				<ul>
					<li>
						Descrição do estabelecimento (até 200 caracteres)
					</li>
					<li>
						Fotos do local com legenda (até 3 fotos)
					</li>
					<li>
						Link do site da empresa
					</li>
					<li>
						Área para inserção de cardápio (bares/restaurantes)
					</li>
					<li>
                  Logo da empresa no mapa do aplicativo
					</li>
					<li>
                  Anúncio único sem possibilidade de edição
					</li>
				</ul>

			</div>
		</div>
	</div>
</div>
