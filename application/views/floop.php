<br><br><br><br><br><br>
            <div class="col-md-12 text-center">
              <h3>O Floop</h3>
            </div>
            <div class="col-md-8 col-md-offset-2 modal-body">
               <p>
                           O Floop é um serviço que possibilita consultar a lotação de um
                           estabelecimento, antes de chegar ao mesmo, por meio de um aplicativo disponível na
                           Play Store e App Store. O usuário é capaz de interagir com o aplicativo por meio de
                           notificações a respeito da quantidade de pessoas e nível de lotação de determinado
                           local. As notificações são enviadas ao processamento por meio de um algoritmo
                           específico e, se validadas, serão exibidas no mapa do aplicativo por meio de
                           diferentes cores indicando o nível de lotação em tempo real. O registro do usuário se
                           dará mediante permissão ao acesso a sua conta do Facebook ou Google+ ou via
                           cadastro através do próprio aplicativo. O uso do Floop significa aceitar, expressamente
                           e sem exceções, os termos de uso descritos. Os mesmos poderão ser
                           alterados a qualquer tempo, sem notificação imediata aos usuários. Visite
                           regularmente esta página e consulte sempre os termos vigentes.
               </p>
						</div>
