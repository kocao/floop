<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
		<footer id="footer" class="background-color-quaternary m-none pb-xlg">
			<div class="container pt-xlg pb-xlg">
				<div class="row mb-xlg pb-xlg">
					<div class="col-md-5">
						<div class="mb-xlg pb-xlg">
							<h4 class="text-color-light font-weight-light mb-lg pb-xs">Entre em Contato</h4>
							<div class="mb-md">

							</div>
							<div class="mb-md">

							</div>
							<div>
								<strong class="font-weight-light text-color-light">E-mail:</strong>
								<a href="mailto:suporte@floop.com.br" class="text-decoration-none font-weight-light">suporte@floop.com.br</a>
							</div>
						</div>

					</div>
					<div class="col-md-3">
						<div class="contact-details">
							<h4 class="text-color-light font-weight-light mb-sm">Floop</h4>
							<p class="custom-font-size-3 text-color-light font-weight-normal text-uppercase mb-none">O Seu Tempo Mais Valioso</p>
						</div>
					</div>
					<div class="col-md-3 col-md-offset-1">
						<h4 class="text-color-light font-weight-light mb-lg pb-xs">Siga-nos nas redes sociais</h4>
						<ul class="social-icons" >
							<li class="" style="color:white;">
								<a href="<?php echo $this->config->item('site_facebook'); ?>" target="_blank" title="Facebook">
                           <i class="fa fa-facebook" ></i>
								</a>
							</li>
							<li class="">
								<a href="<?php echo $this->config->item('site_twitter'); ?>" target="_blank" title="Twitter">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li class="">
								<a href="<?php echo $this->config->item('site_google'); ?>" target="_blank" title="Google+">
									<i class="fa fa-google-plus" aria-hidden="true"></i>
								</a>
							</li>
							<li class="">
								<a href="<?php echo $this->config->item('site_instagram'); ?>" target="_blank" title="Instagram">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="footer-border center pt-xlg">
							<p class="font-weight-normal mb-none small">© Copyright 2017. Floop - Todos os direitos reservados.</p>
						</div>
					</div>
				</div>
			</div>
      		</footer>
