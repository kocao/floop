<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<head>

		<!-- Basico -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>Floop</title>

		<meta name="keywords" content="floop encontrar locais" />
		<meta name="description" content="Aplicativo para encontrar locais">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" > -->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

		<!-- Favicon -->

        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-16.png" sizes="16x16">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-32.png" sizes="32x32">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-48.png" sizes="48x48">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-64.png" sizes="64x64">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-128.png" sizes="128x128">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo assets_url(); ?>img/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo assets_url(); ?>img/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo assets_url(); ?>img/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo assets_url(); ?>img/manifest.json">
        <link rel="mask-icon" href="<?php echo assets_url(); ?>img/safari-pinned-tab.svg" color="#dc4539">
        <meta name="theme-color" content="#ffffff">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

      <!--Facebook ShareLinkContent-->
      <meta property="og:url"                content="http://www.floop.com.br" />
      <meta property="og:type"               content="website" />
      <meta property="og:title"              content="Floop App" />
      <meta property="og:description"        content="Saiba a lotação de vários tipos de estabelecimentos antes de chegar lá." />
      <meta property="og:image"              content="https://firebasestorage.googleapis.com/v0/b/floopapp-672ae.appspot.com/o/share_content%2Fscreenshot.jpg?alt=media&token=8621cc79-c4ce-4e6d-adc9-1a29a440a98e" />


		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light|ABeeZee|Autour+One|Roboto" rel="stylesheet" type="text/css">


		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/animate.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/theme.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/theme-shop.css">

		<!-- CSS Default -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/settings.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/layers.css">
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/skin-floop.css">

		<!-- Floop CSS -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/floop.css">

		<!-- CSS Customizado -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/custom.css">

		<!-- CSS lightbox -->
		<link rel="stylesheet" href="<?php echo assets_url(); ?>css/lightbox.css">

		<!-- Libs de cabeçalho -->
		<script src="<?php echo assets_url(); ?>js/modernizr.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/analytics.js"></script>
		<script src="<?php echo assets_url(); ?>js/modal.js"></script>

	</head>
