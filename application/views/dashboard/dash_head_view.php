<!DOCTYPE html>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Dashboard | Aplicativo Floop</title>

		<!-- Favicon -->

        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-16.png" sizes="16x16">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-32.png" sizes="32x32">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-48.png" sizes="48x48">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-64.png" sizes="64x64">
        <link rel="icon" href="<?php echo assets_url(); ?>img/favicon-128.png" sizes="128x128">

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo assets_url(); ?>img/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo assets_url(); ?>img/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo assets_url(); ?>img/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo assets_url(); ?>img/manifest.json">
        <link rel="mask-icon" href="<?php echo assets_url(); ?>img/safari-pinned-tab.svg" color="#dc4539">
        <meta name="theme-color" content="#ffffff">


		<meta name="keywords" content="Floop" />
		<meta name="description" content="Painel de Controle - Floop App">
		<meta name="author" content="Hostpar Informática Ltda">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css"> -->

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<?php switch ($file_view) {
			case 'dashboard/dash_finan_view' || 'dashboard/dash_cad_planos_view' || 'dashboard/dash_users_view':
		?>
				<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/select2/select2.css" />
				<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<?php
				break;
			default:
		?>
				<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
				<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
				<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/morris/morris.css" />
		<?php
				break;
		}
		?>
		<!-- Custom input file -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>css/dash.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo dash_assets_url(); ?>vendor/modernizr/modernizr.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.min.js"></script>
      <script src="<?php echo assets_url() ?>js/jquery.Jcrop.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.maskedinput.js"></script>
		<script src="<?php echo assets_url(); ?>js/analytics.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <link rel="stylesheet" href="<?php echo assets_url() ?>css/jquery.Jcrop.min.css" type="text/css" />
      <style type="text/css">
        .jcrop-holder div
        {
          -webkit-border-radius: 50% !important;
          -moz-border-radius: 50% !important;
          border-radius: 50% !important;
        }
        .jcrop-holder
         {
             margin: 0 auto;
         }
        .jcrop-append
         {
             margin: 0 auto;
         }
      </style>
		<script>
			jQuery(function($) {
				$('#target').Jcrop({
						aspectRatio: 1,
						minSize: 50,
						onSelect: applyCoords,
						onChange: applyCoords
				});
  			$('#target_div').addClass('hidden');
			});

		</script>



	</head>
