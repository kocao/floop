<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<?php echo base_url('dashboard'); ?>" class="logo">
						<img src="<?php echo dash_assets_url(); ?>images/logo.png" height="35" alt="Floop" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: user box -->
				<div class="header-right">
					<span class="separator"></span>
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<i class="fa fa-user" style="background-color: white;"></i>
							</figure>
							<div class="profile-info" data-lock-name="<?php echo $this -> session -> nomeusr; ?>" data-lock-email="<?php echo $this -> session -> emailusr; ?>">
								<span class="name"><?php echo $this -> session -> nomeusr; ?></span>
								<?php
								if ($this -> libfloop -> user_is_admin()) {
									echo '<span class="name">Administrador</span>';
								}
								?>
							</div>
							<i class="fa custom-caret"></i>
						</a>
						<div class="dropdown-menu" style="background-color: #1d2127;">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo base_url('login/logoff'); ?>"><i class="fa fa-power-off"></i> Logoff</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: user box -->
			</header>
			<!-- end: header -->
