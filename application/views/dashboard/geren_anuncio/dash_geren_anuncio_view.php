<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="panel">
	<?php if ($header_action){	?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Cadastro de Planos</h2>
			</header>
			<?php } ?>
	<div class="panel-body">
		<!--
		<div class="row">
			<div class="col-sm-6">

				<div class="mb-md">
					<button id="addToTable" class="btn btn-primary">
						Incluir <i class="fa fa-plus"></i>
					</button>
				</div>

			</div>
		</div>
		-->
		<div class="col-md-12 table-responsive">
			
		<table class="table table-bordered table-striped mb-none" id="datatable-editable">
			<thead>
				<tr>
					<th>Usuário</th>
					<th>Plano</th>
               <th>Publicação</th>
					<th>Vencimento</th>
					<th>Status</th>
					<th>Opções</th>
				</tr>
			</thead>
			<tbody>

			<?php if ($rows){ ?>
				<?php foreach ($rows as $key => $row){ ?>
					<tr class="gradeA">
						<td class="nomeusr"><?php echo $row['nome_user']; ?></td>
						<td class="nomeusr"><?php echo $row['descr_plano']; ?></td>
						<td class="nomeusr"><?php echo date_view($row['dt_pgto']); ?></td>
						<td class="nomeusr"><?php echo date_view($row['dt_vcto']); ?></td>
						<td class="nomeusr"><?php echo $row['descr_statusanuncio']; ?></td>

						<td class="actions">
							<a href="#" class="hidden on-editing save-row">
								<i class="fa fa-save"></i>
							</a>
							<a href="#" class="hidden on-editing cancel-row">
								<i class="fa fa-times"></i>
							</a>
							<a href="<?php echo base_url('gerenciaranuncios/editar?id_user=' . $row['id_user']); ?>" class="on-default edit-row">
								<i class="fa fa-pencil" title="Editar"></i>
							</a>
                     <?php if($this -> anuncios_model -> anuncio_publicado($row['id_anuncio'])) { ?>
   							<a href="<?php echo base_url('gerenciaranuncios/desativar_anuncio?id_anuncio=' . $row['id_anuncio']); ?>" onclick="return confirm('Tem certeza que deseja desativar o anúncio?')" class="on-default remove-row">
   								<i class="fa fa-trash-o" title="Desativar"></i>
   							</a>
                     <?php } ?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>



			</tbody>

		</table>
		</div>
	</div>
   <br><br>
   <div class="col-md-12 text-center">
      <a class="btn btn-primary" href="<?php echo base_url('financeiro/exportar_firebase')?>">Sincronizar PagSeguro/Firebase</a>
   </div>
</section>

<!-- end: page -->
