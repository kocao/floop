<?php
$planos_rows = $this -> users_model -> get_rows_cad_planos();
$periodos_rows = $this -> users_model -> get_rows_periodos();
$anuncio = $this -> anuncios_model -> get_anuncio($userdetail['id_user']);
?>
<section class="panel">
   <header class="panel-heading">
      <div class="panel-actions">
         <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
         <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
      </div>
      <h2 class="panel-title">Alterar Anúncio</h2>
   </header>
   <div class="panel-body" style="display: block;">
      <?php echo form_open('gerenciaranuncios/atualizar_plano'); ?>
      <input type="hidden" name="id_user" value="<?php echo $userdetail['id_user'] ?> ">
      <div class="form-group">
         <label class="col-md-3 control-label" for="inputDefault">Empresa</label>
         <div class="col-md-6">
            <span class="form-control"><?php echo $userdetail['nomeempresa_user'] ?></span>
         </div>


      </div>
      <div class="form-group">
         <label class="col-md-3 control-label" for="inputDefault">Plano</label>
         <div class="col-md-6">
            <select name="id_plano">
               <?php if ($planos_rows){ ?>
                  <?php foreach ($planos_rows as $key => $row){ ?>
                     <?php
                     $selected = '';
                     if ($row['id_plano'] == $userdetail['id_plano']){
                        $selected = " selected='TRUE'";
                     }
                     ?>
                     <option value="<?php echo $row['id_plano']; ?>" <?php echo $selected;?>><?php echo $row['descr_plano']; ?></option>
                     <?php } ?>
                     <?php } ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <label class="col-md-3 control-label" for="inputDefault">Data de Vencimento</label>
         <div class="col-md-6">
            <input type="text" name="dt_vcto" class="data" value="<?php echo date_view($anuncio['dt_vcto']) ?>">
         </div>
      </div>

      <div class="form_group text-center">
         <button type="submit" class="btn btn-primary" name="button">Alterar Plano</button>
      </div>
      <!-- <div class="form-group">
         <label class="col-md-3 control-label" for="inputDefault">Ativo</label>
         <div class="col-md-6">
            <select name="ativo">
                     <option value="A" <?php #echo $row['ativo'];?>><?php echo $row['descr_plano']; ?></option>
                     <option value="A" <?php #echo $selected;?>><?php echo $row['descr_plano']; ?></option>
            </select>
         </div>
      </div> -->


      <?php echo form_close() ?>
   </div>
</section>
<script>
// $('.data').mask('00/00/0000');
$('.data').keyup(function(){
   v=$(this).val();
   v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
   v=v.replace(/^(\d{2})(\d{2})(\d{4})/g,"$1/$2/$3"); //Coloca parênteses em volta dos dois primeiros dígitos
  //  v=v.replace(/^(\d{2})(\d{1,2})/g,"$1/$2"); //Coloca parênteses em volta dos dois primeiros dígitos
  //  v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
   $(this).val(v);
});
// </script>
