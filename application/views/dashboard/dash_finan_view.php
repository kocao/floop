<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="panel">
	<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title"><?php echo $titulo_dash_view; ?></h2>
			</header>
			<?php } ?>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-6">
				<div class="mb-md">
					<button id="addToTable" class="btn btn-primary">
						Incluir <i class="fa fa-plus"></i>
					</button>
				</div>
				<input type="hidden" id="urlgravafinanceiro" value="<?php echo base_url('financeiro/gravar'); ?>"/>
			</div>
		</div>
		<table class="table table-bordered table-striped mb-none" id="datatable-editable">
			<thead>
				<tr>
					<th>Parceiro</th>
					<th>Descrição &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th>Vencimento</th>
					<th>Valor</th>
					<th>Status</th>
					<th>Opções</th>
				</tr>
			</thead>
			<tbody>
				
			<?php if ($rows){ ?>
				<?php foreach ($rows as $key => $row){ ?>
					<tr class="gradeA">
						<td class="nomeusr"><?php echo $row['nome_user']; ?></td>
						<td>Teste</td>
						<td class="datavecto"><?php echo date_format(date_create($row['dtvecto_finan']), 'd-m-Y'); ?></td>
						<td><?php echo $row['valor_finan']; ?></td>
						<td><?php
						if ($row['status_finan'] == 'P') {echo 'Pendente';
						} else {echo 'Quitado';
						};
 ?></td>
						<td class="actions">
							<a href="#" class="hidden on-editing save-row"> 
								<i class="fa fa-save"></i> 
							</a>
							<a href="#" class="hidden on-editing cancel-row">
								<i class="fa fa-times"></i>
							</a>
							<a href="<?php echo base_url('financeiro/editar/' . $row['id_finan']); ?>" class="on-default edit-row">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="#" class="on-default remove-row">
								<i class="fa fa-trash-o"></i>
							</a>
						</td>
					</tr>
											
				 
				<?php } ?> 
			<?php } ?>				
				
				

			</tbody>

		</table>
	</div>
</section>
<!-- end: page -->
