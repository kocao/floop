<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<div class="row"><div class="col-lg-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>', '</p></strong></div></div></div>');
function cmp($a, $b)
{
    return strcmp($a->sequence, $b->sequence);
}
$cardapios = $this->users_model->get_cardapios();
$locais = $this->users_model->get_locais();
$promocoes = $this->users_model->get_promocoes();
$id_user = $this->users_model->get_id_user();
$logo = $this->users_model->get_logo();
$dias_semana = ['Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado','Domingo'];
$horarios = $this -> anuncios_model -> get_horarios();
$eventos = $this -> anuncios_model -> get_eventos();

switch ($plan_contratado_row -> abrev_plano) {
   case 'B':
      $descricao = "Descrição resumida do local (até 200 caracteres)";
      $img_demo = "cota_bronze.png";
      $id_modal = "cota_bronze";
      break;
   case 'P':
      $descricao = "Descrição detalhada do local (até 600 caracteres)";
      $img_demo = "cota_prata.png";
      $id_modal = "cota_prata";
      break;
   case 'O':
      $descricao = "Descrição detalhada do local (até 1000 caracteres)";
      $img_demo = "cota_ouro.png";
      $id_modal = "cota_ouro";
      break;
   default:
      break;
}
/*
“
Descrição resumida do local (até 200 caracteres)”para a cota bronze,
Descrição detalhada do local (até 600 caracteres)” para a cota prata,
Descrição detalhada do local (até 1000 caracteres)” ouro
*/
   ?>
   <div class="row">
      <div class="col-md-12">
      <section class="panel">
            <!-- <header class="panel-heading">
               <div class="panel-actions">
                  <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
               </div>
               <h2 class="panel-title">Informações Sobre o Anúncio</h2>
            </header> -->
            <div class="panel-body" style="display: block;">
               <div class="col-md-12" style="color: #f44242;">
                  <strong>
                  Bem-vindo, anunciante! Obrigado por anunciar no Floop!
                  Abaixo você pode verificar onde e como os campos preenchidos aqui irão aparecer no aplicativo. É simples, rápido e prático! Caso algum campo não se aplique a seu negócio, pode deixá-lo sem preenchimento.
                  Dica: Alguns campos são essenciais para nós e eles estão marcados com um asterisco.
                  </strong>
               </div>

               <div class="col-md-10 col-md-offset-1">
               <a class="lightbox" href="#<?php echo $id_modal ?>" onclick="$('#<?php echo $id_modal ?>').removeClass('hidden')">
<!--                  <img src="<?php #echo assets_url(); ?>img/passo01.png" class="img-passo"/> -->
                  <img class="img-responsive img-passo" src="<?php echo assets_url()."img/".$img_demo?>" alt="" />
               </a>

               </div>

               <div class="col-md-12" style="color: #f44242;">
               <strong>
               Preencha os campos abaixo, customize o seu anúncio e floope com a gente! Não leva mais que 5 minutos!
               </strong>
               </div>
            </div>
      </section>
      </div>
   </div>
   <div class="row">
         <?php echo form_open_multipart('anuncios/gravar',["id" => "info", "onsubmit" => "addHttpSubmit();return validateForm()"]); ?>
         <?php echo form_hidden('HEADER_ACTION', $header_action); ?>
         <?php echo form_hidden('ID_ANUNCIO', $idanuncio); ?>
         <?php echo form_hidden('ID_TIPOIMG', 1); ?>

      <div class="col-lg-12">
         <section class="panel">
               <header class="panel-heading">
                  <div class="panel-actions">
                     <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                  </div>
                  <h2 class="panel-title">Informações Sobre o Anúncio</h2>
               </header>

               <div class="panel-body" style="display: block;">
                  <div class="col-md-12">
                     Insira aqui uma descrição para o seu estabelecimento. Conte sua história, especialidades e detalhes à sua escolha. Você ainda pode colocar o site de sua empresa ou divulgá-la por meio do endereço de suas redes sociais.
                  </div>
                  <p>
                     &nbsp;
                  </p>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="DESCR_ANUNCIO">Descrição <span class="required"> *</span></label>
                     <div class="col-md-6">
                           <textarea name="DESCR_ANUNCIO" maxlength="<?php echo $dataplano -> maxdescr_plano;?>" class="form-control" rows="9" placeholder="<?php echo $descricao ?>" required <?php echo required_message() ?>><?php if ($anunciorow){echo $anunciorow[0]['descr_anuncio'];} ?></textarea>
                     </div>
                  </div>
                  <?php if ($dataplano -> showsite_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="siteemp_user">Site</label>
                     <div class="col-md-6">
                        <input type="text" name="siteemp_user" class="form-control link" value="<?php echo $anunciorow[0]['siteemp_user'] ?>" placeholder="Link do site da empresa" >
                         <!-- oninvalid="this.setCustomValidity('Lütfen işaretli yerleri doldurunuz')" -->
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showreserva_plano == true) {?>
                     <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Link para reservas</label>
                        <div class="col-md-6">
                           <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkreservamesa_user'];} ?>" name="lnkreservamesa_user" class="form-control link" placeholder="Link para reservas de mesa">
                        </div>
                     </div>
                  <?php } ?>
                  <?php if ($dataplano -> showcomprar_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Link para venda de produtos</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkcomprar_user'];} ?>" name="lnkcomprar_user" class="form-control link" placeholder="Link para venda de produtos" >
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showfacebook_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Facebook</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkfacebook_user'];} ?>" name="lnkfacebook_user" class="form-control link" placeholder="Perfil do Facebook">
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showyoutube_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Youtube</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkyoutube_user'];} ?>" name="lnkyoutube_user" class="form-control link" placeholder="Perfil do Youtube" >
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showtwitter_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Twitter</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnktwitter_user'];} ?>" name="lnktwitter_user" class="form-control link" placeholder="Perfil do Twitter" >
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showinstagram_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Instagram</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkinstagram_user'];} ?>" name="lnkinstagram_user" class="form-control link" placeholder="Perfil do Instagram">
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showgmais_plano == true) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Google+</label>
                     <div class="col-md-6">
                        <input type="text" value="<?php if ($anunciorow){echo $anunciorow[0]['lnkgmais_user'];} ?>" name="lnkgmais_user" class="form-control link" placeholder="Perfil do Google +">
                     </div>
                  </div>
                  <?php } ?>

               </div>
               </section>
            </div>
               <div class="row">
                  <div class="col-md-12 proximo">
                     <?php if($this -> anuncios_model -> anuncio_publicado()){ ?>
                        <div class="row">
                           <div class="col-md-6 pull-left">
                              <a href="<?php echo base_url('dadospessoais') ?>" class="btn btn-primary">Anterior</a>
                           </div>
                           <div class="col-md-6 pull-right">
                              <button type="submit" class="btn btn-primary pull-right" name="">Atualizar no App</button>
                              <?php #echo form_submit('btngravar', 'Pŕoximo', 'class="btn btn-primary pull-right"'); ?>
                           </div>
                        </div>
                        <?php } else { ?>
                           <div class="row">
                              <div class="col-md-6 pull-left">
                                 <a href="<?php echo base_url('alterarplano') ?>" class="btn btn-primary">Anterior</a>
                              </div>
                              <div class="col-md-6">
                                 <button type="submit" class="btn btn-primary pull-right"  name="">Pŕoximo</button>
                                 <?php #echo form_submit('btngravar', 'Pŕoximo', 'class="btn btn-primary pull-right"'); ?>
                              </div>
                           </div>
                           <?php } ?>

                        </div>
                     </div>
            <br><br>
                  <div class="col-md-12">
                     <section class="panel">
                        <div class="panel-heading">
                           <h3 class="panel-title">Horário de funcionamento</h3>
                           <div class="panel-actions">
                              <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              Insira abaixo os horários de funcionamento do seu estabelecimento.

                           </div>
                           <p>
                              &nbsp;
                           </p>
                           <div class="col-md-12 table-responsive">
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th></th>
                                       <th class="text-center">Horário de Início</th>
                                       <th class="text-center">Horário de Término</th>
                                       <th class="text-center">Fechado</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td class="text-center">
                                          <button type="button" name="button" onclick="copiar();" class="btn btn-default">Copiar</button>
                                       </td>
                                       <td class="text-center">
                                          <?php echo time_select('hinicioall','minicioall','','','hinicio')?>
                                       </td>
                                       <td class="text-center">
                                          <?php echo time_select('hfimall','mfimall')?>
                                       </td>
                                       <td class="text-center">
                                          <!-- <select class="fechadoall" name="fechadoall">
                                          <option value="0" <?php# echo ($horarios[$key]['fechado'] == "f" ? "selected" : "")?>>Não</option>
                                          <option value="1" <?php# echo ($horarios[$key]['fechado'] == "t" ? "selected" : "")?>>Sim</option>
                                       </select> -->
                                    </td>
                                 </tr>
                                 <?php foreach ($dias_semana as $key => $dia) { ?>

                                    <tr>
                                       <td class="pull-left">
                                          <?php echo $dia ?>
                                          <input type="hidden" name="dia[]" value="<?php echo $dia ?>">
                                       </td>
                                       <td class="text-center">
                                          <?php echo time_select('hinicio[]','minicio[]', $horarios[$key]['horarioinicio'])?>
                                       </td>
                                       <td class="text-center">
                                          <?php echo time_select('hfim[]','mfim[]', $horarios[$key]['horariofim'])?>
                                       </td>
                                       <td class="text-center">
                                          <select class="fechado" name="fechado[]">
                                             <option value="0" <?php echo ($horarios[$key]['fechado'] == "f" ? "selected" : "")?>>Não</option>
                                             <option value="1" <?php echo ($horarios[$key]['fechado'] == "t" ? "selected" : "")?>>Sim</option>
                                          </select>
                                       </td>
                                    </tr>
                                    <?php }?>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </section>
                  </div>
               <?php if ($dataplano -> maxfotoseventos_plano >= 1) {?>
                  <section>
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Agenda de eventos</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label>Insira abaixo os próximos eventos que vão bombar em seu estabelecimento. Dica: Insira em ordem correta de tempo (do evento mais antigo para o mais recente).</label>
                                 </div>
                              </div>
                              <p>
                                 &nbsp;
                              </p>
                              <div class="" id="eventos">
                                 <?php foreach ($eventos as $key => $evento) {
                                    $this -> load -> view('dashboard/_evento', ['evento' => $evento]);
                                 }?>
                              </div>
                              <div class="col-md-12 text-center">
                                 <button class="glyphicon glyphicon-plus btn btn-default" type="button" name="button" onclick="add_evento();"></button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <?php } ?>
                  <?php echo form_close(); ?>
               </div>
               <section class="panel">
                  <?php if ($header_action){
                     ?>
                     <header class="panel-heading">
                        <div class="panel-actions">
                           <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                        </div>
                        <h2 class="panel-title">Logo<span class="required"> *</span></h2>
                     </header>
                     <?php } ?>
                  <div class="panel-body">
                     <div class="col-md-12">
                        Insira aqui o logo do seu estabelecimento. Dica: coloque uma imagem sem fundo (png) para melhor visualização no aplicativo.
                     </div>
                     <p>
                        &nbsp;
                     </p>
                     <?php $this -> load -> view('dashboard/_galeria', ['fotos' => $logo, 'tipo' => 'logo']); ?>
                  </div>
               </section>
               <?php if ($dataplano -> maxfotos_plano >= 1) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Fotos do estabelecimento<span class="required"> *</span></h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <div class="col-md-12">
                                 Insira aqui fotos do seu estabelecimento. Caso queira adicionar uma legenda, escreva o texto no campo abaixo antes de fazer upload da imagem. Dica: coloque uma imagem com fundo, seguindo a proporção aproximada de 210 pixels de altura x 420 pixels de largura, para melhor visualização no aplicativo.
                              </div>
                              <p>
                                 &nbsp;
                              </p>
                              <?php $this -> load -> view('dashboard/_galeria', ['fotos' => $locais, 'tipo' => 'local']); ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <?php if ($dataplano -> maxfotoscardapio_plano >= 1) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Cardápio</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <div class="col-md-12">
                                 Insira aqui fotos do seu cardápio.
                              </div>
                              <p>
                                 &nbsp;
                              </p>
                              <?php $this -> load -> view('dashboard/_galeria', ['fotos' => $cardapios, 'tipo' => 'cardapio']); ?>

                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <?php } ?>
               <?php if ($dataplano -> maxfotospromo_plano >= 1) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Promoções</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <div class="col-md-12">
                                 Divulgue aqui suas principais promoções aos seus clientes.
                              </div>
                              <?php $this -> load -> view('dashboard/_galeria', ['fotos' => $promocoes, 'tipo' => 'promocao']); ?>
                           </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <?php } ?>
                  <?php } ?>
                  <?php if (!empty($logo[0]) && $logo[0]['recortado'] != 't'){ ?>

                  <div id="target_div2">
                        <div class="text-center">
                           <h3>Por favor recorte o logo para melhor visualização no app.</h3>
                        </div>
                        <img id="target" src="<?php echo $logo[0]['url_file']?>" class="img-logo-zoom" alt="loading" />
                        <br>
                        <div class="text-center">
                        <?php echo form_open("anuncios/redimensiona_logo", ["onsubmit" => "return validateRecorte();"]); ?>
                            <input type="text" id="coords" name="coords" style="display: none;">
                            <button type="submit" class="btn btn-success btn-md">Recortar <i class="glyphicon glyphicon-scissors"></i></button>
                            <button type="button" class="btn btn-danger btn-md" name="button" onclick="$('#logoDelete').click(); $('#target_div2').remove()" >Cancelar <i class="glyphicon glyphicon-remove"></i></button>
                        <?php echo form_close(); ?>
                        </div>
                  </div>

                  <?php } ?>

                  <footer class="">
                  </footer>
               </div>
            </div>
            <script>
            function addHttpSubmit(){
               $( ".link" ).each(function( index ) {
                 add_http($(this));
               });            }
            $('.link').select(function(){
            })
            $('.link').on('paste', function (e) {
               link = e.originalEvent.clipboardData.getData('Text');
               if(!link.startsWith('http://') && !link.startsWith('https://') && link.length > 8 ){
                  link = '';
                  link = 'http://' + link;
                  $(this).val('').val(link); //Clear the current value, then insert the value of the data that was pasted
               }
            });

            $('.link').keyup(function(){
               add_http($(this));
            });
            function add_http(t){
               link = t.val()
               if(!link.startsWith('http://') && !link.startsWith('https://') && link.length > 8 ){
                  t.val('http://' + link)
               }
            }
            function validateRecorte(){
               var coord = $('#coords').val();
               var valido = true;
               if(coord == ''){
                  valido = false
                  alert('Recorte o logo para continuar.');
               }
               return valido;
            }
            function validateForm(){
               var totalLogo = $('#logo-thumbs img').length;
               var totalLocal = $('#local-thumbs img').length;
               var valido = true;
               if(totalLogo < 1) {
                  alert('É obrigatório adicionar um logo.');
                  valido = false;
               }
               if(totalLocal < 1){
                  alert('É necessário adicionar pelo menos uma foto do estabelecimento');
                  valido = false
               }
               if(valido){
                  $('#overlay').show();
               }
               return valido;
            }
            function copiar(){
               $("select").each(function() {
                 var select = $(this).attr('name');  // first pass, create name mapping
                 switch (select) {
                    case 'hinicio[]':
                       var val = document.getElementsByName("hinicioall")[0].value;
                       $(this).val(val);
                       break;
                    case 'minicio[]':
                       var val = document.getElementsByName("minicioall")[0].value;
                       $(this).val(val);
                       break;
                    case 'hfim[]':
                       var val = document.getElementsByName("hfimall")[0].value;
                       $(this).val(val);
                       break;
                    case 'mfim[]':
                       var val = document.getElementsByName("mfimall")[0].value;
                       $(this).val(val);
                       break;
                    default:
                       break;

                 }
               });
            }
            function divisor() {
               $('.slider-thumbs').each(function(){
                  $(this).find('.divisor').remove();
                  $(this).find('.foto').each(function(index){
                        if( index % 4 == 0 && index != 0){
                           $(this).before('<div class="col-sm-12 divisor"><hr/></div>');
                        }
                  });
               })
            }

            $('.fechado').change(function(){
               if($(this).val() == "1"){
                  $(this).parent().prev().children().hide();
                  $(this).parent().prev().prev().children().hide();

               } else {
                  $(this).parent().prev().children().show();
                  $(this).parent().prev().prev().children().show();

               }
            });

            function add_evento(){
               $.get('anuncios/add_evento', function(data){
                  $('#eventos').append(data);
               });
            }

            $(document).ready(function($) {
               divisor();
               $('.fechado').each(function(){
                  if($(this).val() == "1"){
                     $(this).parent().prev().children().hide();
                     $(this).parent().prev().prev().children().hide();
                  } else {
                     $(this).parent().prev().children().show();
                     $(this).parent().prev().prev().children().show();
                  }
               });
            });

            $(document).on('click', '#close-preview', function(){
               $('.image-preview').popover('hide');
            });

            function deletar_imagem(id_image, t){
               $('#overlay').show();
               $.post("anuncios/deletar_imagem", {id_file: id_image}, function(data){
                  t.closest('.foto').remove();
                  divisor();
               });
               $('#overlay').hide();
            }

            $(':file').change(function(){
               // $('#local').submit();
               var form = "#" + $(this).attr('name');
               var tipo = $(this).attr('name');
               var data = new FormData($(form)[0]);
               var thumb = $('#' + $(this).attr('name') + '-thumbs');

               $('#overlay').show();  // show the loading message.

               $.ajax({
                    url:'anuncios/upload_file',
                    type:'post',
                    data: data,
                    contentType: false,
                    processData: false,
                    success:function(data){

                       if(data == 'false') {
                          alert('Você atingiu o limite de imagens para essa categoria.');
                       } else {
                          setTimeout(function(){ var x = 'x' }, 10000);
                          thumb.append(data);
                          divisor();
                      }
                      $('#overlay').hide();
                      if(tipo == 'logo'){
                          $('#info').attr('action', 'anuncios/gravar_logo');
                          $('#info').attr('onsubmit', '');
                          $('#info').submit();// hide the loading message
                      }
                    }
                 });
            });

            $(function() {
               // Create the close button
               var closebtn = $('<button/>', {
                  type:"button",
                  text: 'x',
                  id: 'close-preview',
                  style: 'font-size: initial;',
               });
               closebtn.attr("class","close pull-right");

               // Clear event
               $('.image-preview-clear').click(function(){
               });
               // Create the preview image
               $(".image-preview-input input:file").change(function (){
               });
            });


            </script>
