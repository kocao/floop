<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">
	<div class="sidebar-header">
		<div class="sidebar-title">
			Menu
		</div>
		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
				<ul class="nav nav-main">

					<li>
						<a href="<?php echo base_url(); ?>"> <i class="fa fa-external-link" aria-hidden="true"></i> <span>Home <em class="not-included">(landing page)</em></span> </a>
					</li>
					<li class="nav-parent">
						<a> <i class="fa fa-columns" aria-hidden="true"></i> <span>Minha conta</span> </a>
						<ul class="nav nav-children">
                     <?php if($this -> anuncios_model -> anuncio_publicado() ) { ?>
                        <li>
                           <a href="<?php echo base_url('anuncios/meu_anuncio'); ?>"> Meu anúncio </a>
                        </li>
                     <?php }?>
							<li>
								<a href="<?php echo base_url('dadospessoais'); ?>"> Dados pessoais </a>
							</li>
                     <?php if( !$this -> anuncios_model -> anuncio_publicado() ) { ?>
							<li>
								<a href="<?php echo base_url('alterarplano'); ?>"> Escolher plano </a>
							</li>
                     <?php }?>
                     <?php if( !$this -> anuncios_model -> anuncio_publicado() ) { ?>
							<li>
								<a href="<?php echo base_url('anuncios'); ?>"> Criar Anúncio</a>
							</li>
                     <?php }else if($this -> users_model -> get_row_user_plan() -> abrev_plano != 'B') { ?>
                        <li>
                           <a href="<?php echo base_url('anuncios'); ?>"> Editar Anúncio</a>
                        </li>
                     <?php }?>

                     <?php if( !$this -> anuncios_model -> anuncio_publicado() ) { ?>
							<li>
								<a href="<?php echo base_url('pagamento'); ?>"> Pagamento </a>
							</li>
                     <?php } ?>
							<li>
								<a href="<?php echo base_url('Dadospessoais/recuperar_senha_logado'); ?>"> Alterar Senha</a>
							</li>

                     <?php if( $this -> anuncios_model -> anuncio_publicado() && $this -> users_model -> get_row_user_plan() -> abrev_plano != 'O') { ?>
                        <li>
                           <a href="<?php echo base_url('alterarplano/upgrade_plano'); ?>"> Upgrade de plano </a>
                        </li>
                        <?php } ?>
						</ul>
					</li>
					<?php if ($this -> libfloop -> user_is_admin()){
					?>
					<li>
						<a href="<?php echo base_url('usuarios'); ?>"> <i class="fa fa-users" aria-hidden="true"></i> <span>Cadastro de Usuários</span> </a>
					</li>
					<li>
						<a href="<?php echo base_url('planosdeanuncios'); ?>"> <i class="fa fa-table" aria-hidden="true"></i> <span>Cadastro de Planos</span> </a>
					</li>
					<li>
						<a href="<?php echo base_url('gerenciaranuncios'); ?>"> <i class="fa fa-adjust" aria-hidden="true"></i> <span>Gerenciar anúncios</span> </a>
					</li>
					<?php } ?>
				</ul>
			</nav>

			<hr class="separator" />

		</div>

	</div>

</aside>
<!-- end: sidebar -->
