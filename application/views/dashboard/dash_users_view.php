<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="panel">
	<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title"><?php echo $titulo_dash_view; ?></h2>
			</header>
			<?php } ?>
	<div class="panel-body">
<!--
		<div class="row">
			<div class="col-sm-6">

				<div class="mb-md">
					<button id="addToTable" class="btn btn-primary">
						Incluir <i class="fa fa-plus"></i>
					</button>
				</div>

			</div>
		</div>
		-->
		<table class="table table-bordered table-striped mb-none" id="datatable-editable">
			<thead>
				<tr>
					<th>Usuário / E-mail</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Status</th>
					<th>Opções</th>
				</tr>
			</thead>
			<tbody>

			<?php if ($rows){ ?>
				<?php foreach ($rows as $key => $row){ ?>
					<tr class="gradeA">
						<td><?php echo $row['email_user']; ?></td>
						<td class="nomeusr"><?php echo $row['nome_user']; ?></td>
						<td class="datavecto"><?php echo $row['descr_usertype']; ?></td>
							<?php
								if ($row['confirmado'] == 'f') {
									$row['status_str'] = 'Pendente';
								}
							?>
						<td><?php echo $row['status_str']; ?></td>
						<td class="actions">
							<a href="#" class="hidden on-editing save-row">
								<i class="fa fa-save"></i>
							</a>
							<a href="#" class="hidden on-editing cancel-row">
								<i class="fa fa-times"></i>
							</a>
							<a href="<?php echo base_url('usuarios/editar/' . $row['id_user']); ?>" title="Editar" class="on-default edit-row">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="<?php echo base_url('usuarios/delete_user?id_user=' . $row['id_user']); ?>" onclick="return confirm('Tem certeza que deseja excluir o usuário?')" title="Deletar" class="on-default edit-row">
								<i class="fa fa-trash"></i>
							</a>

						</td>
					</tr>


				<?php } ?>
			<?php } ?>



			</tbody>

		</table>
	</div>
</section>
<!-- end: page -->
