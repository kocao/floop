<?php echo  validation_errors('<div class="row"><div class="col-lg-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>', '</p></strong></div></div></div>');
?>
<header class="panel-heading">
   <div class="panel-actions">
      <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
      <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
   </div>
</header>
<br>
<br>
<br>
<br>
<br>
<br>
<h2 class="text-center">Alterar Senha</h2>
<?php echo form_open_multipart('dadospessoais/alterar_senha'); ?>
<input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>">
<input type="hidden" name="hash_recuperar" value="<?php echo $user['hash_recuperar'] ?>">
<div class="col-md-6 col-md-offset-3">
   <div class="panel-body" style="display: block;">
      <div class="form-group">
         <label class="col-md-3 col-md-offset-3 control-label" for="inputPassword">Senha</label>
         <div class="col-md-3">
            <input type="password" name="PWS_USER1" class="form-control" />
         </div>
      </div>

      <div class="form-group">
         <label class="col-md-3 col-md-offset-3 control-label" for="inputPassword">Confirme a Senha</label>
         <div class="col-md-3">
            <input type="password" name="PWS_USER2" class="form-control">
         </div>
      </div>
      <div class="form-group col-md-12 text-center">
         <button type="submit" class="btn btn-primary" name="button">Alterar Senha</button>

      </div>
   <?php echo form_close() ?>
   </div>
</div>
