<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// $data_row = $this -> users_model -> get_user_data();
$categoria_rows = $this -> users_model -> get_categorias_row();
$json_categoria = json_encode($categoria_rows);
// if ($this -> anuncios_model -> anuncio_publicado() && $plan_contratado_row -> abrev_plano == 'B') {
//    $disabled = "disabled";
// } else {
//    $disabled = "";
// }


echo validation_errors('<div class="row"><div class="col-lg-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>', '</p></strong></div></div></div>');
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Dados Pessoais</h2>
			</header>
			<?php } ?>
			<?php echo form_open_multipart('dadospessoais/gravar',['onsubmit' => "$('#overlay').show();"]); ?>
			<?php echo form_hidden('HEADER_ACTION', $header_action); ?>
			<div class="panel-body" style="display: block;">

  			<div class="form-group">
					<label class="col-md-3 control-label" for="inputReadOnly">Nome de Usuário / E-mail</label>
					<div class="col-md-6">
						<input type="text" value="<?php echo $data_row -> email_user; ?>" name="EMAIL_USER" class="form-control" readonly="readonly"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Nome do anunciante<span class="required"> *</span></label>
					<div class="col-md-6">
						<input type="text" value="<?php echo $data_row -> nome_user; ?>" name="NOME_USER" class="form-control" placeholder="Nome do anunciante" required <?php echo required_message()." $disabled" ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">CNPJ<span class="required"> *</span></label>
					<div class="col-sm-8">
						<div class="row">
							<div class="visible-xs mb-md"></div>
							<div class="col-sm-4">
								<input id="cnpj" type="text" value="<?php echo $data_row -> cnpj_user; ?>" name="CNPJ_USER" class="form-control" placeholder="Informe o CNPJ" required >
							</div>
						</div>

					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Nome da empresa<span class="required"> *</span></label>
					<div class="col-md-6">
						<input type="text" value="<?php echo $data_row -> nomeempresa_user; ?>" name="NOMEEMPRESA_USER" class="form-control" placeholder="Nome da empresa" required <?php echo required_message()." $disabled" ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Categoria<span class="required"> *</span></label>
					<div class="col-md-6">
						<select class="form-control mb-md" id="ID_CATEGORIA" name="ID_CATEGORIA" <?php echo required_message()." $disabled" ?>>
                     <option></option>
							<?php if ($categoria_rows){ ?>
								<?php foreach ($categoria_rows as $key => $row){ ?>
									<?php
									$selected = '';
									if ($row['id_categoria'] == $data_row -> id_categoria){
										$selected = " selected='TRUE'";
									}
									?>
									<option value="<?php echo $row['id_categoria']; ?>" <?php echo $selected;?>><?php echo ucwords($row['descr_categoria']); ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Telefones<span class="required"> *</span></label>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-sm-4">
								<input type="text" value="<?php echo $data_row -> fonecom_user; ?>" name="FONECOM_USER" maxlength="14" class="form-control telefone" placeholder="Telefone comercial" required <?php echo required_message()." $disabled" ?>>
							</div>
							<div class="col-sm-4">
								<input type="text" value="<?php echo $data_row -> fonecel_user; ?>" name="FONECEL_USER" maxlength="15" class="form-control telefone" placeholder="Telefone celular" required <?php echo required_message()." $disabled" ?>>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="form-group">
					<label class="col-md-3 control-label">Buscar por</label>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-md-12">
                        <select class="form-control" name="tipo_busca" id="tipo_busca">
                           <option value="1" <?php # echo ($data_row -> tipo_busca == 1 ? "selected" : "") ?>>Endereço</option>
                           <option value="2" <?php # echo ($data_row -> tipo_busca == 2 ? "selected" : "") ?>>Nome do estabelecimento</option>
                        </select>
							</div>
						</div>
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label class="col-md-3 control-label">Endereço<span class="required"> *</span></label>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-md-6">
								<input id="rua_endereco" type="text" value="<?php# echo $data_row -> enderemp_user; ?>" name="ENDEREMP_USER" class="form-control endereco" placeholder="Nome da rua, av.,etc." required <?php #echo required_message()." $disabled" ?>>
							</div>
							<div class="col-md-3">
								<input id="numero_endereco" type="text" value="<?php #echo $data_row -> nroendemp_user; ?>" name="NROENDEMP_USER" class="form-control endereco" placeholder="Número" required <?php# echo required_message()." $disabled" ?>>
							</div>
							<div class="visible-xs mb-md"></div>
						</div>
					</div>
				</div> -->
				<!-- <div class="form-group">
					<label class="col-md-3 control-label">Cidade<span class="required"> *</span></label>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-sm-6">
                           <input id="cidade_endereco" type="text"  value="<?php# echo $data_row -> cidendemp_user; ?>" name="CIDENDEMP_USER" class="form-control" placeholder="Cidade" required <?php #echo required_message()." $disabled" ?>>
							</div>
							<div class="visible-xs mb-md"></div>
						</div>
					</div>
				</div> -->
				<label class="col-md-12 control-label" style="padding-bottom: 0 !important; padding-top: 4% !important">
               <h4>
               <strong>
                     Digite o nome do estabelecimento no local indicado no mapa para encontrá-lo. Se você não encontrar o seu estabelecimento, digite o endereço.
                  </strong>
               </h4>
            </label>
            <!-- <div class="row info-endereco">
               <div class="col-md-6 col-md-offset-3 text-justify">
                  <p>
                     Ao selecionar o botão “Pesquisar no mapa”, verifique se o marker encontra-se no local correto. Caso contrário, posicione o marker com um clique no local exato do seu estabelecimento.
                  </p>
               </div>
            </div> -->
				<div class="row">
					<!-- <div class="row info-endereco">
						<div class="col-md-2 col-md-offset-5 text-center">
							 <button class="btn btn-danger btn-sm" type="button" onclick="searchAddress();"><b>Pesquisar no mapa</b></button>
						</div>
					</div> -->
                  <input id="pac-input" class="controls" type="text" name="ENDEREMP_USER"
                     placeholder="Digite o nome do estabelecimento ou o endereço" value="<?php echo $data_row -> enderemp_user; ?>">
               <br>
					<div class="col-md-12 text-center">
						 <div class="text-center" id="map-canvas"></div>
					</div>
               <span id="spaninfo">
                  <div id="infowindow-content">
                     <span id="place-name"  class="title"></span><br>
                     <span id="place-address"></span>
                  </div>
               </span>

				</div>
            <br>
				<input type="hidden" value="<?php echo $data_row -> latitude_user; ?>" name="LATITUDE_USER" id="LATITUDE_USER" class="form-control">
				<input type="hidden" value="<?php echo $data_row -> longitude_user; ?>" name="LONGITUDE_USER" id="LONGITUDE_USER" class="form-control">
            <input type="hidden" value="<?php echo $data_row -> place_id; ?>" name="place_id" id="place_id" class="form-control">
				<div class="form-group col-md-12">
					<div class="row">
						<input type="checkbox"  name="termo" required <?php echo ($data_row -> termo ? "checked" : "")?><?php echo " ".$disabled ?>>&nbsp;&nbsp;Li e estou de acordo com os <a href="termosdeuso" target="_blank">Termos de Uso</a>
					</div>
					<div class="row">
						<input type="checkbox"  name="privacidade" required <?php echo ($data_row -> privacidade ? "checked" : "")?><?php echo " ".$disabled ?>/>&nbsp;&nbsp;Li e estou de acordo com a <a href="politicadeprivacidade" target="_blank">Política de Privacidade</a>
					</div>

				</div>
			</div>
			<footer class="panel-footer">
				<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQ__oQHiquHIVfZERpCJ3WegeTHjU-2eY&language=pt&region=BR&libraries=places,geometry"></script>
				<script src="<?php echo assets_url(); ?>js/map.js"></script>
				<link rel="stylesheet" href="<?php echo assets_url(); ?>css/map.css">
            <?php if ($this -> anuncios_model -> anuncio_publicado()) {?>
               <input type="hidden" name="meu_anuncio" value="true" />
               <div class="pull-right">
                  <?php echo form_submit('btngravar', 'Gravar', 'class="btn btn-primary"'); ?>
               </div>
            <?php } else { ?>
				<div class="pull-right">
					<?php echo form_submit('btngravar', 'Próximo', 'class="btn btn-primary"'); ?>
				</div>
            <?php } ?>
			</footer>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>
<script type="text/javascript">
// $(document).ready(function(){
//    if($("#tipo_busca").val() == 1){
//       $('.endereco').removeAttr('readonly');
//       $('#pac-input').addClass('hidden');
//       $('.info-endereco').removeClass('hidden');
//       $('.info-estabelecimento').addClass('hidden');
//    } else {
//       $('.endereco').attr('readonly', 'readonly');
//       $('#pac-input').removeClass('hidden');
//       $('.info-endereco').addClass('hidden');
//       $('.info-estabelecimento').removeClass('hidden');
//    }
// });
// $('#tipo_busca').change(function(){
//    /*
//    1 - Endereço
//    2 - Nome do Estabelecimento
//    */
//    clearMarkers();
//    tipo_busca = $(this).val();
//    google.maps.event.trigger(map, 'resize');
//    if($(this).val() == 1){
//       $('.endereco').removeAttr('readonly');
//       $('#pac-input').addClass('hidden');
//       $('.info-endereco').removeClass('hidden');
//       $('.info-estabelecimento').addClass('hidden');
//    } else {
//       $('.endereco').attr('readonly', 'readonly');
//       $('.endereco').val('');
//       $('#pac-input').removeClass('hidden');
//       $('.info-endereco').addClass('hidden');
//       $('.info-estabelecimento').removeClass('hidden');
//    }
//    document.getElementById('place_id').value = ""; //latitude
//    place_id = "";
//    document.getElementById('LATITUDE_USER').value = ""; //latitude
//    document.getElementById('LONGITUDE_USER').value = ""; //longitude
//
// });
$("#cnpj").mask("99.999.999/9999-99");
$('.telefone').keyup(function(){
   v=$(this).val();
   v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
   v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
   v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
   $(this).val(v);
});
</script>
