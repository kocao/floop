<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$plan_contratado_row = $this -> users_model -> get_row_user_plan();
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
				</div>
				<h2 class="panel-title">Plano Contratado</h2>
			</header>
			<?php } ?>
			<div class="panel-body" style="display: block;">

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Plano contratado</label>
					<div class="col-md-6">
						<?php echo $plan_contratado_row -> descr_plano; ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Periodicidade</label>
					<div class="col-sm-8">
						<div class="row">
							<div class="visible-xs mb-md"></div>
							<div class="col-sm-4">
								<?php echo ucwords(strtolower($plan_contratado_row -> descr_periodo)); ?>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Valor</label>
					<div class="col-md-6">
						<?php echo number_format($plan_contratado_row -> valor,2,',','.'); ?>
					</div>
				</div>
			</div>
			<!--
			<footer class="panel-footer">

				<a class="btn btn-primary" href="<?php echo base_url('alterarplano'); ?>">Alterar Plano</a>
			</footer>
			-->
		</section>
	</div>
</div>
