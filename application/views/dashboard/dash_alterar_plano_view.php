<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$planos_rows = $this -> users_model -> get_rows_cad_planos();
// $periodos_rows = $this -> users_model -> get_rows_periodos();
$plan_contratado_row = $this -> users_model -> get_row_user_plan();

$periodos = [];
$idplanousr = 0;
$idperiodouser = 0;

if ($plan_contratado_row){
	$idplanousr = $plan_contratado_row -> id_plano;
	$idperiodouser = $plan_contratado_row -> id_periodo;
	$periodos = $this -> users_model -> get_planos_vlr($plan_contratado_row -> id_plano);
}

$dados['periodos'] = $periodos;
$dados['id_periodo_user'] = $idperiodouser;

echo validation_errors('<div class="row"><div class="col-lg-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>', '</p></strong></div></div></div>');

?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
				</div>
				<h2 class="panel-title">Detalhes do Plano</h2>
			</header>
			<?php } ?>
			<?php echo form_open('alterarplano/gravarplano'); ?>
			<?php echo form_hidden('HEADER_ACTION', $header_action); ?>
			<div class="panel-body" style="display: block;">
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Plano contratado <span class="required"> *</span></label>
					<div class="col-md-6">
						<select class="form-control mb-md" id="selectplanpag" name="selectplanpag" required <?php echo required_message() ?>>
							<option value=""></option>
							<?php if ($planos_rows){ ?>
								<?php foreach ($planos_rows as $key => $row){ ?>
									<?php
									$selected = '';
									if ($row['id_plano'] == $idplanousr){
										$selected = " selected='TRUE'";
									}
									?>
									<option value="<?php echo $row['id_plano']; ?>" <?php echo $selected;?>><?php echo $row['descr_plano']; ?></option>
								<?php } ?>
							<?php } ?>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Periodicidade <span class="required"> *</span></label>
					<div class="col-sm-8">
						<div class="row">
							<div class="visible-xs mb-md"></div>
							<div id="select_periodo" class="col-sm-4">
								<?php $this -> load -> view('dashboard/_periodos_select', $dados); ?>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Valor</label>
					<div class="col-md-6">
						<span id="vlrplanpag"> 						</span>

					</div>
				</div>
			</div>
			<?php #$this -> load -> view('dashboard/dash_sales_plan_view'); ?>
			<footer class="panel-footer">
				<div class="col-md-12">
						<div class="col-md-6 pull-left">
							<a href="<?php echo base_url().'dadospessoais' ?>" class="btn btn-primary">Anterior</a>
						</div>
						<div class="col-md-6">
							<?php echo form_submit('btngravar', 'Próximo', 'class="btn btn-primary pull-right"'); ?>
						</div>
				</div>
			</footer>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>
<script>
/*
selectplanpagper
selectplanpag
*/
      $(document).ready(function(){
         // $('#selectplanpag').change();
      });
		$('#selectplanpag').change(function(){
				fill_periodicidade($(this).val());
		});
		function fill_periodicidade(idplano){
			$.post("<?php echo base_url() ?>alterarplano/get_periodos", { id_plano: idplano}, function(data){
					$('#select_periodo').html(data);
					$('#vlrplanpag').html('');
			})

		}
		function fill_valor(idplano, idperiodo){
		  $.post("<?php echo base_url() ?>alterarplano/get_preco", { id_plano: idplano, id_periodo: idperiodo }, function(data){
		      $('#vlrplanpag').html(data);
		  })
		}
</script>
