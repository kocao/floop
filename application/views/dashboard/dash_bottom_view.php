<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
		<!-- Vendor -->
		<script src="<?php echo dash_assets_url(); ?>vendor/jquery/jquery.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo dash_assets_url(); ?>vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor CSS -->
		<?php //echo $file_view; ?>
		<?php switch ($file_view) {
			case 'dashboard/dash_finan_view':
		?>
			<script src="<?php echo dash_assets_url(); ?>vendor/select2/select2.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
			<!-- Ffinanceiro -->
			<script src="<?php echo dash_assets_url(); ?>javascripts/tables/dash.finan.js"></script>			
		<?php
				
				break;
		case 'dashboard/dash_users_view':
		?>
			<script src="<?php echo dash_assets_url(); ?>vendor/select2/select2.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
			<!-- cadastro de usuários -->
			<script src="<?php echo dash_assets_url(); ?>javascripts/tables/dash.users.js"></script>			
		<?php		
				break;				
		case 'dashboard/dash_cad_planos_view':
		?>
			<script src="<?php echo dash_assets_url(); ?>vendor/select2/select2.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
			<script src="<?php echo dash_assets_url(); ?>vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
			<!-- cadastrodeplanos -->
			<script src="<?php echo dash_assets_url(); ?>javascripts/tables/dash.cadplan.js"></script>
			<script src="<?php echo dash_assets_url(); ?>js/custom.js"></script>			
		<?php		
				break;
		case 'dashboard/dash_plano_contratado_view' || 'dashboard/dash_home_view':
		?>
			<script src="<?php echo dash_assets_url(); ?>js/custom.js"></script>
			<script>update_planos();</script>			
		<?php		
				break;
		default:
		?>
							
		<?php				
				break;
		}
		?>
				<!-- Theme Base, Components and Settings -->
				<script src="<?php echo dash_assets_url(); ?>javascripts/theme.js"></script>
				
				<!-- Theme Custom -->
				<script src="<?php echo dash_assets_url(); ?>javascripts/theme.custom.js"></script>
				
				<!-- Theme Initialization Files -->
				<script src="<?php echo dash_assets_url(); ?>javascripts/theme.init.js"></script>
		