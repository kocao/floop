<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Dados Pessoais</h2>
			</header>
			<?php } ?>
			<?php echo form_open_multipart('usuarios/gravar'); ?>
			<?php echo form_hidden('HEADER_ACTION', $header_action); ?>
			<?php echo form_hidden('iduser', $iduser); ?>
			<div class="panel-body" style="display: block;">
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label" for="inputReadOnly">E-mail</label>
						<div class="col-md-6">
							<p><?php echo $data_row -> email_user; ?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label" for="inputDefault">Nome</label>
						<div class="col-md-6">
							<p><?php echo $data_row -> nome_user; ?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Tipo</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="visible-xs mb-md"></div>
								<div class="col-sm-4">
									<select data-plugin-selecttwo="" class="form-control populate" id="selectplanpagper" name="selecttypeuser">
										<option value="1" <?php if ($data_row -> id_usertype == 1){echo 'selected="true"';}?>>Administrador</option>
										<option value="2" <?php if ($data_row -> id_usertype == 2){echo 'selected="true"';}?>>Usuário Padrão</option>
									</select>
								</div>
							</div>
	
						</div>
					
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Status</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="visible-xs mb-md"></div>
								<div class="col-sm-4">
									<select data-plugin-selecttwo="" class="form-control populate" id="selectplanpagper" name="selectstatususer">
										<option value="1" <?php if ($data_row -> ativo_inativo == 1){echo 'selected="true"';}?>>Ativo</option>
										<option value="0" <?php if ($data_row -> ativo_inativo == 0){echo 'selected="true"';}?>>Inativo</option>
									</select>
								</div>
							</div>
	
						</div>
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<?php echo form_submit('btngravar', 'Gravar', 'class="btn btn-primary"'); ?>
			</footer>
			<?php echo form_close(); ?>
		</section>
	</div>
</div>
