


<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$plan_contratado_row = $this -> users_model -> get_row_user_plan();
$pagamento_pendente = $this -> financeiro_model -> pagamento_pendente();
$pagamento_ativo = $this -> financeiro_model -> pagamento_ativo();


$tz = 'America/Sao_Paulo';
$timestamp = time();
$dt = new DateTime($pagamento_ativo['dtrequisicao'], new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$data_requisicao = $dt->format('d/m/Y');

if ($plan_contratado_row){
	$idplanousr = $plan_contratado_row -> id_plano;
	$idperiodouser = $plan_contratado_row -> id_periodo;
} else {
	redirect(base_url('alterarplano'));
}
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Efetuar Pagamento</h2>
			</header>
			<?php } ?>
			<div class="panel-body" style="display: block;">

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Plano contratado</label>
					<div class="col-md-6">
						<?php echo $plan_contratado_row -> descr_plano; ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Periodicidade</label>
					<div class="col-sm-8">
						<div class="row">
							<div class="visible-xs mb-md"></div>
							<div class="col-sm-4">
								<?php echo ucwords($plan_contratado_row -> descr_periodo); ?>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Valor</label>
					<div class="col-md-6">
						<?php echo number_format($plan_contratado_row -> valor,2,',','.'); ?>
					</div>
				</div>
            <?php if ($pagamento_pendente){ ?>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Requisição de pagamento</label>
   					<div class="col-md-6">
   						<?php echo $data_requisicao; ?>
   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Status</label>
   					<div class="col-md-6">
   						<?php echo $this -> financeiro_model -> descr_status($pagamento_ativo['status_finan']); ?>
   					</div>
   				</div>
               <div class="form-group">
                  <div class="col-md-6">
                     Existe uma requisição de pagamento pendente, a requisição foi realizada em <?php echo $data_requisicao ?>. Caso você já tenha pago, aguarde até o pagamento ser identificado. Se você ainda não efetuou o pagamento você pode pagar <a href="https://pagseguro.uol.com.br/v2/checkout/payment.html?code=<?php echo $pagamento_ativo['url_code']?>" target="_blank">clicando aqui</a>, ou se o link não estiver mais disponível você pode gerar outro link <a href="pagamento/pagar" target="_blank">clicando aqui</a>.
                  </div>
               </div>
            <?php } ?>
			</div>
			<footer class="panel-footer">
            <?php if (!$pagamento_pendente){ ?>
               <div class="row">
                  <div class="col-md-6 pull-left">
                     <a href="<?php echo base_url('anuncios') ?>" class="btn btn-primary">Anterior</a>
                  </div>
                  <div class="col-md-6 pull-right">
                     <a href="<?php echo base_url('pagamento/pagar') ?>" target="_blank" class="btn btn-primary pull-right">Pagar   <img src="<?php echo assets_url(); ?>img/logo_pagseguro2.png" /></a>
                  </div>
               </div>
            <?php } ?>
			</footer>
		</section>
	</div>
</div>
