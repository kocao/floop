<div id="main_area">
   <div class="row">
      <div class="col-sm-12 slider-thumbs" id="<?php echo $tipo ?>-thumbs">
         <?php
         foreach($fotos as $key => $foto) {
            $this -> load -> view('dashboard/_item_galeria', ['url_file' => $foto['url_file'], 'id_file' => $foto['id_file'], 'legenda' => $foto['legenda']]);
         }
         ?>
      </div>
   </div>
</div>
<div class="row">
<form id="<?php echo $tipo ?>" action="anuncios/upload_file" method="post" enctype="multipart/form-data">
   <div class="form-group">
      <div class="col-xs-10 col-md-offset-4 col-md-4 text-center">
         <!-- image-preview-filename input [CUT FROM HERE]-->
         <div class="input-group image-preview">
            <!-- <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
            <span class="input-group-btn">
               <!-- image-preview-clear button -->
               <!-- <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                  <span class="glyphicon glyphicon-remove"></span> Excluir
               </button> -->
               <!-- image-preview-input -->
               <?php if($tipo == "local") { ?>
                  <input class="form-control" type="text" name="legenda" value="" placeholder="Legenda"/>
               <?php } ?>
               <div class="btn btn-default image-preview-input">
                  <span class="glyphicon glyphicon-plus"></span>
                     <!-- <span class="image-preview-input-title">Escolher Arquivo</span> -->
                  <input type="hidden" name="id_user" value="<?php echo $id_user ?>">

                  <input type="file" accept="image/png, image/jpeg, image/gif" name="<?php echo $tipo ?>"/>
               </div>
            </span>
         </div><!-- /input-group image-preview [TO HERE]-->
      </div>
   </div>
</form>

<script>
$('#logo-thumbs').change(function(){
   alert('teste');
});
</script>
