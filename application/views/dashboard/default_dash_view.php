<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="fixed">
	<?php $this -> load -> view('dashboard/dash_head_view'); ?>
	<body>
		<section class="body">
			<?php $this -> load -> view('dashboard/dash_header_view'); ?>

			<div class="inner-wrapper">
				<?php $this -> load -> view('dashboard/dash_menu_view'); ?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $titulo_dash_view; ?></h2>

					</header>
					<!-- start: page -->
					<?php $this -> load -> view('flashdata_view'); ?>
					<?php $this -> load -> view($file_view); ?>
					<!-- end: page -->
				</section>
			</div>
		</section>
		<?php $this -> load -> view('dashboard/dash_bottom_view'); ?>
      <div id="overlay" style="display: none;">
            <img src="<?php echo assets_url()?>img/overlay.gif" class="loading_circle" alt="loading" />
      </div>
			<div class="lightbox-target col-md-12 text-center hidden" id="cota_prata">
				<img src="<?php echo assets_url(); ?>img/cota_prata.png" class="img-passo-zoom"/>
			<a class="lightbox-close" href="#"></a>
			<div class="lightbox-target col-md-12 text-center hidden" id="cota_bronze">
				<img src="<?php echo assets_url(); ?>img/cota_bronze.png" class="img-passo-zoom"/>
			<a class="lightbox-close" href="#"></a>
			<div class="col-md-12 text-center hidden" id="cota_ouro">
				<img class="img-passo-zoom" src="<?php echo assets_url(); ?>img/cota_ouro.png" class="img-passo-zoom"/>
			<a class="lightbox-close" href="#"></a>
   </div>



	</body>
</html>
<script>
function applyCoords(c){
  $("#coords").val("X : " + c.x + ", Y : " + c.y + ", X2 : " + c.x2+ ", Y2 : " + c.y2+ ", W : " + c.w+ ", H : " + c.h)
}

$('.lightbox-target').on('click', function(e) {
  if (e.target !== this)
    return;

  $('.mfp-close').click();
});
</script>
