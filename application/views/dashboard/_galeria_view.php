<div id="main_area">
   <div class="row">
      <div class="col-sm-12 slider-thumbs" id="<?php echo $tipo ?>-thumbs">
         <?php
         foreach($fotos as $key => $foto) {
            $this -> load -> view('dashboard/_item_galeria_view', ['url_file' => $foto['url_file'], 'id_file' => $foto['id_file'], 'legenda' => $foto['legenda']]);
         }
         ?>
      </div>
   </div>
</div>
