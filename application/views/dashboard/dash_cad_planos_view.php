<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="panel">
	<?php if ($header_action){	?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Cadastro de Planos</h2>
			</header>
			<?php } ?>
	<div class="panel-body">
		<!--
		<div class="row">
			<div class="col-sm-6">

				<div class="mb-md">
					<button id="addToTable" class="btn btn-primary">
						Incluir <i class="fa fa-plus"></i>
					</button>
				</div>

			</div>
		</div>
		-->
		<table class="table table-bordered table-striped mb-none" id="datatable-editable">
			<thead>
				<tr>
					<th>Descrição</th>
					<th>Qt.Max.Descr</th>
					<th>Qt.Max.Fotos</th>
					<th>Qt.Max.Eventos</th>
					<th>Opções</th>
				</tr>
			</thead>
			<tbody>

			<?php if ($rows){ ?>
				<?php foreach ($rows as $key => $row){ ?>
					<tr class="gradeA">
						<td class="nomeusr"><?php echo $row['descr_plano']; ?></td>
						<td class="nomeusr"><?php echo $row['maxdescr_plano']; ?></td>
						<td class="nomeusr"><?php echo $row['maxfotos_plano']; ?></td>
						<td class="nomeusr"><?php echo $row['maxfotoseventos_plano']; ?></td>

						<td class="actions">
							<a href="#" class="hidden on-editing save-row">
								<i class="fa fa-save"></i>
							</a>
							<a href="#" class="hidden on-editing cancel-row">
								<i class="fa fa-times"></i>
							</a>
							<a href="<?php echo base_url('planosdeanuncios/editar/' . $row['id_plano']); ?>" class="on-default edit-row">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="#" class="hidden on-default remove-row">
								<i class="fa fa-trash-o"></i>
							</a>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>



			</tbody>

		</table>
	</div>
</section>
<!-- end: page -->
