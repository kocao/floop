<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<div class="row"><div class="col-lg-12"><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>', '</p></strong></div></div></div>');
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title"><?php echo $titulo_dash_view; ?></h2>
			</header>
			<?php } ?>
			<?php echo form_open('planosdeanuncios/gravar'); ?>
			<?php echo form_hidden('HEADER_ACTION', $header_action); ?>
			<?php echo form_hidden('IDPLANO', $idplano); ?>
			<div class="panel-body" style="display: block;">
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label" for="inputDefault">Descrição</label>
						<div class="col-md-6">
							<input type="text" value="<?php echo $data_row -> descr_plano; ?>" name="DESCRICAO" class="form-control" placeholder="Nome do usuário">
						</div>
					</div>
				</div>
				<div class="row">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Nro. máximo de caracteres</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> maxdescr_plano; ?>" name="MAXDESCR_PLANO" class="form-control" placeholder="Nro. máximo de caracteres">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Nro. Máximo de fotos do local</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> maxfotos_plano; ?>" name="MAXFOTOS_PLANO" class="form-control" placeholder="Nro. Máximo de fotos">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Nro. Máximo de fotos do cardápio</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> maxfotoscardapio_plano; ?>" name="MAXFOTOSCARDAPIO_PLANO" class="form-control" placeholder="Nro. Máximo de fotos do cardápio">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Nro. Máximo de eventos</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> maxfotoseventos_plano; ?>" name="MAXFOTOSEVENTOS_PLANO" class="form-control" placeholder="Nro. Máximo de fotos de eventos">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Nro. Máximo de fotos de promoção</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> maxfotospromo_plano; ?>" name="MAXFOTOSPROMO_PLANO" class="form-control" placeholder="Nro. Máximo de fotos de promoções">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="divider">
					&nbsp;
				</div>
				<div class="row">
					<div class="form-group">
						<label class="col-md-3 control-label">Abreviatura do Plano</label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" value="<?php echo $data_row -> abrev_plano; ?>" name="ABREV_PLANO" class="form-control" placeholder="Nro. Máximo de eventos">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="divider">
					&nbsp;
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-sm-3 control-label">Outras opções:</label>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWFACEBOOK_PLANO" <?php
								if ($data_row -> showfacebook_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-project">Permitir link Facebook</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWYOUTUBE_PLANO" <?php
								if ($data_row -> showyoutube_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
							<label for="for-website">Permitir link Youtube</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWTWITTER_PLANO" <?php
								if ($data_row -> showtwitter_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-all">Permitir link Twitter</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWINSTAGRAM_PLANO" <?php
								if ($data_row -> showinstagram_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-all">Permitir link Instagram</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWGMAIS_PLANO" <?php
								if ($data_row -> showgmais_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-project">Permitir link G+</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWSITE_PLANO" <?php
								if ($data_row -> showsite_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
							<label for="for-website">Permitir link para website</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWRESERVA_PLANO" <?php
								if ($data_row -> showreserva_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-all">Permitir link para reservas</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input value="TRUE" type="checkbox" name="SHOWCOMPRAR_PLANO" <?php
								if ($data_row -> showcomprar_plano == 't') {echo 'checked=TRUE';
								}
 ?>>
								<label for="for-all">Permitir link para compra de ingressos</label>
							</div>
						</div>
					</div>
				</div>
				<div class="divider">
					&nbsp;
				</div>


				<?php
					if ($data_vlr){
				?>

				<?php
						foreach ($data_vlr as $key => $value) {
				?>

				<div class="row">
					<div class="col-md-4 form-group">
						<label class="col-md-3 control-label">Valor <?php echo $value['descr_periodo']; ?></label>
						<div class="col-md-6 control-label">
							<div class="input-group">
								<span class="input-group-addon"> <i class="fa fa-dollar"></i> </span>
								<input name="VALORES[<?php echo $idplano; ?>][<?php echo $value['id_periodo']; ?>]" value="<?php echo $value['valor']; ?>" data-plugin-masked-input="0" data-input-mask="99999.00" placeholder="0.00" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-md-6 form-group">
						<label class="col-md-3 control-label">Url pagto <?php echo $value['descr_periodo']; ?></label>
						<div class="col-md-6 control-label">
							<div class="col-md-12">

								<input type="text" name="URLSPAG[<?php echo $idplano; ?>][<?php echo $value['id_periodo']; ?>]" value="<?php echo $value['url_pagto']; ?>" placeholder="Url Pagamento Pagseguro" class="form-control">
							</div>
						</div>
					</div>
				</div>
					<?php
					} ?>

				<?php
				}
				?>
			</div>
			<footer class="panel-footer">
				<?php echo form_submit('btngravar', 'Gravar', 'class="btn btn-primary"'); ?>
			</footer>
			<?php echo form_close(); ?>
		</section>

	</div>
</div>
