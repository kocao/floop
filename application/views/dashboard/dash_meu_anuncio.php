<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$cardapios = $this->users_model->get_cardapios();
$locais = $this->users_model->get_locais();
$promocoes = $this->users_model->get_promocoes();
$id_user = $this->users_model->get_id_user();
$logo = $this->users_model->get_logo();
$dias_semana = ['Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado','Domingo'];
$horarios = $this -> anuncios_model -> get_horarios();
$eventos = $this -> anuncios_model -> get_eventos();
$dataplano = $this -> users_model -> get_plano_usuario();
// $anunciorow = $this -> db -> where('id_anuncio', $this -> anuncios_model -> get_id_anuncio())->get('anuncios')->result_array();
$id_anuncio = $this -> anuncios_model -> get_id_anuncio();
$anunciorow = $this -> db -> query("select anuncios.*, anuncios_status.descr_statusanuncio from anuncios inner join anuncios_status on anuncios.id_statusanuncio = anuncios_status.id_statusanuncio where id_anuncio = $id_anuncio")->result_array();
$plan_contratado_row = $this -> users_model -> get_row_user_plan();

   ?>

   <div class="row">
   	<div class="col-lg-12">

   		<section class="panel">
   			<header class="panel-heading">
   				<div class="panel-actions">
   					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
   				</div>
   				<h2 class="panel-title">Plano Contratado</h2>
   			</header>
   			<div class="panel-body" style="display: block;">
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Plano contratado</label>
   					<div class="col-md-6">
   						<?php echo $plan_contratado_row -> descr_plano; ?>
   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label">Periodicidade</label>
   					<div class="col-sm-8">
   						<div class="row">
   							<div class="visible-xs mb-md"></div>
   							<div class="col-sm-4">
   								<?php echo ucwords(strtolower($plan_contratado_row -> descr_periodo)); ?>
   							</div>
   						</div>

   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Valor</label>
   					<div class="col-md-6">
   						<?php echo number_format($plan_contratado_row -> valor,2,',','.'); ?>
   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Publicação</label>
   					<div class="col-md-6">
   						<?php echo date_view($anunciorow[0]['dt_pgto']) ?>
   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Vencimento</label>
   					<div class="col-md-6">
   						<?php echo date_view($anunciorow[0]['dt_vcto']) ?>
   					</div>
   				</div>
   				<div class="form-group">
   					<label class="col-md-3 control-label" for="inputDefault">Status</label>
   					<div class="col-md-6">
   						<?php echo $anunciorow[0]['descr_statusanuncio'] ?>
   					</div>
   				</div>
   			</div>
   			<!--
   			<footer class="panel-footer">

   				<a class="btn btn-primary" href="<?php echo base_url('alterarplano'); ?>">Alterar Plano</a>
   			</footer>
   			-->
   		</section>
   	</div>
   </div>

   <div class="row">
      <div class="col-lg-12">
         <section class="panel">
            <?php if ($header_action){
               ?>
               <header class="panel-heading">
                  <div class="panel-actions">
                     <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                  </div>
                  <h2 class="panel-title">Informações Sobre o Anúncio</h2>
               </header>
               <?php } ?>

               <div class="panel-body" style="display: block;">
                  <?php if(!empty($anunciorow[0]['descr_anuncio'])) { ?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="DESCR_ANUNCIO">Descrição</label>
                     <div class="col-md-6">
                           <textarea class="form-control" rows="9" disabled><?php echo $anunciorow[0]['descr_anuncio'] ?></textarea>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showsite_plano == true && !empty($anunciorow[0]['siteemp_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="siteemp_user">Site</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['siteemp_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showreserva_plano == true && !empty($anunciorow[0]['lnkreservamesa_user'])) {?>
                     <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Link para reservas</label>
                        <div class="col-md-6">
                           <span class="form-control"><?php echo $anunciorow[0]['lnkreservamesa_user']?></span>
                        </div>
                     </div>
                  <?php } ?>
                  <?php if ($dataplano -> showcomprar_plano == true && !empty($anunciorow[0]['lnkcomprar_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Link para venda de produtos</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnkcomprar_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showfacebook_plano == true && !empty($anunciorow[0]['lnkfacebook_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Facebook</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnkfacebook_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showyoutube_plano == true && !empty($anunciorow[0]['lnkyoutube_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Youtube</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnkyoutube_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showtwitter_plano == true && !empty($anunciorow[0]['lnktwitter_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Twitter</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnktwitter_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showinstagram_plano == true && !empty($anunciorow[0]['lnkinstagram_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Instagram</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnkinstagram_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <?php if ($dataplano -> showgmais_plano == true && !empty($anunciorow[0]['lnkgmais_user'])) {?>
                  <div class="form-group">
                     <label class="col-md-3 control-label" for="inputDefault">Google+</label>
                     <div class="col-md-6">
                        <span class="form-control"><?php echo $anunciorow[0]['lnkgmais_user']?></span>
                     </div>
                  </div>
                  <?php } ?>
                  <br><br>

               </section>
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3 class="panel-title">Horário de funcionamento</h3>
                           <div class="panel-actions">
                              <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="col-md-8 col-md-offset-2">
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th></th>
                                       <th class="text-center">Horário de Início</th>
                                       <th class="text-center">Horário de Término</th>
                                       <th class="text-center">Fechado</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php foreach ($dias_semana as $key => $dia) { ?>
                                       <tr>
                                          <td  class="text-center">
                                             <?php echo $dia ?>
                                          </td>
                                          <td class="text-center">
                                             <?php echo $horarios[$key]['fechado'] == "f" ? $horarios[$key]['horarioinicio'] : "" ?>
                                          </td>
                                          <td class="text-center">
                                             <?php echo $horarios[$key]['fechado'] == "f" ? $horarios[$key]['horariofim'] : "" ?>
                                          </td>
                                          <td class="text-center">
                                             <?php echo ($horarios[$key]['fechado'] == "t" ? "Sim" : "Não")?>
                                          </td>
                                       </tr>
                                       <?php }?>

                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php if (!empty($eventos)) {?>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="panel panel-default">
                              <div class="panel-heading">
                                 <h3 class="panel-title">Agenda de eventos</h3>
                                 <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                                 </div>
                              </div>
                              <div class="panel-body">
                                 <div class="col-md-12" id="eventos">
                                    <?php foreach ($eventos as $key => $evento) { ?>
                                       <div class="evento">
                                          <div class="form-group">
                                             <label class="col-md-3">Nome do evento</label>
                                             <div class="col-md-6">
                                                <span class="form-control"><?php echo $evento['complemento'] ?></span>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="col-md-3">Data</label>
                                             <div class="col-md-6">
                                                <span class="form-control"><?php echo $evento['data'] ?></span>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="col-md-3">Horário</label>
                                             <div class="col-md-6">
                                                <span class="form-control"><?php echo $evento['hora'] ?></span>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="col-md-3">Descrição do evento</label>
                                             <div class="col-md-6">
                                                <textarea class="form-control" rows=6 disabled><?php echo $evento['descricao'] ?></textarea>
                                             </div>
                                          </div>
                                          <div class="col-md-12">
                                             <hr>
                                          </div>
                                       </div>
                                       <?php }?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php } ?>
               <section class="panel">
                  <?php if ($header_action){
                     ?>
                     <header class="panel-heading">
                        <div class="panel-actions">
                           <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                        </div>
                        <h2 class="panel-title">Logo</h2>
                     </header>
                     <?php } ?>
                  <div class="panel-body">
                     <?php $this -> load -> view('dashboard/_galeria_view', ['fotos' => $logo, 'tipo' => 'logo']); ?>
                  </div>
               </section>
               <?php if ($dataplano -> maxfotos_plano >= 1) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Fotos do estabelecimento</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <?php $this -> load -> view('dashboard/_galeria_view', ['fotos' => $locais, 'tipo' => 'local']); ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <?php if (!empty($cardapios)) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Cardápio</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <?php $this -> load -> view('dashboard/_galeria_view', ['fotos' => $cardapios, 'tipo' => 'cardapio']); ?>

                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <?php } ?>
               <?php if (!empty($promocoes)) {?>
               <section>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <h3 class="panel-title">Promoções</h3>
                              <div class="panel-actions">
                                 <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
                              </div>
                           </div>
                           <div class="panel-body">
                              <?php $this -> load -> view('dashboard/_galeria_view', ['fotos' => $promocoes, 'tipo' => 'promocao']); ?>
                           </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <?php } ?>
                  <?php } ?>
                  <footer>
                     <?php if ($this -> anuncios_model -> anuncio_publicado() && $plan_contratado_row -> abrev_plano == 'B') {?>
                     <?php } else { ?>
                        <div class="row">
                           <div class="col-md-12 text-center">
                              <div class="col-md-2 col-md-offset-5">
                                 <a href="<?php echo base_url('anuncios') ?>" class="btn btn-primary">Editar Anúncio</a>
                              </div>
                           </div>
                        </div>
                     <?php } ?>
                  </footer>
               </div>
            </div>

            <script>
            </script>
