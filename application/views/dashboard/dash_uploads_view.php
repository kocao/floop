<?php if($this -> users_model -> get_id_anuncio_usuario() > 0){
	
	?>
	<script src="<?php echo dash_assets_url(); ?>uploads/dropzone/build.js?v=6"></script>
  <script>
    var Dropzone = require("enyo-dropzone");
    Dropzone.autoDiscover = false;
  </script>
  <link rel="stylesheet" href="<?php echo dash_assets_url(); ?>uploads/css/dropzone.css" />
  
		<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			
			<!-- Main content -->
			<div class="content-wrapper">
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading"><h5 class="panel-title">Selecionar Imagens</h5></div>
								<div class="panel-body">
									<div class="panel-body">
										<div class="row">
									          <div>
									            <span class="text-default">
									              <label for="LEGENDA_IMG">Legenda (para todas as novas imagens):</label>
									                <input type="text" class="form-control" name="LEGENDA_IMG" id="LEGENDA_IMG" placeholder="Informe a legenda" maxlength="100">
									            </span>
									            <input type="hidden" name="ID_TIPOIMG" id="ID_TIPOIMG" value="<?php echo $idtipoimg;?>">
									          </div>
											
									        <div id="actions" class="row">
									          <div class="col-lg-7">
									            <span class="btn btn-success fileinput-button">
									                <i class="glyphicon glyphicon-plus"></i>
									                <span>Adicionar</span>
									            </span>
									            <button type="submit" class="btn btn-primary start">
									                <i class="glyphicon glyphicon-upload"></i>
									                <span>Iniciar</span>
									            </button>
									            <button type="reset" class="btn btn-warning cancel">
									                <i class="glyphicon glyphicon-ban-circle"></i>
									                <span>Cancelar</span>
									            </button>
									          </div>
									          <div class="col-lg-5">
									            <span class="fileupload-process">
									              <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									              </div>
									            </span>
									          </div>
									        </div>
									        <div class="table table-striped files" id="previews">
									          <div id="template" class="file-row">
									            <div>
									                <span class="preview"><img data-dz-thumbnail /></span>
									            </div>
									            <div>
									                <p class="name" data-dz-name></p>
									                <strong class="error text-danger" data-dz-errormessage></strong>
									            </div>
									            <div>
									                <p class="size" data-dz-size></p>
									                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									                </div>
									            </div>
									            <div>
									              <button class="btn btn-primary start">
									                  <i class="glyphicon glyphicon-upload"></i>
									                  <span>Iniciar</span>
									              </button>
									              <button data-dz-remove class="btn btn-warning cancel">
									                  <i class="glyphicon glyphicon-ban-circle"></i>
									                  <span>Cancelar</span>
									              </button>
									              <button data-dz-remove class="btn btn-danger delete">
									                <i class="glyphicon glyphicon-trash"></i>
									                <span>Deletar</span>
									              </button>
									            </div>
									          </div>
									        </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /UPLOAD -->
					<!-- imaggns -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading"><h5 class="panel-title">Imagens Gravadas</h5></div>
								<div class="panel-body">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-3 col-sm-12 col-lg-3">Quantidade de imagens existentes: </div>
											<div class="col-md-4 col-sm-4 col-lg-4">
												<input name="" type="text" id="total_existentes" class="form-control" disabled="disabled" value="<?php echo count($files);?>" />
											</div>
											<div>&nbsp;</div>
											<div>&nbsp;</div>
										</div>
										<div class="row">
								<?php		if(!empty($files)): foreach($files as $arquivo): ?>
												<div class="col-md-4" id="imagem_<?php echo $arquivo['id_img']; ?>">
													
													<div>
														<div class="thumbnail">
															 <div class="thumb" style="text-align: center;height: 200px;background-image: url('<?php echo $this -> libfloop ->  get_url_img_user().$arquivo['file_img']; ?>');background-repeat: no-repeat;background-size: 100%;background-position: 50%;">
																
															 </div>
															 <div>&nbsp;</div>
															 <div>&nbsp;</div>
															 
															<div class="panel-footer">
																<ul>
																<?php /*
																	<li>
																		<?php echo $arquivo['file_img']. ' ';?>- Adicionado em <?php echo date("d/m/Y",strtotime($arquivo['data_upload'])); ?>
																	</li>
																	*/ ?>
																	<li>
																		<input type="text" class="form-control" name="LEGENDA_IMG" id="LEGENDA_<?php echo $arquivo['id_img']; ?>" value="<?php echo $arquivo['legenda_img'];?>" placeholder="Informe a legenda" maxlength="50">
																		<div>&nbsp;</div>
																		<button title="Salvar Legenda" class="btn btn-primary responsivoMeu legitRipple" onclick="atualizalegenda(<?php echo $arquivo['id_img']; ?>)"><i class="glyphicon glyphicon-floppy-disk"></i></button>
																		<button class="btn btn-danger delete" title="Deletar Imagem" onclick="excluirimagem(<?php echo $arquivo['id_img']; ?>)"><i class="glyphicon glyphicon-trash"></i></button>
																	</li>	 
																</ul>
															</div>
															
														</div>
													</div>
												</div>
								<?php 		endforeach; else: ?>
									                    	<p>Imagens não encontradas...</p>
								<?php 		endif; ?>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
    <script>
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);
		var imgDropzone = new Dropzone(document.body, {
			url: "<?php echo base_url('anuncios');?>",
			acceptedFiles: "image/jpeg,image/png,image/gif",
			thumbnailWidth: 80,
			thumbnailHeight: 80,
			parallelUploads: 20,
			previewTemplate: previewTemplate,
			autoQueue: false,
			previewsContainer: "#previews",
			clickable: ".fileinput-button"
		});
		imgDropzone.on("addedfile", function(file) {
			file.previewElement.querySelector(".start").onclick = function() { imgDropzone.enqueueFile(file); };
		});
		imgDropzone.on("totaluploadprogress", function(progress) {
			document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
		});
		imgDropzone.on("sending", function(file, xhr, formData, e) {
			document.querySelector("#total-progress").style.opacity = "1";
			formData.append("LEGENDA_IMG", $('#LEGENDA_IMG').val());
			formData.append("ID_TIPOIMG", $('#ID_TIPOIMG').val());
			file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
		});
		imgDropzone.on("queuecomplete", function(progress) {
			document.querySelector("#total-progress").style.opacity = "0";
		});
		document.querySelector("#actions .start").onclick = function() {
		    imgDropzone.enqueueFiles(imgDropzone.getFilesWithStatus(Dropzone.ADDED));
		};
		document.querySelector("#actions .cancel").onclick = function() {
			imgDropzone.removeAllFiles(true);
		};
		imgDropzone.on("complete", function (file) {
			imgDropzone.removeFile(file);
			if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
				location.reload();
			}
		});

		function excluirimagem(CODIGOIMG){
			var elemento = document.getElementById("imagem_" + CODIGOIMG);
			$.ajax({
			  url: "<?php echo base_url('anuncios/deletar_imagem'); ?>",
			  type: "POST",
			  data: {
			  	ID_IMG: CODIGOIMG,
			  	},
			  dataType:"json",
			  success: function(data) {
				if(data == '2'){
					console.log('erro ao excluir');
				} else {
					console.log('excluiu');
					elemento.parentElement.removeChild(elemento);
				}
			  },
			  error: function(jqXHR, textStatus, errorThrown){ 
			  	console.log(errorThrown);
			  	console.log(jqXHR.responseText);
			  	 }
			});
		}
		function atualizalegenda(CODIGOIMG){
			var LEGENDA;
			LEGENDA = $('#LEGENDA_'+CODIGOIMG).val();
			$.ajax({
			  url: "<?php echo base_url('anuncios/alterar_legenda'); ?>",
			  type: "POST",
			  data: {
			  	ID_IMG: CODIGOIMG,
			  	LEGENDA_IMG: LEGENDA,
			  	},
			  dataType:"json",
			  success: function(data) {
			  	if(data == '1'){
	                
			  	} else {
					}
			  },
			  error: function(jqXHR, textStatus, errorThrown){ 
			  	
			  	 }
			});
		}
				
    </script>
<?php }?>