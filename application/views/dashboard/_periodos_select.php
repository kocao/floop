<select data-plugin-selecttwo="" class="form-control populate" id="selectplanpagper" name="selectplanpagper" required <?php echo required_message() ?>>
  <option value=""></option>
  <?php foreach ($periodos as $key => $periodo) { ?>
    <option value="<?php echo $periodo['id_periodo'] ?>" <?php echo ($periodo['id_periodo'] == $id_periodo_user ? "selected='true')" : "" ) ?>><?php echo ucwords(strtolower($periodo['descr_periodo'])) ?></option>
  <?php } ?>
</select>
<script>
$('#selectplanpagper').change(function(){
    fill_valor($('#selectplanpag').val(), $(this).val());
});

</script>
