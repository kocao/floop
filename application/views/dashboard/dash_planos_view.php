<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title">Detalhes do Plano</h2>
			</header>
			<?php } ?>
			<div class="panel-body" style="display: block;">

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Plano contratado</label>
					<div class="col-md-6">
						<select class="form-control mb-md">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Periodicidade</label>
					<div class="col-sm-8">
						<div class="row">
							<div class="visible-xs mb-md"></div>
							<div class="col-sm-4">
								<select class="form-control mb-md">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Valor</label>
					<div class="col-md-6">
						<input type="text" value="Read-Only Input" id="inputReadOnly" class="form-control" readonly="readonly">
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<button class="btn btn-primary">
					Gravar
				</button>
			</footer>
		</section>
	</div>
</div>