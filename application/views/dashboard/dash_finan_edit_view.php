<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<?php if ($header_action){
			?>
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
					<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
				</div>
				<h2 class="panel-title"><?php echo $titulo_dash_view; ?></h2>
			</header>
			<?php } ?>
			<div class="panel-body" style="display: block;">
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Status</label>
					<div class="col-md-6">
						<select class="form-control mb-md">
							<option value="P">Pendente</option>
							<option value="Q">Quitado</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Parceiro</label>
					<div class="col-md-6">
						<select class="form-control mb-md">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="inputDefault">Descrição</label>
					<div class="col-md-6">
						<input type="text" value="" id="nomeuser" class="form-control" id="inputDefault" placeholder="Nome do usuário">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Valor</label>
					<div class="col-md-6 control-label">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-dollar"></i> </span>
							<input id="valor" data-plugin-masked-input="0" data-input-mask="99999,00" placeholder="0,00" class="form-control">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Vencimento</label>
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
							<input type="text" data-plugin-datepicker="" class="form-control">
						</div>
					</div>
				</div>

				
			</div>
			<footer class="panel-footer">
				<button class="btn btn-primary" onclick="grava_user_data()">
					Gravar
				</button>
			</footer>
		</section>

	</div>
</div>
