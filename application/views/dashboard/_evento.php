<div class="evento">
   <div class="form-group">
      <label class="col-md-3"for="complemento">Nome do evento</label>
      <div class="col-md-6">
         <input class="form-control desabilita" type="text" name="complemento[]" value="<?php echo $evento['complemento'] ?>">
      </div>
   </div>
   <div class="form-group">
      <label class="col-md-3"for="complemento">Data</label>
      <div class="col-md-6">
         <input class="form-control data desabilita" maxlength="10" type="text" name="data_evento[]" value="<?php echo $evento['data'] ?>">
      </div>
   </div>
   <div class="form-group">
      <label class="col-md-3"for="complemento">Horário</label>
      <div class="col-md-6">
         <?php echo time_select('hevento[]','mevento[]', $evento['hora'], 'desabilita') ?>
      </div>
   </div>
   <div class="form-group">
      <label class="col-md-3"for="complemento">Descrição do evento</label>
      <div class="col-md-6">
         <textarea class="form-control  desabilita" rows="6" type="textarea" name="descricao[]" ><?php echo $evento['descricao'] ?></textarea>
      </div>
   </div>
   <div class="col-md-1 col-md-offset-11">
      <button type="button" class="glyphicon glyphicon-trash btn btn-default fechado" onclick="$(this).closest('.evento').remove()" name="button"></button>
   </div>
   <div class="col-md-12">
      <hr>
   </div>
</div>
<script>
 $('.data').keyup(function(){
    v=$(this).val();
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d{2})(\d{4})/g,"$1/$2/$3"); //Coloca parênteses em volta dos dois primeiros dígitos
   //  v=v.replace(/^(\d{2})(\d{1,2})/g,"$1/$2"); //Coloca parênteses em volta dos dois primeiros dígitos
   //  v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    $(this).val(v);
 });


</script>
