<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if ($this->session->flashdata('msgerror')) {
	echo '<div class="container mt-xlg pt-xlg pb-xlg">';
	echo '<div class="row">';
	echo '<div>&nbsp;</div>';	
	echo '<div class="alert alert-dismissable alert-danger">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this->session->flashdata('msgerror') . '</strong>';
	echo '</div></div>';
	echo '</div>';
}

if ($this->session->flashdata('msgwarning')) {
	echo '<div class="container mt-xlg pt-xlg pb-xlg">';
	echo '<div class="row">';
	echo '<div>&nbsp;</div>';	
	echo '<div class="alert alert-dismissable alert-warning">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this->session->flashdata('msgwarning') . '</strong>';
	echo '</div></div>';
	echo '</div>';
} 

if ($this->session->flashdata('msginfo')) {
	echo '<div class="container mt-xlg pt-xlg pb-xlg">';
	echo '<div class="row">';
	echo '<div>&nbsp;</div>';	
	echo '<div class="alert alert-dismissable alert-info">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this->session->flashdata('msginfo') . '</strong>';
	echo '</div></div>';
	echo '</div>';
} 
 
if ($this->session->flashdata('msgdone')) {
	echo '<div class="container mt-xlg pt-xlg pb-xlg">';
	echo '<div class="row">';
	echo '<div>&nbsp;</div>';	
	echo '<div class="alert alert-dismissable alert-success">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this->session->flashdata('msgdone') . '</strong>';
	echo '</div></div>';
	echo '</div>';
}     		
?>