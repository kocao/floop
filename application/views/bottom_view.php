<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- terceiros -->
		<script src="<?php echo assets_url(); ?>js/jquery.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.appear.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.easing.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery-cookie.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/bootstrap.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/common.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.validation.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.easy-pie-chart.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.gmap.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.lazyload.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.isotope.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/owl.carousel.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/vide.min.js"></script>
		
		<!-- comoponentes e configuracoes do tema -->
		<script src="<?php echo assets_url(); ?>js/themefloop.js"></script>
		
		<!-- jquery -->
		<script src="<?php echo assets_url(); ?>js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo assets_url(); ?>js/jquery.themepunch.revolution.min.js"></script>

		<!-- views contatos -->
		<script src="<?php echo assets_url(); ?>js/view.contact.js"></script>

		<!-- principal -->
		<script src="<?php echo assets_url(); ?>js/floop.js"></script>
		
		<!-- customizacao -->
		<script src="<?php echo assets_url(); ?>js/custom.js"></script>
		
		<!-- arquivos de inicializacao -->
		<script src="<?php echo assets_url(); ?>js/theme.init.js"></script>