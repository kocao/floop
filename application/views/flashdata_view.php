<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($this -> session -> flashdata('msgerror')) {
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	echo '<div>&nbsp;</div>';
	echo '<div class="alert alert-dismissable alert-danger">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this -> session -> flashdata('msgerror') . '</strong>';
	echo '</div></div>';
	echo '</div>';
	if(isset($_SESSION['msgerror'])){
    	unset($_SESSION['msgerror']);
	}
}

if ($this -> session -> flashdata('msgwarning')) {
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	echo '<div>&nbsp;</div>';
	echo '<div class="alert alert-dismissable alert-warning">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this -> session -> flashdata('msgwarning') . '</strong>';
	echo '</div></div>';
	echo '</div>';
	if(isset($_SESSION['msgwarning'])){
    	unset($_SESSION['msgwarning']);
	}
}

if ($this -> session -> flashdata('msginfo')) {
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	echo '<div>&nbsp;</div>';
	echo '<div class="alert alert-dismissable alert-info">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this -> session -> flashdata('msginfo') . '</strong>';
	echo '</div></div>';
	echo '</div>';
	if(isset($_SESSION['msginfo'])){
    	unset($_SESSION['msginfo']);
	}	
}

if ($this -> session -> flashdata('msgdone')) {
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	echo '<div>&nbsp;</div>';
	echo '<div class="alert alert-dismissable alert-success">';
	echo '<button type="button" class="close" data-dismiss="alert">×</button>';
	echo '<strong>' . $this -> session -> flashdata('msgdone') . '</strong>';
	echo '</div></div>';
	echo '</div>';
	if(isset($_SESSION['msgdone'])){
    	unset($_SESSION['msgdone']);
	}	
}


?>