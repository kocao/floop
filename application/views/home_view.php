<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="hidden-xs">
	<p>
		&nbsp;
	</p>
</div>

<section class="background-color-light pt-xlg row" style="margin: auto;" id="intro">
	<div class="container">
		<div class="row">
			<div class="col-md-6 intro col-sm-12 col-xs-12">
				<div class="col-md-12 introtxt">
						<h2 class="text-center header-intro" style="font-family: 'ABeeZee', sans-serif; color: #dc4439;	font-weight: bold;">Saiba a lotação de vários tipos de estabelecimentos antes de chegar lá.</h2>


						<h2 class="text-center body-intro"> O Floop é um aplicativo que busca facilitar a vida das pessoas, informando-as em tempo real a respeito da lotação de determinado estabelecimento, a partir de informações fornecidas pelos próprios usuários. </h2>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="video-row">
					 <div class="embed-responsive embed-responsive-4by3">
								<iframe allowfullscreen="" class="embed-responsive-item"  src="https://www.youtube.com/embed/3tpvWNViyk0?feature=player_embedded&amp;autoplay=0&amp;modestbranding=1&amp;showinfo=0&amp;showsearch=0&amp;wmode=transparent&amp;autohide=1&amp;controls=1&rel=0">
								</iframe>
					   </div>
				</div>
			</div>
		</div>
		<p>
			&nbsp;
		</p>
		<div class="row">
			<div class="col-md-12">
					<h3 class="text-center" style="font-family: 'ABeeZee', sans-serif; color: #dc4439;	font-size: 1.9em;font-weight: bold;"> O SEU TEMPO MAIS VALIOSO </h3>
	  	</div>
			<div class="col-md-12 text-center store-icons">
				<a href="https://appsto.re/br/SzXkjb.i"> <img alt="Floop" class="imgappstore" src="<?php echo assets_url(); ?>img/app-store-original.png"> </a>
				<a href="https://play.google.com/store/apps/details?id=br.com.newvisionsappcenter.floop"> <img alt="Floop" class="imggoogleplay" src="<?php echo assets_url(); ?>img/google_play.png"> </a>
				<span class="imgbtnsocial">
					<a href="<?php echo $this->config->item('site_facebook'); ?>" target="_blank"> <img alt="Floop" src="<?php echo assets_url(); ?>img/facebook.png"> </a>
					<a href="<?php echo $this->config->item('site_instagram'); ?>" target="_blank"> <img alt="Floop" src="<?php echo assets_url(); ?>img/instagram.png"> </a>
					<a href="<?php echo $this->config->item('site_twitter'); ?>" target="_blank"> <img alt="Floop" src="<?php echo assets_url(); ?>img/twitter.png"> </a>
				</span>
			</div>
		</div>
	</div>
</section>


<section class="background-color-light pt-xlg" id="home">
	<div class="container mt-xlg pt-xlg pb-xlg">
		<div class="row mt-xl" style="background: #f4f4f4;">
			<div class="col-md-12" style="">
				<div class="col-md-8 col-sm-12 col-xs-12" style="">
					<p>
						&nbsp;
					</p>
					<h2 class="mb-xs mt-xl text-center" style="font-family: 'ABeeZee', sans-serif;">Juntos somos mais fortes!</h2>
					<p class="lead mb-xl text-center" style="font-family: 'Roboto', sans-serif; text-align: center;">
						Com no máximo 3 cliques, você informa a lotação do estabelecimento em que está no momento, se há filas, e ainda ajuda outras pessoas. A precisão das informações do app só depende de você!.
					</p>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12" style="">
					<img alt="Floop" class="img-responsive center-block" src="<?php echo assets_url(); ?>img/juntos_mais_fortes.png">
				</div>
			</div>
		</div>

	  <div class="row mt-xl">
			<div class="col-md-12">
				<div class="col-md-8 col-sm-12 col-xs-12 col-md-push-4">
					<p>
						&nbsp;
					</p>
					<h2 class="mb-xs mt-xl" style="font-family: 'ABeeZee', sans-serif; text-align: center;">Qual é o seu perfil?</h2>
					<p class="lead mb-xl" style="font-family: 'Roboto', sans-serif; text-align: center;">
						Com o Floop, você utiliza as informações do aplicativo da forma como achar melhor. Você pode evitar filas de espera ou encontrar os lugares que estão bombando no momento!
					</p>

				</div>
				<div class="col-md-4 col-sm-12 col-xs-12 col-md-pull-8">
					<img alt="Floop" class="img-responsive center-block" src="<?php echo assets_url(); ?>img/qual_seu_perfil.png">
				</div>
			</div>
		</div>
		<div class="row mt-xl" style="background: #f4f4f4;">
			<div class="col-md-12">
				<div class="col-md-8 col-sm-12 col-xs-12 text-center" >
					<div class="hidden-xs">
					<p>
						&nbsp;
					</p>
					<p>
						&nbsp;
					</p>
				</div>
					<h2 class="mb-xs mt-xl" style="font-family: 'ABeeZee', sans-serif;">Estabelecimentos não faltam para você consultar</h2>
					<p class="lead mb-xl" style="font-family: 'Roboto', sans-serif;">
						São milhares de estabelecimentos cadastrados em todo o Brasil, de vários tipos de categorias, desde serviços que você utiliza no dia-a-dia, até aqueles destinados apenas para seu bem-estar e divertimento!
					</p>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12" style="padding: 2% 0;">
					<img alt="Floop" class="img-resposive center-block" src="<?php echo assets_url(); ?>img/estabelecimentos.png">
				</div>
			</div>
		</div>
		<div class="row" >
			<div class="col-md-12">
				<div class="col-md-8 col-sm-12 col-xs-12 col-md-push-4">
					<p>
						&nbsp;
					</p>
					<h2 class="mb-xs mt-xl" style="font-family: 'ABeeZee', sans-serif; text-align: center;">O seu evento no Floop!</h2>
					<p class="lead mb-xl" style="font-family: 'Roboto', sans-serif; text-align: center;">
						Com o Floop, você pode adicionar um evento temporário no mapa e outros usuários poderão ver sua  lotação. Pode ser um show, uma festa, um churrasco, ou o que estiver bombando na sua cidade.
					</p>
				</div>
				<div class="col-md-4 col-md-4 col-sm-12 col-xs-12 col-md-pull-8">
					<img alt="Floop" class="img-responsive center-block" src="<?php echo assets_url(); ?>img/seu_evento.png">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section section-no-border mt-none" id="plans">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<h2 class="heading-dark mt-xl mb-none"><strong>VOCÊ SABIA?</strong></h2>
				<p class="mb-xl"></p>
			</div>
		</div>
		<div class="row mt-xl">
			<div class="counters counters-text-dark">
				<div class="col-md-4 col-sm-12">
					<div class="counter appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
						<i class="fa fa-user"></i>
						<strong data-to="93" data-append="%">93%</strong>
						<label>Das pessoas </label>
						<p class="text-color-primary mb-xl">
							gostariam de saber a lotação de um estabelecimento antes de chegar nele
						</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="counter appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
						<i class="fa fa-user-plus"></i>
						<strong data-to="79" data-append="%">79%</strong>
						<label>Das pessoas</label>
						<p class="text-color-primary mb-xl">
							gostariam de ver anúncios e novidades sobre os estabelecimentos no aplicativo
						</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="counter appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
						<i class="fa fa-users"></i>
						<strong data-to="78" data-append="%">78%</strong>
						<label>Das pessoas</label>
						<p class="text-color-primary mb-xl">
							consideram as filas no Brasil como um problema
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 center">
				<p  style="color: black;">
					Fonte: Pesquisa realizada com 600 pessoas de diferentes cidades do Brasil
				</p>
			</div>
		</div>
      <p>
         &nbsp;
      </p>

		<div class="row">
			<div class="col-md-12 center">
            <h2 class="heading-dark mt-xl mb-none"><strong>O Floop oferece ainda opções de anúncios totalmente customizados para você divulgar o seu negócio. Escolha abaixo o melhor plano, monte seu anúncio pelo nosso site e floope com a gente!
            </strong></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 center">
				<h2 class="heading-dark mt-xl mb-none"><strong>COMO FUNCIONA?</strong></h2>
			</div>
		</div>
      <p>
         &nbsp;
      </p>

		<div class="row">
         <div class="col-md-4">
            <a class="lightbox" href="#passo1" onclick="$('#passo1').removeClass('hidden')">
               <img src="<?php echo assets_url(); ?>img/passo01.png" class="img-passo"/>
            </a>

            <span class="blog-item-content">
               <span class="category text-uppercase font-weight-semibold text-danger">Passo 01</span>
            <div class="blog-item">
                  <p class="mb-xlg">
                     O usuário seleciona o estabelecimento desejado no mapa.
                  </p>
            </div>
            </span>
         </div>
         <div class="col-md-4">
            <a class="lightbox" href="#passo2" onclick="$('#passo2').removeClass('hidden')">
               <img src="<?php echo assets_url(); ?>img/passo02.png" class="img-passo"/>
            </a>
            <span class="blog-item-content">
               <span class="category text-uppercase font-weight-semibold text-danger">Passo 02 </span>
               <p class="mb-xlg">
                  Aberta a tela de notificações, o usuário clica no ícone de informações adicionais, que só aparece para nossos estabelecimentos parceiros.
               </p>
            </span>
         </div>
         <div class="col-md-4">
            <a class="lightbox" href="#passo3" onclick="$('#passo3').removeClass('hidden')">
               <img src="<?php echo assets_url(); ?>img/passo03.png" class="img-passo"/>
            </a>
            <span class="blog-item-content"> <span class="category text-uppercase font-weight-semibold text-danger">Passo 03</span>
               <p class="mb-xlg">
                  A tela de anúncios aparece customizada para o usuário, de acordo com o seu plano e as informações que você colocou em nosso site.
               </p>
            </span>
         </div>
		</div>
      <br><br><br><br>
		<?php $this->load->view('sales_plan_view'); ?>
	</div>
</section>

<section id="contato" class="pt-xlg">
	<div class="container pt-xlg mt-xlg">
		<div class="row pt-sm mb-md">
			<div class="col-md-12">
				<h2 class="text-color-dark text-uppercase font-weight-bold center mb-xs">Entre em Contato</h2>
				<p class="custom-font-size-1 center mb-sm">
					Informações de contato
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1">
				<div class="row">
					<div class="col-md-12">
						<div id="note" style="text-align: center;font-family: 'ABeeZee', sans-serif; color: #dc4439;"></div>
						<div id="fields">
							<form id="ajax-contato-form">
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>Nome *</label>
											<input type="text" value="" data-msg-required="Informe seu nome." maxlength="100" class="form-control" name="name" id="name" required="" aria-required="true">
										</div>
										<div class="col-md-6">
											<label>E-mail *</label>
											<input type="email" value="" data-msg-required="Informe seu e-mail." data-msg-email="Informe um e-mail válido." maxlength="100" class="form-control" name="email" id="email" required="" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Assunto</label>
											<input type="text" value="" data-msg-required="Informe o assunto." maxlength="100" class="form-control" name="subject" id="subject" required="" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Mensagem *</label>
											<textarea maxlength="5000" data-msg-required="Escreva uma mensagem." rows="10" class="form-control" name="message" id="message" required aria-required="true"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Enviar" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
