<br><br><br><br><br><br>
            <div class="col-md-12 text-center">
              <h3>Política de Privacidade</h3>
            </div>
            <div class="col-md-8 col-md-offset-2 modal-body">
							<p>
								A Política de Privacidade está incorporada nos termos de uso do aplicativo
								(para mais informações consulte nossos Termos de Uso) e descreve as informações
								que o Floop coleta dos usuários e como essas informações são utilizada pelos nossos
								serviços. Se você acredita que sua privacidade está sendo violada de alguma forma,
								por favor entre em contato conosco via os canais de atendimento no site ou no
								aplicativo. O Floop respeita acima de tudo a privacidade de seus usuários.
							</p>
							<p>
								Nossa Política de Privacidade pode ser alterada de tempo em tempo,
								por favor visite sempre nossa página para consultar a situação vigente. O uso do Floop significa
								aceitar, expressamente e sem exceções, os termos abaixo descritos.
							</p>
							<p><b>Informações coletadas</b>
							</p>
							<p>
								Para acessar o Floop, é necessário fazer cadastro com um email válido ou
								então logar com as redes sociais Facebook ou Google+ (mais informações no próximo
								item). Na opção de cadastro via email, será solicitado algumas informações básicas:
								nome completo, data de nascimento, sexo, cidade, número de celular e foto. As
								informações requeridas em ambos os casos tem como objetivo principal entender o
								perfil de usuário que acessa o aplicativo com a finalidade de melhorá-lo de forma
								contínua e direcionada a seu principal público-alvo. O Floop também pode usar essas
								informações para:
							</p>
							<p>
                     <ul>
								<li>Enviar notícias, novidades ou anúncios sobre o aplicativo.</li>
								<li>Prover suporte adequado e rápido em caso de dúvidas ou problemas.</li>
								<li>Conduzir pesquisas ou questionários visando melhorar o aplicativo para seus usuários.</li>
								<li>Enviar eventualmente promoções ou promover marketing sobre os nossos estabelecimentos parceiros.</li>
								<li>Reforçar e comunicar mudanças nos Termos de Uso ou Política de privacidade.</li>
								<li>Contatar o usuário sempre que acreditar ser estritamente necessário.</li>
                     </ul>
							</p>
							<p>
								Sempre que o usuário estiver utilizando-o, o Floop coletará sua localização
								detalhada. Isso é necessário uma vez que só é possível notificar sobre a lotação de
								determinado estabelecimento quando estiver próximo ao mesmo. Além disso, sua
								localização é importante para outros fins como por exemplo a função &quot;Olhar ao redor&quot;
								ou exibir resultados de estabelecimentos próximos.
							</p>
							<p><b>Integração com redes sociais</b>
							</p>
							<p>
								Ao clicar no login com redes sociais, o usuário concorda automaticamente no
								compartilhamento de informações (inclusive pessoais), que estiverem em seu perfil
								público, com o Floop.
							</p>
							<p>
								<b>Notificações</b>
							</p>
							<p>O Floop pode enviar notificações Push sempre que houver um aviso de lotação de determinado estabelecimento e outro usuário estiver próximo desse local. Essa é uma das formas do aplicativo confirmar informações dadas pelos seus usuários com o objetivos de prover informações com mais alto grau de confiabilidade. O usuário pode desativar essas notificações através das configurações no aplicativo. As notificações dadas são anônimas, isto é, quem notificou não pode ser identificado por outras pessoas.
							</p>

							<p><b>Compartilhamento de dados pessoais</b></p>

							<p>O Floop não comercializará sob nenhuma hipótese seus dados pessoais a terceiros, nem liberará suas informações, sem o seu consentimento, a menos que haja alguma determinação da justiça ou se comprovada qualquer atitude/comportamento que infrija as leis vigentes no país.
							</p>
							<p><b>Deletar conta</b></p>

							<p>Se desejar parar de utilizar o Floop e/ou cancelar sua inscrição por algum motivo, a opção de deletar a conta está disponível no Menu do aplicativo </p>

							<p><b>Anunciantes</b></p>

							<p>Os estabelecimentos que desejam anunciar no Floop devem criar uma conta e fazer login somente pelo site do aplicativo. Ao cadastrá-las, serão necessárias algumas informações básicas como por exemplo: Nome do estabelecimento, email para contato, endereço, CNPJ, etc. O Floop utilizará tais informações para identificar seus parceiros e localizar de forma correta a localização do estabelecimento anunciante no mapa do aplicativo. Por isso é muito importante que o endereço informado no cadastro corresponda ao idêntico endereço no mapa. O Floop também pode utilizar essas informações para:
							</p>

							<p>
							<ul>
							<li>Enviar notícias ou novidades sobre o aplicativo.</li>
							<li>Prover suporte adequado e rápido em caso de dúvidas ou problemas.</li>
							<li>Conduzir pesquisas ou questionários visando melhorar o marketing dos estabelecimentos parceiros e o alcance de seus anúncios.</li>
							<li>Enviar promoções e novidades sobre alteração nos planos disponíveis para anúncios.</li>
							<li>Reforçar e comunicar mudanças nos Termos de uso ou Política de privacidade.</li>
							<li>Contatar o estabelecimento sempre que acreditar ser estritamente necessário.</li>
							</ul>
							</p>

							<p>Baseado no plano contratado, o estabelecimento pode incluir vários tipos de informação em seu anùncio customizado, como por exemplo fotos, promoções, cardápios, etc. Essas informações serão exibidas para todos os usuários do aplicativo, mediante clique em botão específico dentro do balão do estabelecimento no mapa.
							</p>
							<p><b>Segurança de informação</b></p>

							<p>O Floop preza pela segurança das informações de seus usuários. No entanto, o usuário não deve atestar ou concluir que o Floop está completamente imune a ataques de pessoas mal intencionadas, falhas de sistema, acesso, ou ainda qualquer tipo de mau-uso. Portanto, o usuário deve estar ciente de que o aplicativo não pode oferecer garantia em relação a seus dados, embora se comprometa a tomar medidas necessárias para sempre evoluir seu sistema de segurança de informações.
							</p>
						</div>
