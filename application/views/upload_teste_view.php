<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	
	<title>Floop - Teste de upload</title>
	<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>vendor/bootstrap/css/bootstrap.css" />
	<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo dash_assets_url(); ?>stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo dash_assets_url(); ?>vendor/modernizr/modernizr.js"></script>
	
	<script src="<?php echo dash_assets_url();?>uploads/dropzone/build.js?v=6"></script>
  <script>
    var Dropzone = require("enyo-dropzone");
    Dropzone.autoDiscover = false;
  </script>
  <style>
    html, body {
      height: 100%;
    }
    #actions {
      margin: 2em 0;
    }
    div.table {
      display: table;
    }
    div.table .file-row {
      display: table-row;
    }
    div.table .file-row > div {
      display: table-cell;
      vertical-align: top;
      border-top: 1px solid #ddd;
      padding: 8px;
    }
    div.table .file-row:nth-child(odd) {
      background: #f9f9f9;
    }
    #total-progress {
      opacity: 0;
      transition: opacity 0.3s linear;
    }
    #previews .file-row.dz-success .progress {
      opacity: 0;
      transition: opacity 0.3s linear;
    }
    #previews .file-row .delete {
      display: none;
    }
    #previews .file-row.dz-success .start,
    #previews .file-row.dz-success .cancel {
      display: none;
    }
    #previews .file-row.dz-success .delete {
      display: block;
    }
  </style>
</head>
<html>
<body>
	<?php //$this -> load -> view('common/header_logado'); ?>
		<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php //$this -> load -> view('common/menu_logado'); ?>
			<!-- Main content -->
			<div class="content-wrapper">
				<?php //$this -> load -> view('common/bread'); ?>
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading"><h5 class="panel-title"><?php echo $pagetitle ?></h5></div>
								<div class="panel-body">
									<div class="panel-body">
										<div class="row">
									        <div id="actions" class="row">
									          <div class="col-lg-7">
									            <span class="btn btn-success fileinput-button">
									                <i class="glyphicon glyphicon-plus"></i>
									                <span>Adicionar</span>
									            </span>
									            <button type="submit" class="btn btn-primary start">
									                <i class="glyphicon glyphicon-upload"></i>
									                <span>Iniciar</span>
									            </button>
									            <button type="reset" class="btn btn-warning cancel">
									                <i class="glyphicon glyphicon-ban-circle"></i>
									                <span>Cancelar</span>
									            </button>
									          </div>
									          <div class="col-lg-5">
									            <span class="fileupload-process">
									              <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									              </div>
									            </span>
									          </div>
									        </div>
									        <div class="table table-striped files" id="previews">
									          <div id="template" class="file-row">
									            <div>
									                <span class="preview"><img data-dz-thumbnail /></span>
									            </div>
									            <div>
									                <p class="name" data-dz-name></p>
									                <strong class="error text-danger" data-dz-errormessage></strong>
									            </div>
									            <div>
									                <p class="size" data-dz-size></p>
									                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									                </div>
									            </div>
									            <div>
									              <button class="btn btn-primary start">
									                  <i class="glyphicon glyphicon-upload"></i>
									                  <span>Iniciar</span>
									              </button>
									              <button data-dz-remove class="btn btn-warning cancel">
									                  <i class="glyphicon glyphicon-ban-circle"></i>
									                  <span>Cancelar</span>
									              </button>
									              <button data-dz-remove class="btn btn-danger delete">
									                <i class="glyphicon glyphicon-trash"></i>
									                <span>Deletar</span>
									              </button>
									            </div>
									          </div>
									        </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /UPLOAD -->
					<!-- PRODUTOSGRAVADAS -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading"><h5 class="panel-title">Produtos</h5></div>
								<div class="panel-body">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-3 col-sm-12 col-lg-3">Quantidade de produtos existentes: </div>
											<div class="col-md-9 col-sm-12 col-lg-9">
												<input name="" type="text" id="total_existentes" class="form-control" disabled="disabled" value="<?php echo $numeroRegistros;?>" />
											</div>
										</div>
										<div class="row">
								<?php		if(!empty($files)): foreach($files as $arquivo): ?>
												<div class="col-md-4" id="imagem_<?php echo $arquivo['codigo_produto']; ?>">
													<div class="panel-heading"><a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
													<div class="panel-body">
														<div class="thumbnail">
															 <div class="thumb" style="text-align: center;height: 200px;background-image: url('<?php echo base_url($path_upload.$arquivo['nome_arquivo']); ?>');background-repeat: no-repeat;background-size: 100%;background-position: 50%;">
																<a  class="lightBoxFoto" id="light_<?php echo $arquivo['codigo_produto']; ?>" href="<?php echo base_url($path_upload.$arquivo['nome_arquivo']); ?>" rel="example_group"  data-popup="lightbox">
																	<span class="zoom-image"><i class="icon-plus2"></i></span>
																</a>
															 </div>
															 
															<div class="panel-footer">
																<ul> <?php /*
																	<li>
																		<?php echo $arquivo['nome_arquivo']. ' ';?>- Adicionado em <?php echo date("d/m/Y",strtotime($arquivo['data_upload'])); ?>
																	</li> */ ?>
																	<li>
																		<input type="text" class="form-control" name="descricao" id="descri_<?php echo $arquivo['codigo_produto']; ?>" value="<?php echo $arquivo['descricao'];?>" placeholder="Informe a descrição" maxlength="50">
																		<input type="text" class="form-control" name="valor" id="valor_<?php echo $arquivo['codigo_produto']; ?>" value="<?php echo $arquivo['valor'];?>" placeholder="Informe o valor" maxlength="15">
																		<button title="Salvar Descrição" class="btn btn-primary responsivoMeu legitRipple" onclick="atualizaProduto(<?php echo $arquivo['codigo_produto']; ?>)"><i class="glyphicon glyphicon-floppy-disk"></i></button>
																		<button class="btn btn-danger delete" title="Deletar Produto" onclick="confirmaexcluirProduto(<?php echo $arquivo['codigo_produto']; ?>,'<?php echo $arquivo['nome_arquivo']; ?>')"><i class="glyphicon glyphicon-trash"></i></button>
																	</li>	 
																</ul>
															</div>
															
														</div>
													</div>
												</div>
								<?php 		endforeach; else: ?>
									                    	<p>Produto(s) não encontrada(s)...</p>
								<?php 		endif; ?>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					<!-- /PRODUTOSGRAVADAS -->
					<!-- Footer -->
					<?php //$this -> load -> view('common/footer'); ?>
					<!-- /footer -->
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
    <script>
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);
		var produtoDropzone = new Dropzone(document.body, {
			url: "<?php echo base_url('uploadteste');?>",
			acceptedFiles: "image/jpeg,image/png,image/gif",
			thumbnailWidth: 80,
			thumbnailHeight: 80,
			parallelUploads: 20,
			previewTemplate: previewTemplate,
			autoQueue: false,
			previewsContainer: "#previews",
			clickable: ".fileinput-button"
		});
		produtoDropzone.on("addedfile", function(file) {
			file.previewElement.querySelector(".start").onclick = function() { produtoDropzone.enqueueFile(file); };
		});
		produtoDropzone.on("totaluploadprogress", function(progress) {
			document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
		});
		produtoDropzone.on("sending", function(file, xhr, formData, e) {
			document.querySelector("#total-progress").style.opacity = "1";
			file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
		});
		produtoDropzone.on("queuecomplete", function(progress) {
			document.querySelector("#total-progress").style.opacity = "0";
		});
		document.querySelector("#actions .start").onclick = function() {
		    produtoDropzone.enqueueFiles(produtoDropzone.getFilesWithStatus(Dropzone.ADDED));
		};
		document.querySelector("#actions .cancel").onclick = function() {
			produtoDropzone.removeAllFiles(true);
		};
		produtoDropzone.on("complete", function (file) {
			produtoDropzone.removeFile(file);
			if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
				location.reload();
			}
		});
		function confirmaexcluirProduto(CODIGOPRODUTO,NOMEARQUIVO){
		var formandosel = "";
		var turmasel = "";
		
		swal(
			{
				title: "Tem certeza?",
				text: "Tem certeza que deseja excluir essa produto?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#EF5350",
				confirmButtonText: "Sim, Excluir",
				cancelButtonText: "Não, Cancelar",
				closeOnConfirm: false,
				closeOnCancel: false
			},
	        function(isConfirm){
	            if (isConfirm) {
	            	excluirProduto(CODIGOPRODUTO,NOMEARQUIVO);
	                swal({
	                    title: "Excluído!",
	                    text: "A produto foi excluída.",
	                    confirmButtonColor: "#66BB6A",
	                    type: "success"
	                });
	            } else {
	                swal({
	                    title: "Cancelado",
	                    text: "Cancelado com sucesso",
	                    confirmButtonColor: "#2196F3",
	                    type: "error"
	                });
	            }
	        });
		}
		function excluirProduto(CODIGOPRODUTO,NOMEARQUIVO){
			var elemento = document.getElementById("imagem_" + CODIGOPRODUTO);
			$.ajax({
			  url: "<?php echo base_url('produto')."/deleteProdutoTurma"; ?>",
			  type: "POST",
			  data: {
			  	CODIGO_PRODUTO: CODIGOPRODUTO,
			  	NOME_ARQUIVO: NOMEARQUIVO,
			  	},
			  dataType:"json",
			  success: function(data) {
				if(data == '2'){
					$.jGrowl('Erro ao excluir produto', {
						theme: 'bg-danger'
						});
				} else {
					elemento.parentElement.removeChild(elemento);
				}
			  },
			  error: function(jqXHR, textStatus, errorThrown){  }
			});
		}
		
		function atualizaProduto(CODIGOPRODUTO){
			var DESCRI;
			var VALOR;
			DESCRI = $('#descri_'+CODIGOPRODUTO).val();
			VALOR = $('#valor_'+CODIGOPRODUTO).val();
			VALOR = VALOR.replace(/\./g, '').replace(',', '.');
			if (!isNumber(VALOR)){
						$.jGrowl('Informe um valor válido!', {
							theme: 'bg-danger'
							});
						$('#valor_'+CODIGOPRODUTO).focus();
						
			} else {
				$.ajax({
				  url: "<?php echo base_url('produto')."/updateProduto"; ?>",
				  type: "POST",
				  data: {
				  	CODIGO_PRODUTO: CODIGOPRODUTO,
				  	DESCRI_PRODUTO: DESCRI,
				  	VALOR_PRODUTO: VALOR,
				  	},
				  dataType:"json",
				  success: function(data) {
				  	if(data == '1'){
		                swal({
		                    title: "Salvo!",
		                    text: "Descrição gravado com sucesso.",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
		                });
				  	} else {
						$.jGrowl('Erro ao atualizar produto', {
							theme: 'bg-danger'
							});
						}
				  },
				  error: function(jqXHR, textStatus, errorThrown){  }
				});
			}
		}
		function isNumber(n) {
		    'use strict';
		    n = n.replace(/\./g, '').replace(',', '.');
		    return !isNaN(parseFloat(n)) && isFinite(n);
		}
    </script>
</body>
</html>