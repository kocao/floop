<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header id="header" class="header-narrow header-transparent custom-header-transparent-style-1" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-logo">
						<a href="<?php echo base_url(); ?>"> <img alt="Floop" width="102" height="40" src="<?php echo assets_url(); ?>img/logo.png"> </a>
					</div>
				</div>
				<div class="header-column">
					<div class="header-row">
						<div class="header-nav">
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
								<i class="fa fa-bars"></i>
							</button>
							<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
								<nav>
									<ul class="nav nav-pills custom-nav-pills" id="mainNav">
										<li class='centered-item'>
											<a class="font-weight-semibold text-color-light" <?php
											if ($prefix_menu == '') {echo 'data-hash ';
											}
										?>href="<?php echo base_url(); ?>#home"> HOME </a>
										</li>
										<li class='centered-item'>
										<a class="font-weight-semibold text-color-light" <?php
											if ($prefix_menu == '') {echo 'data-hash data-hash-offset="80"';
											}
										?>href="<?php echo base_url(); ?>#plans">
										ANUNCIE	NO FLOOP
										</a>
										</li>
										<li class="dropdown centered-item">
											<a class="dropdown-toggle font-weight-semibold text-color-light" href="#"> SOBRE <i class="fa fa-caret-down"></i></a>
											<ul class="dropdown-menu">
												<li >
													<a href="ofloop" class="font-weight-semibold text-color-dark">
														O Floop
													</a>
												</li>
												<li>
													<a href="termosdeuso" class="font-weight-semibold text-color-dark">
														Termos de Uso
													</a>
												</li>
												<li>
													<a href="politicadeprivacidade" class="font-weight-semibold text-color-dark">
														Política de Privacidade
													</a>
												</li>
											</ul>
										</li>
										<li class='centered-item'>
										<a class="font-weight-semibold text-color-light" <?php
											if ($prefix_menu == '') {echo 'data-hash data-hash-offset="80"';
											}
										?>href="<?php echo base_url(); ?>#contato">
										FALE CONOSCO
										</a>
										</li>
										<?php if( $this -> libfloop -> user_logged_in() ) {
										?>
										<li class='centered-item'>
										<a class="font-weight-semibold text-color-light" href="<?php echo base_url('dashboard'); ?>">
										Área do Anunciante
										</a>
										</li>
										<?php } ?>

										<li class='centered-item'>
										<a class="font-weight-semibold text-color-light" href="<?php
											if ($this -> libfloop -> user_logged_in()) {echo base_url('login/logoff');
											} else {echo base_url('login');
											}
 ?>">
										<?php
										if ($this -> libfloop -> user_logged_in()) {
											echo '<i class="fa fa-sign-out small"></i>logoff';
										} else {
											echo '<i class="fa fa-user"></i>login/cadastro';
                                 echo '<br><span class="small">* Apenas para anunciantes</span>';
										}
										?><br>
										</a>
                              <br>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
