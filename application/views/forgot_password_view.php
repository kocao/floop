<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

				<section id="registro" class="background-color-light pt-xlg">
					<div class="container mt-xlg pt-xlg pb-xlg">
						<div class="row">
							<div class="col-md-12">
                        <?php
                        echo validation_errors('<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>','</p></strong></div>');
                        $this->load->view('flashdata_view');
                        ?>

								<div class="featured-boxes">
									<div class="row">
										<div class="col-sm-4">
										</div>
										<div class="col-sm-4">
											<div class="featured-box featured-box-primary align-left mt-xlg" style="height: 227px;">
												<div class="box-content">
													<h4 class="heading-primary text-uppercase mb-md">Alterar senha</h4>
													<?php echo form_open('login/password_recovery') ?>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>E-mail</label>
																	<input type="text" value="" name = "email" class="form-control input-lg">
																</div>
															</div>
														</div>

														<div class="row">

															<div class="col-md-6">
																<span class="remember-box checkbox">
																	<p>&nbsp;</p>
																</span>
															</div>

															<div class="col-md-6">
																<input type="submit" value="Concluir" class="btn btn-primary pull-right mb-xl" data-loading-text="Carregando...">
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="col-sm-4">
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</section>
