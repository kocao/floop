<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="sticky-header-active">
	<?php $this->load->view('head_view');	?>
	<body data-target="#header" data-spy="scroll" data-offset="100">
			<?php $this->load->view('header_view');	?>
			<div role="main" class="main">
				<main role="main">
				<?php

				$this->load->view($file_view);
				?>
				</main>
			</div>
			<?php $this->load->view('footer_view');	?>

		<?php $this->load->view('bottom_view');	?>
	</body>
   <div class="lightbox-target col-md-12 text-center hidden" id="passo1">
      <img src="<?php echo assets_url(); ?>img/passo01red.png" class="img-passo-zoom"/>
      <a class="lightbox-close" href="#"></a>
   </div>
   <div class="lightbox-target col-md-12 text-center hidden" id="passo2">
      <img src="<?php echo assets_url(); ?>img/passo02red.png" class="img-passo-zoom"/>
      <a class="lightbox-close" href="#"></a>
   </div>
   <div class="lightbox-target col-md-12 text-center hidden" id="passo3">
      <img src="<?php echo assets_url(); ?>img/passo03red.png" class="img-passo-zoom"/>
      <a class="lightbox-close" href="#"></a>
   </div>
</html>
<script>
$('.lightbox-target').on('click', function(e) {
  if (e.target !== this)
    return;
  
  $('.mfp-close').click();
});
</script>