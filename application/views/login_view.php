<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

				<section id="registro" class="background-color-light pt-xlg">
					<div class="container mt-xlg pt-xlg pb-xlg">
						<?php
							echo validation_errors('<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>','</p></strong></div>');
							$this->load->view('flashdata_view');
						?>
						<div class="row">
							<div class="col-md-12">

								<div class="featured-boxes">
									<div class="row">
										<div class="col-sm-6">
											<div class="featured-box featured-box-primary align-left mt-xlg" style="height: 327px;">
												<div class="box-content">
													<h4 class="heading-primary text-uppercase mb-md">Já tenho uma conta</h4>
													<?php echo form_open('login/login_user') ?>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>E-mail</label>
																	<input type="text" value="" name = "email" class="form-control input-lg">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Senha</label>
																	<input type="password" value="" name = "password" class="form-control input-lg">
																</div>
                                                <a class="pull-right" href="<?php echo base_url('login/forgot_password');?>">(Esqueceu a senha?)</a>
															</div>
														</div>
														<div class="row">

															<div class="col-md-6">
																<span class="remember-box checkbox">
																	<p>&nbsp;</p>
																</span>
															</div>

															<div class="col-md-6">
																<input type="submit" value="Entrar" class="btn btn-primary pull-right mb-xl" data-loading-text="Carregando...">
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="featured-box featured-box-primary align-left mt-xlg" style="height: 327px;">
												<div class="box-content">
													<h4 class="heading-primary text-uppercase mb-md">Nova Conta</h4>
													<?php echo form_open('login/create_user'); ?>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Nome</label>
																	<input type="text" name = "name" value="" class="form-control input-lg">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>E-mail</label>
																	<input type="text" name = "email" value="" class="form-control input-lg">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-6">
																	<label>Senha</label>
																	<input type="password" name = "password" value="" class="form-control input-lg">
																</div>
																<div class="col-md-6">
																	<label>Redigite a senha</label>
																	<input type="password" name = "password1" value="" class="form-control input-lg">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<input type="submit" value="Registrar" class="btn btn-primary pull-right mb-xl" data-loading-text="Carregando...">
															</div>
														</div>
													<?php echo form_close(); ?>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</section>
