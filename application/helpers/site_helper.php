<?php  if ( ! defined('BASEPATH')) exit('Acesso negado.');
define('DEFAULT_USER_IMAGES_PATH', 'public'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'imgusers'.DIRECTORY_SEPARATOR);
define('DEFAULT_USER_IMAGES_URL', 'public/assets/imgusers/');

if ( ! function_exists('assets_url()')){
		function assets_url(){
			return base_url().'public/assets/';
		}
}
if ( ! function_exists('dash_assets_url()')){
		function dash_assets_url(){
			//return base_url().'public/dash_assets/';
			return base_url().'public/assets/';
		}
}

if ( ! function_exists('theme_url()')){
		function theme_url(){
			return base_url().'themes/';
		}
}

if ( ! function_exists('getGUID()')){
		function getGUID($charid){

		        //mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
		        if (empty($charid)){
		        	$charid = strtoupper(md5(uniqid(rand(), true)));
				}
		        $hyphen = chr(45);// "-"
		        $uuid = ''//chr(123)// "{"
		            .substr($charid, 0, 8).$hyphen
		            .substr($charid, 8, 4).$hyphen
		            .substr($charid,12, 4).$hyphen
		            .substr($charid,16, 4).$hyphen
		            .substr($charid,20,12);
		            //.chr(125);// "}"
		        return $uuid;

		}
}
if ( ! function_exists('get_prefix_img_file()')){
	function get_prefix_img_file($idimgtype) {
		switch ($idimgtype) {
			case 1 :
				$prefix_file_name = 'IMGANUNCIO';
				break;
			case 2 :
				$prefix_file_name = 'IMGCARDAPIO';
				break;
			case 3 :
				$prefix_file_name = 'IMGPROMOCAO';
				break;
			case 4 :
				$prefix_file_name = 'IMGEVENTO';
				break;
			default :
				$prefix_file_name = 'IMAGEM';
				break;
		}
		return $prefix_file_name;
	}
}

if ( ! function_exists('get_title_img_file()')){
	function get_title_img_file($idimgtype) {
		switch ($idimgtype) {
			case 1 :
				$title_name = ' - Principal';
				break;
			case 2 :
				$title_name = ' - Cardápio';
				break;
			case 3 :
				$title_name = ' - Promoção';
				break;
			case 4 :
				$title_name = ' - Evento';
				break;
			default :
				$title_name = '';
				break;
		}
		return $title_name;
	}
}

function forminput($labelname='',$labelid='',$attrlabel='',$attrinput='',$setvalueinput='',$inputautofocus='') {
	$strreturn = '<div class="form-group">';
	if (isset($attrlabel)) {
		$strreturn .= form_label($labelname, $labelid, $attrlabel);

	}
	$strreturn .= '<div class="col-lg-10">';
	$strreturn .= form_input($attrinput, set_value($setvalueinput), $inputautofocus);
	$strreturn .= '</div></div>';
	return $strreturn;
}

function formpassword($labelname='',$labelid='',$attrlabel='',$attrinput='',$setvalueinput='',$inputautofocus='') {
	$strreturn = '<div class="form-group">';
	if (isset($attrlabel)) {
		$strreturn .= form_label($labelname, $labelid, $attrlabel);
	}
	$strreturn .= '<div class="col-lg-10">';
	$strreturn .= form_password($attrinput, set_value($setvalueinput), $inputautofocus);
	$strreturn .= '</div></div>';
	return $strreturn;
}

function formcheckbox($attrcheck='',$titlecheck='') {
	$strreturn = '<div class="form-group">';
	$strreturn .= '<div class="checkbox">';
	$strreturn .= '<label>';
	$strreturn .= form_checkbox($attrcheck) . $titlecheck;
	$strreturn .= '</label>';
	$strreturn .= '</div></div>';
	return $strreturn;
}

function formtextarea($labelname='',$labelid='',$attrlabel='',$attrinput='',$setvalueinput='',$inputautofocus='') {
	$strreturn = '<div class="form-group">';
	if (isset($attrlabel)) {
		$strreturn .= form_label($labelname, $labelid, $attrlabel);
	}
	$strreturn .= '<div class="col-lg-10">';
	$strreturn .= form_textarea($attrinput, set_value('nameMensagem'), $inputautofocus);
	$strreturn .= '</div></div>';
	return $strreturn;
}

function validationerrors(){
	return validation_errors('<div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">x</button><strong><p>','</p></strong></div>');
}

function openview(){
	$strreturn = '<div class="col-lg-12">';
	$strreturn .= '<div class="well">';
	return $strreturn;
}

function closeview(){
	$strreturn = '</div></div>';
	return $strreturn;
}
function openformbutton(){
	$strreturn = '<div class="form-group">';
	$strreturn .= '<div class="col-lg-10 col-lg-offset-2">';
	return $strreturn;
}

function closeformbutton(){
	$strreturn = '</div></div>';
	return $strreturn;
}

function nbsp($qtdnb=1){
	$strreturn = '<div>';
	$strreturn .=  str_repeat('&nbsp;', $qtdnb);
	$strreturn .= '</div>' . "\n";
	return $strreturn;
}

	function lang($line, $id = '',$prefix='',$sufix='')
	{
		$CI =& get_instance();

		$line = $prefix . $CI->lang->line($line) . $sufix;

		if ($id != '')
		{
			$line = '<label for="'.$id.'">'.$line."</label>";
		}

		return $line;
	}




	function opentag($tagname,$attributes='',$comments=''){
		$strtag = "\n";
    	$strtag .= '<'.$tagname;
		if ($attributes) $strtag .= ' ' . $attributes;
    	$strtag .= '>' . "\n";
    	if ($comments) $strtag .= '<!--' . $comments . '-->' . "\n";
		return $strtag;
	}

	function closetag($tagname,$comments=''){
		$strtag = "\n";
    	$strtag .= '</'.$tagname.'>';
		if ($comments){
			$strtag .= "\n";
			$strtag .= '<!-- ----------------' . $comments . '---------------- -->';
		}
    	$strtag .= "\n";
		return $strtag;
	}




    function getusertheme(){
		$CI =& get_instance();
		$CI->load->library('session');
		if (!$CI->session->userdata('usertheme')){
			$CI->session->set_userdata('usertheme', DEFAULT_SITE_THEME);
		}
		//return $CI->session->userdata('usertheme');
		return 'new01';//
    }

	function getsitecss(){
		$CI =& get_instance();
		$tema = getusertheme();
		$retornostr = "\n";
		$retornostr .= '<!-- CSS -->'  . "\n";

		if($CI->css_theme == ''){
			$linkcss = array(
		          'href' => theme_url() . $tema . '/bootstrap.css',
		          'rel' => 'stylesheet',
		          'type' => 'text/css',
		          'media' => 'screen'
			);

			$retornostr .= link_tag($linkcss) . "\n";
			$retornostr .= link_tag(asset_url() . '/css/bootswatch.min.css') . "\n";
			$retornostr .= link_tag(asset_url() . '/css/stylesheet.css') . "\n";
			$retornostr .= link_tag(asset_url() . '/css/slider.css') . "\n";
			$retornostr .= link_tag(asset_url() . '/css/font-awesome.min.css') . "\n";
		} else {

			$retornostr .= $CI->css_theme;

		}

		$retornostr .= '<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->' . "\n";
		$retornostr .= '<!--[if lt IE 9]>' . "\n";

		$retornostr .= '	<script src="'. asset_url() . '/bower_components/html5shiv/dist/html5shiv.js"></script>' . "\n";
		$retornostr .= '	<script src="'. asset_url() . '/bower_components/html5shiv/dist/respond.min.js"></script>' . "\n";
		$retornostr .= '<![endif]-->' . "\n";

		$retornostr .= '<!-- CSS END -->' . "\n";

		return $retornostr;

	}

	function getsitejs(){
		$CI =& get_instance();
		$retornostr = "\n";

		$tema = getusertheme();


		if($CI->js_bottom_scripts == ''){
			$retornostr .= '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>' . "\n";
	    	$retornostr .= '<script src="' . asset_url() . '/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>' . "\n";
	    	$retornostr .= '<script src="' . asset_url() . '/js/bootswatch.js"></script>' . "\n";

			$retornostr .= '<script src="' . asset_url() . '/js/jquery.fancybox.pack.js"></script>' . "\n";
			$retornostr .= '<script src="' . asset_url() . '/js/jquery.fancybox-media.js"></script>' . "\n";
			$retornostr .= '<script src="' . asset_url() . '/js/jquery.flexslider.js"></script>' . "\n";
			$retornostr .= '<script src="' . asset_url() . '/js/custom.js"></script>' . "\n";


		} else {
			$retornostr .= $CI->js_bottom_scripts;
		}
		return $retornostr;
	}

   if ( ! function_exists('time_select()')){
   		function time_select($hname, $mname, $selected = "", $class = "", $id = ""){
            $hselected = explode(":", $selected)[0];
            $mselected = explode(":", $selected)[1];
            $retorno = "<select class='$class' name='$hname' id='$id'>";
            $retorno = $retorno."<option value=''></option>";

            for ($i=0; $i < 24 ; $i++) {
               $hora = str_pad($i, 2, "0", STR_PAD_LEFT);
               if ($hora == $hselected){
                  $retorno = $retorno."<option value='$hora' selected>$hora</option>";
               } else {
                  $retorno = $retorno."<option value='$hora'>$hora</option>";
               }
            }

            $retorno = $retorno."</select><span>:</span>";
            $retorno = $retorno."<select name='$mname'>";
            $retorno = $retorno."<option value=''></option>";
            for ($i=0; $i < 60; $i++) {
               $minuto = str_pad($i, 2, "0", STR_PAD_LEFT);
               if ($minuto == $mselected){
                  $retorno = $retorno."<option value='$minuto' selected>$minuto</option>";
               } else {
                  $retorno = $retorno."<option value='$minuto'>$minuto</option>";
               }
            }
            $retorno = $retorno."</select>";
            return $retorno;
         }
   }


   if ( ! function_exists('required_message()')){
      function required_message(){
         return "oninvalid='this.setCustomValidity(\"Por favor preencha esse campo\")' oninput='setCustomValidity(\"\")'";
      }
   }

   if ( ! function_exists('url_message()')){
      function url_message(){
         return "oninvalid='this.setCustomValidity(\"Digite uma URL válida.\")' oninput='setCustomValidity(\"\")'";
      }
   }
   if ( ! function_exists('gerar_hash()')){
      function gerar_hash(){
         $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
         $hash = "";
         for ($i=0; $i < 100 ; $i++) {
            $position = rand(0,71);
            $letter = $seed[$position];
            $hash = $hash.$letter;
         }

         return $hash;
      }
   }

   if ( ! function_exists('date_view()')){
      function date_view($data){
         if(empty($data)){
            return '';
         }
         $data_return = strftime("%d/%m/%Y", strtotime($data));
         return $data_return;
      }
   }
   if ( ! function_exists('date_db()')){
      function date_db($data){
         if(empty($data)){
            return '';
         }

         // strftime("%Y-%m-%d", strtotime(str_replace('/', '-', $data)));
         $data_return = strftime("%Y-%m-%d", strtotime(str_replace('/', '-', $data)));
         return $data_return;
      }
   }
