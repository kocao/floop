<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anuncios_model extends CI_Model {

	function __construct() {
     parent::__construct();
   }

   function insert_or_update($data_anuncio){
      if (count($this->db->where('id_user', $data_anuncio['id_user'])->get('anuncios')->result_array()) >= 1) {
         unset($data_anuncio['id_statusanuncio']);
         if($this -> db -> update("anuncios", $data_anuncio, "id_user = ".$data_anuncio['id_user'])){
            return true;
         }
      } else {
         if($this -> db -> insert("anuncios", $data_anuncio, "id_user = ".$data_anuncio['id_user'])){
            return true;
         }
      }
      return false;
   }

   function get_anuncios($id_user){
      $query = "				select a.id_anuncio, a.id_user, s.descr_statusanuncio, p.descr_plano, f.id_finan, f.status_finan, f.ativoinativo_finan,
				u.nome_user
				from anuncios a
				inner join anuncios_status s on (s.id_statusanuncio = a.id_statusanuncio)
				inner join userdetail ud on (ud.id_user = a.id_user)
				inner join users u on (u.id_user = a.id_user)
            inner join planos p on (p.id_plano = ud.id_plano)
                left join financeiro f on (f.id_user = u.id_user)
				where u.id_user = $id_user
           ";
      $anuncios = $this->db->query($query)->result_array();
      return $anuncios;
   }


   function insert_eventos($post){
      $iduser = $this -> users_model -> get_id_user();
      $id_anuncio = $this->get_anuncios($iduser)[0]['id_anuncio'];
      $insert_dados = $this -> monta_eventos($post);
      $this->db->delete("eventos",['id_anuncio' => $id_anuncio]);
      if(!empty($insert_dados)){
         foreach ($insert_dados as $key => $dados_row) {
            $dados_row['id_anuncio'] = $id_anuncio;
            $this -> db -> insert("eventos", $dados_row);
         }
      }

   }

   function monta_eventos($post){
      $cont = count($post['hevento']);
      for ($i=0; $i < $cont ; $i++) {
         $horario_evento = "";

         if(empty($post['hevento'][$i]) || empty($post['mevento'][$i])){
            $horario_evento = '';
         } else {
            $horario_evento = $post['hevento'][$i].":".$post['mevento'][$i];
         }

         $dados[] = [
            'data' => $post['data_evento'][$i],
            'complemento' => $post['complemento'][$i],
            'descricao' => $post['descricao'][$i],
            'hora' => $horario_evento
         ];
      }
      return $dados;
   }

   function insert_horarios($post){
      $iduser = $this -> users_model -> get_id_user();
      $id_anuncio = $this->get_anuncios($iduser)[0]['id_anuncio'];
      $insert_dados = $this -> monta_horarios($post);
      $this->db->delete("horario_funcionamento",['id_anuncio' => $id_anuncio]);
      foreach ($insert_dados as $key => $dados_row) {
         $dados_row['id_anuncio'] = $id_anuncio;
         $this -> db -> insert("horario_funcionamento", $dados_row);
      }

   }

   function monta_horarios($post){
      $dados = [];

      for ($i=0; $i <7 ; $i++) {
         $horario_inicio = "";
         $horario_fim = "";
         if(empty($post['hinicio'][$i]) || empty($post['minicio'][$i])){
            $horario_inicio = '';
         } else {
            $horario_inicio = $post['hinicio'][$i].":".$post['minicio'][$i];
         }
         if(empty($post['hfim'][$i]) || empty($post['mfim'][$i])){
            $horario_fim = '';
         } else {
            $horario_fim = $post['hfim'][$i].":".$post['mfim'][$i];
         }
         $dados[] = [
            'dia' => $post['dia'][$i],
            'fechado' => $post['fechado'][$i],
            'horarioinicio' => $horario_inicio,
            'horariofim' => $horario_fim,
            'sequence' => $i,
            'id_anuncio' => $id_anuncio];

         }

         return $dados;
      }

   function get_horarios(){
      $iduser = $this -> users_model -> get_id_user();
      $id_anuncio = $this->get_anuncios($iduser)[0]['id_anuncio'];
      if(!empty($id_anuncio)){
         $query = "select * from horario_funcionamento where id_anuncio = $id_anuncio order by sequence";
         $horarios = $this -> db -> query($query) -> result_array();
      } else {
         return false;
      }
      return $horarios;

   }

   function get_eventos($id_user = ""){
      if(empty($id_user)){
         $id_user = $this-> users_model -> get_id_user();
      }

      $id_anuncio = $this->get_anuncios($id_user)[0]['id_anuncio'];
      if(!empty($id_anuncio)){
         $query = "select * from eventos where id_anuncio = $id_anuncio";
         $eventos = $this -> db -> query($query) -> result_array();
      } else {
         return false;
      }
      return $eventos;
   }

   function get_horarios_firebase($id_user = ""){
      if(empty($id_user)){
         $id_user = $this -> users_model -> get_id_user();
      }
      $id_anuncio = $this->get_anuncios($id_user)[0]['id_anuncio'];
      $query = "select dia, fechado, horariofim, horarioinicio from horario_funcionamento where id_anuncio = $id_anuncio order by sequence";
      $horarios = $this->db->query($query)->result_array();
      foreach ($horarios as $key => $horario) {
         $horarios[$key]['horarioInicio'] = $horario['horarioinicio'];
         unset($horarios[$key]['horarioinicio']);
         $horarios[$key]['horarioFim'] = $horario['horariofim'];
         unset($horarios[$key]['horariofim']);
         $horarios[$key]['fechado'] = ($horarios[$key]['fechado'] == "f" ? false : true);
      }

      return $horarios;
   }

   function atualiza_publicacao(){
      #TODO: Alterar status_finan = 3 quando for para produção
      $query = "
            select anuncios.*, financeiro.dtpgto_finan, periodicidade.qtdias_periodo, financeiro.id_plano, financeiro.id_periodo from anuncios
            inner join financeiro
            on anuncios.id_user = financeiro.id_user
            inner join userdetail
            on anuncios.id_user = userdetail.id_user
            inner join periodicidade
            on userdetail.id_periodo = periodicidade.id_periodo
            where anuncios.id_statusanuncio not in (3, 4)
            and financeiro.ativoinativo_finan = 'A'
            and financeiro.status_finan = '3'";

         $anuncios = $this -> db -> query($query)->result_array();

         foreach ($anuncios as $key => $anuncio) {
            $dt_pgto = $anuncio['dtpgto_finan'];
            $dias_periodo = $anuncio['qtdias_periodo'];
            // Quando for upgrade de plano
            if(!empty($anuncio['id_plano'] && !empty($anuncio['id_periodo']))){
               $id_perido_upgrade = $anuncio['id_periodo'];
               $dias_periodo = $this -> db -> query("select qtdias_periodo from periodicidade where id_periodo = $id_perido_upgrade") -> result_array()[0]['qtdias_periodo'];
               $dados_detail = [];
               $dados_detail['id_plano'] = $anuncio['id_plano'];
               $dados_detail['id_periodo'] = $anuncio['id_periodo'];
               $dados_detail['upgrade'] = true;
               $this -> db -> update('userdetail', $dados_detail, ['id_user' => $this -> users_model -> get_id_user()]);
            }
            $dt_vcto =  date('Y-m-d H:i', strtotime("+$dias_periodo days",strtotime($dt_pgto)));

            $dados_anuncio = [];
            $dados_anuncio['dt_vcto'] = $dt_vcto;
            $dados_anuncio['dt_pgto'] = $dt_pgto;
            $dados_anuncio['id_statusanuncio'] = 4;
            $this -> db -> update('anuncios', $dados_anuncio, ['id_anuncio' => $anuncio['id_anuncio']]);
         }
   }

   function anuncios_publicar(){
      $query = "select * from anuncios where id_statusanuncio = 4";
      $anuncios = $this -> db -> query($query)->result_array();
      return $anuncios;
   }

   function anuncios_desativado(){
      $id_anuncio = $this -> get_id_anuncio();
      $query = "select * from anuncios where id_statusanuncio = 6 and id_anuncio = $id_anuncio";
      if(empty($id_anuncio)){
         return false;
      }
      $anuncios = $this -> db -> query($query)->result_array();
      if(count($anuncios) > 0){
         return true;
      } else {
         return false;
      }
   }
   function anuncios_vencido(){
      $id_anuncio = $this -> get_id_anuncio();
      $query = "select * from anuncios where id_statusanuncio = 7 and id_anuncio = $id_anuncio";
      if(empty($id_anuncio)){
         return false;
      }
      $anuncios = $this -> db -> query($query)->result_array();
      if(count($anuncios) > 0){
         return true;
      } else {
         return false;
      }
   }


   function desativar_anuncio($id_anuncio, $force = false, $deletar = false){
      if(!$this -> anuncios_model -> anuncio_publicado($id_anuncio) && !$force){
         return false;
      }
      $id_user = $this -> db -> query("select id_user from anuncios where id_anuncio = $id_anuncio") -> result_array()[0]['id_user'];
      $key_firebase = $this -> db -> query("select key_firebase from userdetail where id_user = $id_user") -> result_array()[0]['key_firebase'];
      $firebase = new Firebasefloop();
      $storage = new Firebasestorage();

      $firebase -> init();
      $storage -> init();

      // Deleta todos os arquivos do firebase
      $all_files = $this -> users_model -> get_all_files($id_user);
      foreach ($all_files as $key => $file) {
         if(!empty($file['path_firebase'])){
            $storage->deleteFile($file['path_firebase']);
         }
      }

      // Deletando do firebase
      $firebase -> deleteKey($key_firebase);

      if($deletar) {
         // Deletando eventos
         $this -> db -> delete ('eventos', ['id_anuncio' => $id_anuncio]);

         // Deletando horários
         $this -> db -> delete ('horario_funcionamento', ['id_anuncio' => $id_anuncio]);

         // Deletando arquivos salvos
         $arquivos = $this -> users_model -> get_all_files();
         foreach ($arquivos as $key => $arquivo) {
            if (file_exists($arquivo['path_file'])) {
               unlink($arquivo['path_file']);
            }
         }

         // Deletando tabela de arquivos;
         $this -> db -> delete ('users_files', ['id_user' => $id_user]);
      }

      // Atualizando anuncios
      $this -> db -> update('anuncios', ['id_statusanuncio' => 6, 'dt_vcto' => null], ['id_anuncio' => $id_anuncio]);

      $this -> db -> update('financeiro',['ativoinativo_finan' => 'I'], ['id_user' => $id_user]);
   }

   function anuncio_publicado($id_anuncio = ""){
      if(empty($id_anuncio)){
         $id_user = $this -> users_model -> get_id_user();
         $id_anuncio = $this -> db -> query("select * from anuncios where id_user = $id_user") -> result_array()[0]['id_anuncio'];
      }
      $tz = 'America/Sao_Paulo';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $data_atual = $dt->format('Y-m-d');

      if (!empty($id_anuncio)){
         $anuncio = $this -> db -> query("select * from anuncios where id_anuncio = $id_anuncio")->result_array()[0];
      }
      // echo (!empty($anuncio));
      // echo ( $anuncio['id_statusanuncio'] == 3);
      // echo ($anuncio['dt_vcto'] < $data_atual);
      // exit(0);
      if(!empty($anuncio) && ($anuncio['id_statusanuncio'] == 3 || $anuncio['id_statusanuncio'] == 4 || $anuncio['id_statusanuncio'] == 5) && $anuncio['dt_vcto'] > $data_atual){
         return true;
      } else {
         return false;
      }
   }

   function get_anuncio($id_user = ""){
      if(empty($id_user)){
         $id_user = $this -> users_model -> get_id_user();
      }
      $anuncio = $this -> db -> query("select * from anuncios where id_user = $id_user") -> result_array()[0];
      return $anuncio;
   }

   function get_id_anuncio(){
      $id_anuncio = $this -> get_anuncio()['id_anuncio'];
      return $id_anuncio;
   }

   function valor_upgrade($id_plano, $id_periodo){
      $anuncio = $this -> anuncios_model -> get_anuncio();
      $plano_usuario = $this -> users_model -> get_row_user_plan();
      $valor = $plano_usuario -> valor;
      $qtdias = $plano_usuario -> qtdias_periodo;

      $query = "select * from periodo_plano
                  inner join periodicidade
                  on periodo_plano.id_periodo = periodicidade.id_periodo
                  where periodo_plano.id_plano = $id_plano
                  and periodo_plano.id_periodo = $id_periodo";

      $novo_plano = $this->db->query($query)->row();

      date_default_timezone_set('America/Sao_Paulo');
      $data_atual = date('Y-m-d');
      $dt_vcto =  $anuncio['dt_vcto'];

      $data_atual = new DateTime( $data_atual );
      $dt_vcto = new DateTime($dt_vcto);

      $intervalo = $data_atual->diff($dt_vcto);

      $dias_restantes = $intervalo -> m * 30 + $intervalo -> d;
      $vlr_credito = $valor / $qtdias * $dias_restantes;

      $meses_novo_plano = $novo_plano -> qtdias_periodo / 30;
      $valor_novo = $novo_plano -> valor * $meses_novo_plano;
      $valor_pagar = $valor_novo - $vlr_credito;

      return number_format(round($valor_pagar, 2), 2, '.','');
      // return round($valor_pagar, 2);
   }

   function anuncios_vencidos(){
      $result = $this -> db -> query("select * from anuncios where dt_vcto < now()");
      if($result){
         return $result -> result_array();
      } else {
         return $result;
      }
   }

   function desativar_vencidos(){
      $vencidos = $this -> anuncios_vencidos();
      if($vencidos){
         foreach ($vencidos as $key => $anuncio) {
            $this -> desativar_anuncio($anuncio['id_anuncio'], true);
					  $this -> db -> update('anuncios',['id_statusanuncio' => 7], ['id_anuncio' => $anuncio['id_anuncio']]);
         }
      }
   }


	 function redimensiona_logo($coordenadas){
		   $coord_array = [];
			$cords = $coordenadas;
		 	$logo = $this -> users_model -> get_logo()[0];
		   $path_origem = $logo['path_file'];
		   $url_origem = $logo['url_file'];
			$dir_origem = dirname($path_origem);
         $file_name = basename($path_origem);
		   $ext = strtolower(pathinfo($path_origem, PATHINFO_EXTENSION));
			$ext == 'jpg' ? $ext = 'jpeg' : '';

			// Criando um Array com as coordenadas
			$temp_array = explode(",",$cords);
			foreach ($temp_array as $key => $item) {
				$array_item = explode(":",$item);
				$coord_array[trim($array_item[0])] = trim($array_item[1]);
			}

			$w = $coord_array["W"];
			$h = $coord_array["H"];
			$x=$coord_array["X"];
			$x2=$coord_array["X2"];
			$y=$coord_array["Y"];
			$y2=$coord_array["Y2"];

			switch($ext){
			    case 'png':
						$img_origin = ImageCreateFrompng($path_origem);
						break;
			    case 'jpg':
						$img_origin = ImageCreateFromjpeg($path_origem);
				    break;
			    case 'jpeg':
						$img_origin = ImageCreateFromjpeg($path_origem);
				    break;
			    case 'gif':
					  $img_origin = ImageCreateFromgif($path_origem);
				    break;
			}

				// Recortando Imagem
				$img_resize	= imagecreatetruecolor( $w, $h );
				imagecopyresampled($img_resize, $img_origin, 0, 0, $x, $y, $w, $h, $w, $h);

		 		switch($ext){
				    case 'png':
		    			imagepng($img_resize, "$dir_origem/resize.png");
							break;
				    case 'jpg':
		 				  imagejpeg($img_resize, "$dir_origem/resize.jpg");
					    break;
				    case 'jpeg':
                  $ext = 'jpg';
		    			imagejpeg($img_resize, "$dir_origem/resize.jpg");
					    break;
				    case 'gif':
					    imagejpeg($img_resize, "$dir_origem/resize.gif");
					    break;
				}
            $this -> anuncios_model -> redimensiona_foto($dir_origem."/resize.".$ext, "resize.".$ext, 200, 200, $dir_origem, $ext);

				// Circulando Imagem
				imagedestroy($img_resize);
		 		switch($ext){
				    case 'png':
							$img_resize = ImageCreateFrompng("$dir_origem/resize.png");
							break;
				    case 'jpg':
							$img_resize = ImageCreateFromjpeg("$dir_origem/resize.jpg");
					    break;
				    case 'jpeg':
							$img_resize = ImageCreateFromjpeg("$dir_origem/resize.jpg");
					    break;
				    case 'gif':
						  $img_resize = ImageCreateFromgif("$dir_origem/resize.gif");
					    break;
				}
            $w	= imagesx($img_resize); //largura
            $h	= imagesy($img_resize); //altura

				$img_circle = imagecreatetruecolor($w, $h);
				$bg = imagecolorallocate($img_circle, 255, 255, 255);
				// $bg = imagecolorallocate($img_circle, 0, 0, 0);
				imagefill($img_circle, 0, 0, $bg);
				$e = imagecolorallocate($img_circle, 0, 0, 0);
				$r = $w <= $h ? $w : $h;
				imagefilledellipse ($img_circle, ($w/2), ($h/2), $r, $r, $e);
				imagecolortransparent($img_circle, $e);
				imagecopymerge($img_resize, $img_circle, 0, 0, 0, 0, $w, $h, 100);
				imagecolortransparent($img_resize, $bg);
            $ext = "png";
		 		switch($ext){
				    case 'png':
		    			imagepng($img_resize, "$dir_origem/pin.png");
							break;
				    case 'jpg':
		 				  imagejpeg($img_resize, "$dir_origem/pin.jpeg");
							$ext = "jpeg";
					    break;
				    case 'jpeg':
		    			imagejpeg($img_resize, "$dir_origem/pin.jpeg");
					    break;
				    case 'gif':
					    imagegif($img_resize, "$dir_origem/pin.gif");
					    break;
				}
		      copy("$dir_origem/pin.$ext", $path_origem."resized.".$ext);
            // $this -> anuncios_model -> redimensiona_foto($path_origem."resized.".$ext, $file_name."resized.".$ext, 200, 200, $dir_origem, $ext);
		 		$this -> db -> update ('users_files', ['path_file' => $path_origem."resized.".$ext, 'url_file' => $url_origem."resized.".$ext], ['id_file' => $logo["id_file"]]);


				// Deletando referências para as imagens
				imagedestroy($img_resize); // kill mask first
				imagedestroy($img_circle); // kill canvas last
				imagedestroy($img_origin); // kill canvas last
		      // unlink($dir_origem/pin.$ext);

		    // Atualizando no banco o logo como recortado
		    $this -> db -> update ('users_files', ['recortado' => true], ['id_file' => $logo['id_file']]);
	 }




    function redimensiona_foto($tmp, $arquivo, $max_x, $max_y, $pasta){
       $ext = strtolower(pathinfo($tmp, PATHINFO_EXTENSION));

       //$max_x = 800 $max_y = 630
       switch($ext){
           case 'png':
              $img = imagecreatefrompng($tmp);
              break;
           case 'jpg':
              $img = imagecreatefromjpeg($tmp);
              break;
           case 'jpeg':
              $img = imagecreatefromjpeg($tmp);
              break;
           case 'gif':
              $img = imagecreatefromgif($tmp);
              break;
       }

      $original_x	= imagesx($img); //largura
      $original_y	= imagesy($img); //altura
      $diretorio	= $pasta."/".$arquivo;
      // verifica se a largura ou altura da imagem é maior que o valor
      // máximo permitido
      if ( ( $original_x > $max_x ) || ( $original_y > $max_y ) ){
   	// verifica o que é maior na imagem, largura ou altura?
           if ( $original_x > $original_y ) {
   		$max_y	= ( $max_x * $original_y ) / $original_x;
   	}else{
   		$max_x	= ( $max_y * $original_x ) / $original_y;
   	}

   	$nova = imagecreatetruecolor($max_x, $max_y);
      $bg = imagecolorallocate ( $nova, 255, 255, 255 );
      imagefilledrectangle($nova,0,0,$max_x,$max_y,$bg);
   	imagecopyresampled($nova, $img, 0, 0, 0, 0, $max_x, $max_y, $original_x, $original_y);

      switch($ext){
          case 'png':
             imagepng($nova, $diretorio);
             break;
          case 'jpg':
             imagejpeg($nova, $diretorio);
             break;
          case 'jpeg':
             imagejpeg($nova, $diretorio);
             break;
          case 'gif':
             imagegif($nova, $diretorio);
             break;
      }

   	// imagejpeg($nova, $diretorio);
   	imagedestroy($nova);
   	imagedestroy($img);
      // se for menor, nenhuma alteração é feita
      }else{
         switch($ext){
             case 'png':
                imagepng($nova, $diretorio);
                break;
             case 'jpg':
                imagejpeg($nova, $diretorio);
                break;
             case 'jpeg':
                imagejpeg($nova, $diretorio);
                break;
             case 'gif':
                imagegif($nova, $diretorio);
                break;
         }
   	imagedestroy($img);
      }
      return($arquivo);
   }

   function getAllPlaces(){
      $places = $this -> db -> query("select place_id from userdetail ud
         inner join anuncios a
         on ud.id_user = a.id_user
         where id_statusanuncio in(3,4,5)
         and dt_vcto >= now()") -> result_array();
      return $places;
   }

   function removeBlackList(){
      $firebasefloopblack = new Firebasefloop();
      $firebasefloopblack -> init("blacklist");

      $places_firebase = $firebasefloopblack -> getAll();
      $places_bd = $this -> anuncios_model -> getAllPlaces();
      // var_dump($places_firebase);
      // var_dump($places_bd);
      // exit(0);
      foreach ($places_firebase as $key => $place_firebase) {
         $ativo = false;
         foreach ($places_bd as $key => $place_bd) {
            if($place_firebase == $place_bd['place_id'] || $place_firebase == "undefined"){
               $ativo = true;
               break;
            }
         }
         if($ativo == false && !empty($place_firebase)){
            $firebasefloopblack -> removeBlackList($place_firebase);
         }
      }
   }
}
