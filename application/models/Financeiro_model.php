<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model {

	function __construct() {
     parent::__construct();
   }

   function pagamento_pendente($id_user = ''){
      if(empty($id_user)){
         $id_user = $this -> users_model -> get_id_user();
      }
      #TODO: alterar para status_finan 3 quando for para produção
      $query = "SELECT * FROM financeiro WHERE  ativoinativo_finan = 'A' AND status_finan <> '3' AND id_user = $id_user";
      $result = $this -> db -> query($query);
      if ($result && !$this -> anuncios_model -> anuncio_publicado() && $result -> num_rows() > 0){
         return true;
      } else {
         return false;
      }

   }
   function pagamento_ativo($id_user = ''){
      if(empty($id_user)){
         $id_user = $this -> users_model -> get_id_user();
      }
      $query = "SELECT * FROM financeiro WHERE  ativoinativo_finan = 'A' AND id_user = $id_user";
      $result = $this -> db -> query($query) -> result_array();
      $pagamento = $result[0];
      if ($result){
         return $pagamento;
      }
   }

   function atualiza_pendentes(){
      #TODO: alterar para status_fina para 3 quando passar para produção
      $query_financeiro = "select * from financeiro where ativoinativo_finan = 'A' AND status_finan <> '3';";

      // Pegando financeiros que estão pendentes e ativos
      $finan_pend = $this -> db -> query($query_financeiro) -> result_array();
      // var_dump($finan_pend);
      // exit(0);

     foreach ($finan_pend as $key => $financeiro) {
        $pagseguro = new Pagseguro();
        $pagseguro->init();
      //   var_dump($financeiro)
         $xml = $pagseguro -> getStatusByRef("ref".$financeiro['id_finan'], $financeiro['dtrequisicao']);
            #TODO: alterar para status_fina para 3 quando passar para produção
            // var_dump(end($array_transaction));
            $last_item = "";
            // echo ($xml -> transactions -> transaction);
            $xmlstring = simplexml_load_string($xml -> asXML());
            $json = json_encode($xmlstring);
            $array = json_decode($json,TRUE);
            $transactions = $array['transactions']['transaction'];
            if(is_array($transactions[0])){
               $last_item = end($transactions);
            } else {
               $last_item = $transactions;
            }

            #TODO: alterar para status_fina para 3 quando passar para produção
            if ($last_item['status'] == "3"){
               // Pegando data e hora atual
               $tz = 'America/Sao_Paulo';
               $timestamp = time();
               $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
               $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
               $data_atual = $dt->format('Y-m-d H:i');
               $this -> db -> update('financeiro', ['transaction_code' => $last_item['code'], 'status_finan' => $last_item['status'], 'dtpgto_finan' => $data_atual], ['id_user' => $financeiro['id_user'], 'ativoinativo_finan' => 'A']);
            }
         }
   }


   function descr_status($status){
      switch ($status) {
         case 1:
            return "Aguardando pagamento";
            break;
         case 2:
            return "Em análise";
            break;
         case 3:
            return "Paga";
            break;
         case 4:
            return "Disponível";
            break;
         case 5:
            return "Em disputa";
            break;
         case 6:
            return "Devolvida";
            break;
         case 7:
            return "Cancelada";
            break;
         default:
            return "Status desconhecido";
            break;
      }
   }

}
