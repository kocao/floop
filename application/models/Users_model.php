<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	var $details;
	function __construct()
    {
        $this->has_one['user_detail'] = array('Userdetail_model','id_user','id_user'); // so... we have a table, the foreign key and the local key
        parent::__construct();
    }

	function validate_user($email, $password) {
		$query = $this -> db -> query("SELECT u.*, t.descr_usertype FROM users u, userstype t WHERE u.email_user = '$email' and t.id_usertype = u.id_usertype and u.ativo_inativo = 1 and u.confirmado = true");
		$row = $query -> row();
		if (isset($row)) {
			$pwstest = $this -> encrypt -> decode($row -> pws_user);
			if ($pwstest == $password) {
				$this -> details = $row;
				$this -> set_session();
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

   function confirmacao_pendente($email){
      $query = $this -> db -> query("SELECT u.*, t.descr_usertype FROM users u, userstype t WHERE u.email_user = '$email' and t.id_usertype = u.id_usertype and u.ativo_inativo = 1 and confirmado = false");
      $row = $query -> row();
      if($row){
         return true;
      } else {
         return false;
      }
   }

	function set_session() {
		$lsession = [];
		$lsession['keyuser'] = $this -> details -> key_user;
		$lsession['nomeusr'] = $this -> details -> nome_user;
		$lsession['emailusr'] = $this -> details -> email_user;
		$lsession['isLoggedIn'] = true;
		$lsession['ttyusrdata'] = $this -> encrypt -> encode($this -> details -> descr_usertype);
		$this -> session -> set_userdata($lsession);
	}

	function logout_user() {
		$user_data = $this -> session -> all_userdata();
		foreach ($user_data as $key => $value) {
			// if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this -> session -> unset_userdata($key);
			// }
		}
      session_destroy();
		// $this -> session -> sess_destroy();
	}

	function create_user($userData) {
		return $this -> db -> insert('users', $userData);
	}

	function exist_user($email) {
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('email_user', $email);
		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_pws_user_by_email($email) {
		$this -> db -> select('pws_user');
		$this -> db -> from('users');
		$this -> db -> where('email_user', $email);
		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			return $this -> encrypt -> decode($query -> row_array()['pws_user']);
		} else {
			return FALSE;
		}
	}

	function get_id_user() {
		$this -> db -> select('id_user');
		$this -> db -> from('users');
		$this -> db -> where('key_user', $this -> session -> keyuser);
		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			return $query -> row_array()['id_user'];
		} else {
			return 0;
		}
	}

	function user_isadmin() {
		/*
		 $this -> db -> select('id_user');
		 $this -> db -> from('users');
		 $this -> db -> where('key_user', $this -> session -> keyuser);
		 $this -> db -> where('id_usertype', 1);
		 $query = $this -> db -> get();
		 if ($query -> num_rows() > 0) {
		 return TRUE;
		 } else {
		 return FALSE;
		 }
		 *
		 */
		$retorno = FALSE;

		if ($this -> libfloop -> user_logged_in() && strtoupper($this -> encrypt -> decode($this -> session -> userdata('ttyusrdata'))) == 'ADMINISTRADOR') {
			$retorno = TRUE;
		}
		return $retorno;
	}

	function get_user_data($id_user = '') {
      if(empty($id_user)){
        $id_user = $this -> get_id_user();
      }
		$query = $this -> db -> query("select u.*,ud.* from users u left join userdetail ud on ud.id_user = u.id_user where u.id_user = $id_user;");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function get_other_user_data($iduser) {
		$query = $this -> db -> query("select u.*,ud.* from users u left join userdetail ud on ud.id_user = u.id_user where u.id_user = '$iduser';");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function create_update_user_details($userdatadetail, $userdata) {
		$iduser = $this -> get_id_user();

		$this -> db -> update("users", $userdata, "id_user = $iduser");
		if (!$this -> create_user_detail($iduser, $userdatadetail)) {
			return $this -> db -> update("userdetail", $userdatadetail, "id_user = $iduser");
		} else {
			return TRUE;
		}
	}

	function create_user_detail($iduser, $userdatadetail) {
		if (isset($iduser)) {
			$this -> db -> select('id_user');
			$this -> db -> from('userdetail');
			$this -> db -> where('id_user', $iduser);
			$query = $this -> db -> get();
			if ($query -> num_rows() == 0) {
				$userdatadetail['id_user'] = $iduser;
				return $this -> db -> insert('userdetail', $userdatadetail);
			} else {
				return FALSE;
			}
		}
	}

	function get_rows_finan() {
		$query = $this -> db -> query("select f.*,u.nome_user from financeiro f left join users u on (u.id_user = f.id_user) where ativoinativo_finan = 'A'");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function get_field_user($fieldname) {
		$keyusr = $this -> session -> keyuser;
		$query = $this -> db -> query("select u.*,ud.* from users u left join userdetail ud on ud.id_user = u.id_user where u.key_user = '$keyusr';");
		if (isset($query)) {
			return $query -> row_array()[$fieldname];
		} else {
			return FALSE;
		}
	}

	function get_rows_cad_planos() {
		$query = $this -> db -> query("select p.* from planos p order by id_plano desc");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function get_planos_data($idplano) {
		$query = $this -> db -> query("select p.* from planos p where p.id_plano = $idplano;");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function get_planos_vlr($idplano) {
		if (empty($idplano)) {
			$id_plano = null;
		} else {
			$query = $this -> db -> query("select p.*, pp.url_pagto, pp.valor, p.descr_periodo from periodicidade p inner join periodo_plano pp on (pp.id_periodo = p.id_periodo and pp.id_plano = $idplano)");
		}

		if (isset($query)) {
			return $query -> result_array();
		} else {
			return [];
		}
	}

	function create_upd_plano($plandata, $valores, $urlspag, $idplano = 0) {
		$query = $this -> db -> query("select 1 from planos p where p.id_plano = $idplano;");
		$row = $query -> row();
		if (isset($row)) {
			$this -> db -> update("planos", $plandata, "id_plano = $idplano");
		} else {
			if ($this -> db -> insert('planos', $plandata)) {
				$idplano = $this -> db -> insert_id();
			}
		}

      $this -> create_upd_vlrs_planos($idplano, $valores);
		// $this -> create_upd_urls_planos($idplano, $urlspag);

	}

	function create_upd_vlrs_planos($idplano, $valores) {
		foreach ($valores as $values) {
			foreach ($values as $key => $val) {
				if (is_numeric($val)) {
					$query = $this -> db -> query("select 1 from periodo_plano p where id_plano = $idplano and id_periodo = $key;");
					$row = $query -> row();
					$plandata = [];
					if (isset($row)) {
						$plandata['valor'] = $val;
						$this -> db -> update("periodo_plano", $plandata, "id_plano = $idplano and id_periodo = $key");
					} else {
						$plandata['id_plano'] = $idplano;
						$plandata['id_periodo'] = $key;
						$plandata['valor'] = $val;
						$this -> db -> insert('periodo_plano', $plandata);
					}
				}
			}
		}
	}

	function create_upd_urls_planos($idplano, $urlspag) {
		foreach ($urlspag as $values) {
			foreach ($values as $key => $val) {
				$plandata['url_pagto'] = $val;
				$this -> db -> update("periodo_plano", $plandata, "id_plano = $idplano and id_periodo = $key");

			}
		}
	}

	function get_vlrplan_by_idper($idplano, $idperiodo) {
		$query = $this -> db -> query("select p.valor from periodo_plano p where id_plano = $idplano and id_periodo = $idperiodo;");
		$row = $query -> row();
		if (isset($row)) {
			return $row -> valor;
		} else {
			return 0;
		}
	}

	function get_urlpagplan_by_idper($idplano, $idperiodo) {
		$query = $this -> db -> query("select p.url_pagto from periodo_plano p where id_plano = $idplano and id_periodo = $idperiodo;");
		$row = $query -> row();
		if (isset($row)) {
			return $row -> url_pagto;
		} else {
			return '#';
		}
	}

	function get_rows_periodos() {
		$query = $this -> db -> query("select p.* from periodicidade p");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function get_row_user_plan() {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select ud.id_plano,ud.id_periodo,p.descr_plano,pp.valor,pp.url_pagto,pe.descr_periodo, pe.qtdias_periodo, p.abrev_plano
										from userdetail ud
										inner join planos p on (p.id_plano = ud.id_plano)
										inner join periodicidade pe on (pe.id_periodo = ud.id_periodo)
										inner join periodo_plano pp on (pp.id_periodo = ud.id_periodo and pp.id_plano = ud.id_plano) where ud.id_user = $iduser;");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function get_plano_usuario() {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select ud.id_plano,ud.id_periodo,p.*
										from userdetail ud
										inner join planos p on (p.id_plano = ud.id_plano)
										where ud.id_user = $iduser;");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function update_user_plan($userdata) {
		$iduser = $this -> get_id_user();
		if ($this -> db -> update("userdetail", $userdata, "id_user = $iduser")) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_rows_users() {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select u.*,
										    ut.descr_usertype,
										    CASE u.ativo_inativo
										        WHEN 1 THEN 'Ativo'
										        ELSE 'Inativo'
										    END as status_str
										from users u
										left join userdetail ud on (ud.id_user = u.id_user)
										inner join userstype ut on (ut.id_usertype = u.id_usertype)
										 where u.id_user <> $iduser");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function get_rows_userstype() {
		$query = $this -> db -> query("select ut.id_usertype,ut.descr_usertype from userstype ut");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function update_user($userdata, $iduser) {
		if ($this -> db -> update("users", $userdata, "id_user = $iduser")) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_categorias_row() {
		$query = $this -> db -> query("select c.* from categorias c order by descr_categoria;");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function create_log($msgdatatxt) {
		$msgdata = ['logtxt' => $msgdatatxt, 'id_user' => $this -> get_id_user()];

		return $this -> db -> insert('log_sistema', $msgdata);
	}

	function get_image_rows($idimgtype) {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select i.* from images_users i where i.id_user = $iduser and i.id_tipoimg = $idimgtype order by i.id_img desc;");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function get_total_image_rows($idimgtype) {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select count(1) as total_img from images_users i where id_user = $iduser and id_tipoimg = $idimgtype;");
		$row = $query -> row();
		if (isset($row)) {
			return $row -> total_img;
		} else {
			return 0;
		}
	}

	function insert_img_users($imgdata) {
		return $this -> db -> insert('images_users', $imgdata);
	}

	function get_id_anuncio_usuario() {
		$iduser = $this -> get_id_user();
		$query = $this -> db -> query("select a.id_anuncio from anuncios a where id_user = $iduser;");
		$row = $query -> row();
		if (isset($row)) {
			return $row -> id_anuncio;
		} else {
			return 0;
		}
	}

	function create_upd_anuncio($dataanuncio, $idanuncio = 0) {
		$query = $this -> db -> query("select 1 from anuncios p where p.id_anuncio = $idanuncio;");
		$row = $query -> row();
		$retorno = FALSE;
		if (isset($row)) {
			if ($this -> db -> update("anuncios", $dataanuncio, "id_anuncio = $idanuncio")) {
				$retorno = TRUE;
			}
		} else {
			if ($this -> db -> insert('anuncios', $dataanuncio)) {
				//$idanuncio = $this -> db -> insert_id();
				$retorno = TRUE;
			}
		}
		return $retorno;
	}

	function get_anuncios_data($idanuncio) {
		$query = $this -> db -> query("select a.* from anuncios a where a.id_anuncio = $idanuncio;");
		$row = $query -> row();
		if (isset($row)) {
			return $row;
		} else {
			return FALSE;
		}
	}

	function upd_legenda_img($idimg, $imgdata) {
		if ($this -> db -> update("images_users", $imgdata, "id_img = $idimg")) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function delete_img($imgdata) {
		return $this -> db -> delete('images_users', $imgdata);
	}

	function get_fileimg_name($idimg) {
		$query = $this -> db -> query("select i.file_img from images_users i where id_img = $idimg;");
		$row = $query -> row();
		if (isset($row)) {
			return $row -> file_img;
		} else {
			return FALSE;
		}
	}

	function get_rows_anuncios_grid() {
		$query = $this -> db -> query("
				select a.*,s.descr_statusanuncio,p.descr_plano,
				u.nome_user
				from anuncios a
				inner join anuncios_status s on (s.id_statusanuncio = a.id_statusanuncio)
				inner join userdetail ud on (ud.id_user = a.id_user)
				inner join users u on (u.id_user = a.id_user)
				inner join planos p on (p.id_plano = ud.id_plano)
				order by a.id_statusanuncio
		");
		if (isset($query)) {
			return $query -> result_array();
		} else {
			return FALSE;
		}
	}

	function new($model, $userdatadetail) {
		$model -> nome_user = $userdatadetail['nome_user'];
		$model -> email_user = $userdatadetail['email_user'];
		$model -> cnpj_user = $userdatadetail['cnpj_user'] ;
		$model -> nomeempresa_user = $userdatadetail['nomeempresa_user'] ;
		$model -> enderemp_user = $userdatadetail['enderemp_user'] ;
		$model -> nroendemp_user = $userdatadetail['nroendemp_user'] ;
		$model -> cependemp_user = $userdatadetail['cependemp_user'] ;
		$model -> compendemp_user = $userdatadetail['compendemp_user'] ;
		$model -> bairroendemp_user = $userdatadetail['bairroendemp_user'] ;
		$model -> cidendemp_user = $userdatadetail['cidendemp_user'] ;
		$model -> ufendemp_user = $userdatadetail['ufendemp_user'] ;
		$model -> id_categoria = $userdatadetail['id_categoria'] ;
		$model -> fonecom_user = $userdatadetail['fonecom_user'] ;
		$model -> fonecel_user = $userdatadetail['fonecel_user'] ;
		$model -> siteemp_user = $userdatadetail['siteemp_user'] ;
		$model -> latitude_user = $userdatadetail['latitude_user'] ;
		$model -> longitude_user = $userdatadetail['longitude_user'];
		return $model;

	}

   function get_dados_pagseguro(){
      $iduser = $this->get_id_user();
      $query = "
      select p.id_plano as id,
      		'Floop Plano ' || p.descr_plano || ' (' || per.descr_periodo || ')' as produto,
      		pp1.valor,  per.qtdias_periodo,
              'ref' || u.id_user as referencia,
              u.nome_user as nome,
              ud.enderemp_user as endereco,
              ud.nroendemp_user as numero,
              ud.cidendemp_user as cidade,
              u.email_user as email
      from users u
      	inner join userdetail ud
          	on ud.id_user = u.id_user
      	inner join periodo_plano pp1
          	on pp1.id_plano = ud.id_plano
          	and pp1.id_periodo = ud.id_periodo
      	inner join planos p
      		on ud.id_plano = p.id_plano
      	inner join periodicidade per
      		on per.id_periodo = pp1.id_periodo
            where u.id_user = $iduser
      ";
      if($query){
         $result = $this -> db -> query($query)->result_array();
      }

      return $result;
   }

   function get_dados_firebase($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }

      $query = "
      select UPPER(SUBSTRING(p.descr_plano,1,1)) as cota, a.descr_anuncio as descricao,
         enderemp_user as endereco,
         a.lnkfacebook_user as facebook,
         a.lnkgmais_user as googlePlus,
         a.lnkinstagram_user as instagram,
         latitude_user as latitude,
         a.lnkcomprar_user as linkComprar,
         a.lnkreservamesa_user as linkReserva,
         longitude_user as longitude,
         nomeempresa_user as nome,
         ud.place_id,
         a.siteemp_user as site,
         a.lnktwitter_user as twitter,
         a.lnkyoutube_user as youtube,
         ud.fonecom_user ||'/' || ud.fonecel_user as telefone,
         c.descr_categoria_ing as tipo
      from users u
      	inner join userdetail ud
          	on ud.id_user = u.id_user
      	inner join periodo_plano pp1
          	on pp1.id_plano = ud.id_plano
          	and pp1.id_periodo = ud.id_periodo
      	inner join planos p
      		on ud.id_plano = p.id_plano
      	inner join periodicidade per
      		on per.id_periodo = pp1.id_periodo
         inner join anuncios a
          	on u.id_user = a.id_user
         inner join categorias c
            on ud.id_categoria = c.id_categoria
       where u.id_user = $id_user";

       $result = $this -> db -> query($query)->result_array();

       return $result;
   }

   function montar_dados_firebase($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }

      $dados = $this->get_dados_firebase($id_user)[0];

      $logo = $this->users_model->get_logo($id_user)[0];
      $horario_funcionamento = $this -> anuncios_model -> get_horarios_firebase($id_user);
      $dados["horarioFuncionamento"] = $horario_funcionamento;
      $dados["linkReserva"] = $dados["linkreserva"];
      $dados["linkComprar"] = $dados["linkcomprar"];
      $dados["googlePlus"] = $dados["googleplus"];
      unset($dados["linkreserva"]);
      unset($dados["linkcomprar"]);
      unset($dados["googleplus"]);

      //convertendo latitude longitude
      $dados['latitude'] = (double)$dados['latitude'];
      $dados['longitude'] = (double)$dados['longitude'];
      $dados['imageMarker'] = '';
      if ($logo) {
         $dados['imageMarker'] = $logo['url_firebase'];
      }

      $cardapios = $this->users_model->get_cardapios($id_user);
      if ($cardapios) {
         $images_cardapio = [];
         foreach ($cardapios as $key => $cardapio) {
            $images_cardapio[] = ["url" => $cardapio['url_firebase']];
         }
         $dados['imagesCardapio'] = $images_cardapio;
      }

      $locais = $this->users_model->get_locais($id_user);
      if ($locais) {
         $images_header = [];
         foreach ($locais as $key => $local) {
            $images_header[] = ["legenda" => (empty($local['legenda']) ? "" : $local['legenda']), "url" => $local['url_firebase']];
         }
         $dados['imagesHeader'] = $images_header;
      }

      $promocoes = $this->users_model->get_promocoes($id_user);
      if ($promocoes) {
         $images_promocoes = [];
         foreach ($promocoes as $key => $promocao) {
            $images_promocoes[] = ["legenda" => $promocao['legenda'], "url" => $promocao['url_firebase']];
         }
         $dados['imagesPromocoes'] = $images_promocoes;
      }

      $eventos = $this -> anuncios_model -> get_eventos($id_user);
      $agendaEventos = [];

      foreach ($eventos as $key => $evento) {
         $agendaEventos[] = ['complemento' => $evento['complemento'],'data' => $evento['data'],"descricao" => $evento['descricao'],'hora' => $evento['hora']];
      }
      $dados['agendaEventos'] = $agendaEventos;

      return $dados;
   }

   function get_logo($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from users_files where type_file = 1 and id_user = $id_user order by sequence";
      $result = $this -> db -> query($query)->result_array();
      return $result;
   }

   function get_locais($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from users_files where type_file = 2 and id_user = $id_user order by sequence";
      $result = $this -> db -> query($query)->result_array();
      return $result;
   }

   function get_cardapios($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from users_files where type_file = 3 and id_user = $id_user order by sequence";
      $result = $this -> db -> query($query)->result_array();

      return $result;
   }
   function get_promocoes($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from users_files where type_file = 4 and id_user = $id_user order by sequence";
      $result = $this -> db -> query($query)->result_array();

      return $result;
   }

   function get_all_files($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from users_files where id_user = $id_user order by sequence";
      $result = $this -> db -> query($query)->result_array();

      return $result;
   }

   function get_userdetail($id_user = ""){
      if(empty($id_user)){
         $id_user = $this->get_id_user();
      }
      $query = "select * from userdetail where id_user = $id_user";
      $result = $this -> db -> query($query)->result_array();

      return $result;
   }

   function exportar_firebase($id_user){
      $firebasefloop = new Firebasefloop();
      $firebasefloop->init();

      $storage = new Firebasestorage();
      $storage->init();

      $user_data = $this -> users_model -> get_user_data($id_user);
      $key_firebase = $user_data -> key_firebase;

      // Se não existir uma referência no firebase cria uma nova
      if(empty($key_firebase)){
         $id = $firebasefloop->insert('null');
         $this->db->update('userdetail',['key_firebase' => $id], ['id_user' => $id_user]);
      } else {
         $id = $key_firebase;
      }

      // Deleta todos os arquivos do firebase
      $all_files = $this -> users_model -> get_all_files($id_user);
      foreach ($all_files as $key => $file) {
         if(!empty($file['path_firebase'])){
            $storage->deleteFile($file['path_firebase']);
         }
      }

      // Pegando todos os arquivos por categoria
      $logo = $this->users_model->get_logo($id_user)[0];
      $cardapios = $this->users_model->get_cardapios($id_user);
      $locais = $this->users_model->get_locais($id_user);
      $promocoes = $this->users_model->get_promocoes($id_user);

      //Inserindo imagem do logo no firebase
      if ($logo) {
         $file = end(explode('/',$logo['path_file']));
         $path_firebase = 'parceiros/'.$id.'/images_marker/'.$file;
         $path_storage = $storage->upload('parceiros/'.$id.'/images_marker', $logo['path_file']);
         $url_firebase = $storage->publicUrl($path_storage);
         $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $logo['id_user'], 'type_file' => $logo['type_file'], 'id_file' => $logo['id_file']]);
      }

      // Inserindo as imagens de cardápio no firebase
      if ($cardapios) {
         foreach ($cardapios as $key => $cardapio) {
            $file = end(explode('/',$cardapio['path_file']));
            $path_firebase = 'parceiros/'.$id.'/images_cardapio/'.$file;
            $path_storage = $storage->upload('parceiros/'.$id.'/images_cardapio', $cardapio['path_file']);
            $url_firebase = $storage->publicUrl($path_storage);
            $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $cardapio['id_user'], 'type_file' => $cardapio['type_file'], 'id_file' => $cardapio['id_file']]);

         }
      }

      // Inserindo as imagens de estabelecimentos no firebase
      if ($locais) {
         foreach ($locais as $key => $local) {
            $file = end(explode('/',$local['path_file']));
            $path_firebase = 'parceiros/'.$id.'/images_header/'.$file;
            $path_storage = $storage->upload('parceiros/'.$id.'/images_header', $local['path_file']);
            $url_firebase = $storage->publicUrl($path_storage);
            $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $local['id_user'], 'type_file' => $local['type_file'], 'id_file' => $local['id_file']]);
         }
      }

      // Inserindo as Imagens de promoção no firebase
      if ($promocoes) {
         foreach ($promocoes as $key => $promocao) {
            $file = end(explode('/',$promocao['path_file']));
            $path_firebase = 'parceiros/'.$id.'/images_promocoes/'.$file;
            $path_storage = $storage->upload('parceiros/'.$id.'/images_promocoes', $promocao['path_file']);
            $url_firebase = $storage->publicUrl($path_storage);
            $this->db->update('users_files', ['url_firebase' => $url_firebase, 'path_firebase' => $path_firebase], ['id_user' => $promocao['id_user'], 'type_file' => $promocao['type_file'], 'id_file' => $promocao['id_file']]);
         }
      }

      $dados = $this->users_model->montar_dados_firebase($id_user);
      $place_id = $dados['place_id'];
      unset($dados['place_id']);

      $key_firebase = $this->users_model->get_user_data($id_user)->key_firebase;
      $firebasefloop -> update($key_firebase, $dados);


      $firebasefloopblack = new Firebasefloop();
      $firebasefloopblack -> init("blacklist");
      if (!empty($place_id)){
         $firebasefloopblack -> addBlackList($place_id);
      }
      $this -> anuncios_model -> removeBlackList();
   }

   function delete_user($id_user){
      $id_anuncio = $this -> db -> query("select id_anuncio from anuncios where id_user = $id_user") -> result_array()[0]['id_anuncio'];
      // deletando do firebase
      if(!empty($id_anuncio)){
         $this -> anuncios_model -> desativar_anuncio($id_anuncio);
      }

      // Deletando arquivos salvos
      $arquivos = $this -> users_model -> get_all_files();
      foreach ($arquivos as $key => $arquivo) {
         if (file_exists($arquivo['path_file'])) {
            unlink($arquivo['path_file']);
         }
      }

      // deletando tabela de arquivos;
      $this -> db -> delete ('users_files', ['id_user' => $id_user]);

      // deletando anuncio
      $this -> db -> delete ('anuncios', ['id_user' => $id_user]);

      //deleando userdetail
      $this -> db -> delete ('userdetail', ['id_user' => $id_user]);


      //deletando eventos
      $this -> db -> delete ('eventos', ['id_anuncio' => $id_anuncio]);

      //deletando financeiro
      $this -> db -> delete ('financeiro', ['id_user' => $id_user]);

      // deletando horários
      $this -> db -> delete ('horario_funcionamento', ['id_anuncio' => $id_anuncio]);

      //deleando usuário
      $this -> db -> delete ('users', ['id_user' => $id_user]);
   }

}
